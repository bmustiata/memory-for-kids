package com.ciplogic.lightbox.java;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.ciplogic.lightbox.java.view.LightBoxHumanView;
import com.ciplogic.lightbox.java.world.LightBoxGameWorld;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.process.GlobalGameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.OpenGlContextLostEvent;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.process.chain.ApplicationStartupChain;

public class LightBox implements ApplicationListener {
	float elapsed;

    GlobalGameProcessManager gameProcessManager = new GlobalGameProcessManager();

    @Override
	public void create () {
        gameProcessManager.addProcess(new ApplicationStartupChain(LightBoxGameWorld.INSTANCE, new LightBoxHumanView()).create());
    }

    @Override
	public void resize (int width, int height) {
        EventBus.INSTANCE.fire( new ScreenResizeEvent(width, height) );
	}

	@Override
	public void render () {
		elapsed += Gdx.graphics.getDeltaTime();
        gameProcessManager.updateProcesses(System.currentTimeMillis());
	}

	@Override
	public void pause () {
        EventBus.INSTANCE.fire(new OpenGlContextLostEvent());
        gameProcessManager.shutdown();
	}

	@Override
	public void resume () {
        gameProcessManager.restart();
        gameProcessManager.addProcess(new ApplicationStartupChain(LightBoxGameWorld.INSTANCE, new LightBoxHumanView()).create());
	}

	@Override
	public void dispose () {
	}
}
