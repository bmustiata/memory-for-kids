package com.ciplogic.lightbox.java;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ciplogic.lightbox.java.form.ActorsOverviewForm;
import com.ciplogic.lightbox.java.world.LightBoxGameWorld;
import com.ciplogic.lightbox.java.world.WorldPreset;
import com.ciplogic.lightbox.java.world.XmlCodeComponent;
import com.ciplogic.memory.core.independent.application.graphics.ShaderManager;
import com.ciplogic.memory.core.independent.logic.actor.Actor;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeSet;

public class LightBoxBuilderDesktop {

    final static boolean[] showGeneratedActors = new boolean[] { false };

    public static void main(String[] args) {
        JFrame frame = new JFrame("LightBox Actors");
        final ActorsOverviewForm form = new ActorsOverviewForm();

        frame.setContentPane(form);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(1000, 800);
        frame.setVisible(true);

        form.actorList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                String actorId = (String) form.actorList.getSelectedValue();

                if (actorId == null) { // no selection
                    return;
                }

                Actor existingActor = LightBoxGameWorld.INSTANCE.get(actorId);

                XmlCodeComponent component = existingActor.getComponent(XmlCodeComponent.class);
                String actorXmlText;

                if (component != null) {
                    actorXmlText = component.xmlCode;
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    existingActor.serializeXml(0, stringBuilder);
                    actorXmlText = stringBuilder.toString();
                }

                form.actorCode.setText(actorXmlText);
            }
        });

        form.addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String actorCode = form.actorCode.getText();

                Actor actor = LightBoxGameWorld.INSTANCE.createActor(actorCode);
                actor.withComponent( new XmlCodeComponent(actor, actorCode) );

                Actor existingActor = LightBoxGameWorld.INSTANCE.hasActor(actor.id);

                if (existingActor != null) {
                    LightBoxGameWorld.INSTANCE.removeActor(existingActor);
                }

                LightBoxGameWorld.INSTANCE.addActor(actor);

                refreshActors(form);
            }
        });

        form.refreshActorsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshActors(form);
            }
        });

        form.removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeSelectedActor(form);
                refreshActors(form);
            }
        });

        form.setGameWorld.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LightBoxGameWorld.WORLD_FILE_NAME = form.worldNameText.getText();
                LightBoxGameWorld.COMPONENT_FACTORY = form.componentFactoryText.getText();

                LightBoxGameWorld.INSTANCE.nuke();
            }
        });

        form.worldPresetCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WorldPreset worldPreset = WorldPreset.valueOf(form.worldPresetCombo.getSelectedItem().toString().toUpperCase());
                form.worldNameText.setText(worldPreset.resourceName);
                form.componentFactoryText.setText(worldPreset.clazz.getCanonicalName());
            }
        });

        form.showGeneratedActorsCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showGeneratedActors[0] = form.showGeneratedActorsCheckBox.isSelected();
                refreshActors(form);
            }
        });

        form.updateVertexShaderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ShaderManager.vertexShaderCode = form.vertexShaderCode.getText();
            }
        });

        form.updateFragmentShaderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ShaderManager.fragmentShaderCode = form.fragmentShaderCode.getText();
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.fullscreen = false;
                config.width = 350;
                config.height = 600;
                config.vSyncEnabled = false;

                config.title = "LightBox";

                new LwjglApplication(new LightBox(), config);

                form.vertexShaderCode.setText( ShaderManager.vertexShaderCode );
                form.fragmentShaderCode.setText( ShaderManager.fragmentShaderCode );
            }
        }).start();
    }

    private static void removeSelectedActor(ActorsOverviewForm form) {
        Actor actor = LightBoxGameWorld.INSTANCE.get(form.actorList.getSelectedValue().toString());
        LightBoxGameWorld.INSTANCE.removeActor(actor);
    }

    private static void refreshActors(ActorsOverviewForm form) {
        TreeSet actorIds = new TreeSet();
        for (Actor actor : LightBoxGameWorld.INSTANCE.getAllActors()) {
            if (actor.id.startsWith("generated-") && !showGeneratedActors[0]) {
                continue;
            }

            actorIds.add(actor.id);
        }

        form.actorList.setListData(actorIds.toArray());
    }
}

