package com.ciplogic.lightbox.java.world;

import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.actors.components.DefaultComponentFactory;
import com.ciplogic.memory.core.view.menu.MenuComponentFactory;
import com.ciplogic.memory.core.view.play.PlayComponentFactory;

public enum WorldPreset {
    EMPTY("empty.world.xml", DefaultComponentFactory.class),
    LIGHTBOX("lightbox.world.xml", DefaultComponentFactory.class),
    MENU("menu.world.xml", MenuComponentFactory.class),
    MEMORY("play.world.xml", PlayComponentFactory.class),
    CHEST("chest.world.xml", PlayComponentFactory.class),
    LOADING("loading.world.xml", DefaultComponentFactory.class);

    public final String resourceName;
    public final Class<? extends ComponentFactory> clazz;

    WorldPreset(String resourceName, Class<? extends ComponentFactory> componentFactoryClass) {
        this.resourceName = resourceName;
        this.clazz = componentFactoryClass;
    }
}
