package com.ciplogic.lightbox.java.view;

import com.ciplogic.lightbox.java.world.LightBoxGameWorld;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.DefaultGameWorldProcess;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;

public class LightBoxHumanView extends HumanGameView {
    @Override
    public void onInit() {
        gameProcessManager.addProcess(new DefaultGameWorldProcess(LightBoxGameWorld.INSTANCE));
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        gameProcessManager.shutdown();
    }

    @Override
    public GameWorld getGameWorld() {
        return LightBoxGameWorld.INSTANCE;
    }
}
