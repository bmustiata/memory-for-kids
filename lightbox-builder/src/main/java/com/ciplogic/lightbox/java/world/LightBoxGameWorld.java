package com.ciplogic.lightbox.java.world;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.actors.components.DefaultComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

public class LightBoxGameWorld extends GameWorld {
    public static LightBoxGameWorld INSTANCE = new LightBoxGameWorld();

    public static String COMPONENT_FACTORY = "com.ciplogic.memory.core.actors.components.DefaultComponentFactory";
    public static String WORLD_FILE_NAME = "lightbox.world.xml";

    @Override
    public ActorFactory getActorFactory() {
        return new XmlCodeGameWorldParser(this, WORLD_FILE_NAME, createComponentFactory());
    }

    private ComponentFactory createComponentFactory() {
        try {
            return (ComponentFactory) Class.forName(COMPONENT_FACTORY).newInstance();
        } catch (Throwable e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return new DefaultComponentFactory();
        }
    }
}
