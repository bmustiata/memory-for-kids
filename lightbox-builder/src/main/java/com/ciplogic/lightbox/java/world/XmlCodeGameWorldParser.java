package com.ciplogic.lightbox.java.world;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.logic.GameWorldParser;

import java.util.Map;

/**
 * A decorator implementation that just makes sure each Actor created also
 * has an XmlCodeComponent in order to be usable with external tools such as
 * LightBoxBuilder.
 */
public class XmlCodeGameWorldParser extends GameWorldParser {
    public XmlCodeGameWorldParser(GameWorld gameWorld, String fileName, ComponentFactory componentFactory) {
        super(gameWorld, fileName, componentFactory);
    }

    @Override
    public Actor createActor(String resource, Map<String, String> actorParameters) {
        return super.createActor(resource, actorParameters);
    }

    @Override
    public Actor createActor(XmlReader.Element actorElement) {
        Actor actor = super.createActor(actorElement);

        actor.withComponent( new XmlCodeComponent(actor, actorElement.toString()) );

        return actor;
    }

    @Override
    public Actor createActor(String actorCode) {
        Actor actor = super.createActor(actorCode);

        actor.withComponent( new XmlCodeComponent(actor, actorCode) );

        return actor;
    }
}
