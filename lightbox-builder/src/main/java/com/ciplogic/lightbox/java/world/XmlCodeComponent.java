package com.ciplogic.lightbox.java.world;

import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

public class XmlCodeComponent extends ActorComponent {
    public String xmlCode;

    public XmlCodeComponent(Actor actor, String actorCode) {
        super(actor, null);

        this.xmlCode = actorCode;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        // empty on purpose.
    }
}
