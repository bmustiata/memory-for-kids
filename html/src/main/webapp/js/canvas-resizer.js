(function($) {

    function resizeCanvas() {
        $('canvas').attr({
            'width' : $(window).width() - 20,
            'height' : $(window).height() - 20
        });

        $('table').css({
            'width' : $(window).width() - 20 + "px",
            'height' : $(window).height() - 20 + "px"
        });
    }


    $(window).on('resize', resizeCanvas);

    var initialResizeInterval = setInterval(function() {
        $('canvas').each(function() {
            clearInterval(initialResizeInterval);
            resizeCanvas();
        });
    }, 500);

})(jQuery);
