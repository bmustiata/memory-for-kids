
# current location C:\Program Files\GIMP 2\32\lib\gimp\2.0\python
import random
import gimpfu

from gimpfu import pdb


def generate_color():
    values = [0xd5, 0x91]
    return ( values[ random.randint(0, 1) ], values[ random.randint(0, 1) ], values[ random.randint(0, 1) ] )

def callback(image, drawable, letter):
    with open('c:/tmp/gimp.log', 'w') as f:
        active_layer = pdb.gimp_image_get_active_layer(image)
        while active_layer:
            pdb.gimp_image_remove_layer(image, active_layer)
            active_layer = pdb.gimp_image_get_active_layer(image)

        pdb.gimp_image_resize(image, 256, 256, 0, 0)

        # render a color filled background
        layer = pdb.gimp_layer_new(image, 256, 256, 1, "Background", 100, 0)
        pdb.gimp_image_insert_layer(image, layer, None, 0)

        pdb.gimp_selection_all(image)
        pdb.gimp_context_set_foreground(generate_color())
        pdb.gimp_edit_bucket_fill(layer, 0, 0, 100, 0, False, 0, 0)
        pdb.gimp_selection_none(image)

        # render the gradient over the background
        gradient_layer = pdb.gimp_layer_new(image, 256, 256, 1, "Gradient", 100, 0)
        pdb.gimp_image_insert_layer(image, gradient_layer, None, 0)

        pdb.gimp_context_set_foreground((0xff, 0xff, 0xff))

        pdb.gimp_image_select_ellipse(image, 0, -297, -99, 598, 264)
        pdb.gimp_context_set_gradient('FG to Transparent')
        pdb.gimp_edit_blend(gradient_layer, 2, 0, 0, 100, 0, 0, False, False, 1, 0, False, 0, 500, 0, 50)
        pdb.gimp_selection_none(image)

        # render the actual symbol / letter
        pdb.gimp_context_set_foreground((0xff, 0xff, 0xff))
        text_layer = pdb.gimp_text_fontname(image, None, 0, 0, letter, 0, True, 240, 0, "Sans")

        pdb.plug_in_autocrop_layer(image, text_layer)

        pdb.gimp_layer_set_offsets(text_layer, (256 - text_layer.width) / 2, (256 - text_layer.height) / 2)

        pdb.script_fu_drop_shadow(image, text_layer, 2, 2, 15, (0, 0, 0), 80, 0)

        print(letter)

gimpfu.register(
    "render_letter",
    "Render a certain letter into a 256x256 image.",
    "Render a certain letter into a 256x256 image.",
    "Bogdan Mustiata <bogdan.mustiata@gmail.com>",
    "Bogdan Mustiata <bogdan.mustiata@gmail.com>",
    "2014",
    "<Image>/Ciplogic/Render",
    "RGB*",
    [
        (gimpfu.PF_STRING, "letter", "Letter", "A")
    ],
    [],
    callback
)

gimpfu.main()
