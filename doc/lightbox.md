Lightbox
========

This document provides general documentation in regards to LightBox.

GameWorld
---------
A GameWorld has multiple actors.

Actors define what is in the GameWorld, where is the camera, the players (if any), etc. Everything
that is manipulated in the GameWorld defines the actual game, or screen of the game that happens.

In order to interact with the actors, HumanGameViews are created. These map the inputs from the user
into game world events, that various actors from the game world will process.

FIXME: why do they do that?
HumanGameViews also run the processes that do the rendering itself, or other processes that are
changing the GameWorld.

Usually a screen in the game is a combination of a HumanGameView, and a GameWorld.

Actor
-----

Each actor must define an ID. In case no ID is defined, a random ID will be generated, prefixed
with "generated-id-" in order to mark that this is actually a generated ID.

Actors are just a container for a bunch of actor components.

Actors can be defined in external files:

For example if in the $/raw/play.world.xml we have:

```xml
<world>
    <!-- ... -->

    <include-time-counters/>
</world>
```

The include-time-counters will be searched in: $/raw/play/include-time-counters.actor.xml

External actors can have parameters that are defined in the root element, with their default values.
```xml
<actor starColor="blue" starY="0" starYShadow="-0.2">
```

Then you can refer to those parameters such as:
```xml
<position x="3" y="{starY}"/>
```

text and loadFont are special components that are part of the core, and they will be included directly
from the core/ folder.

When creating the external actor via the ActorFactory, these parameters can be overriden.

composite
---------

Composite is a special component that allows adding multiple actors under the umbrella of the same
actor:

```xml
<actor id="foo">
    <composite keep-ids="true">
        <actor id="moo">
        ...
        </actor>

        <actor id="boo">
        ...
    </composite>
</actor>
```

In combination with external actors it's a very effective way to split the game world into multiple
files. If keep-ids in the composite component is not set, the ids are being joined together from
the parent, so for the previous sample, assuming keep-ids was false we would have 3 actors:

foo
foo-moo
foo-boo

Since the container is also an actor.

When an actor that has a composite component gets destroyed, it will destroy all the actors
that were registered by the composite component.



