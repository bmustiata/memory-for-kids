Achievements
============

Functional Specification
------------------------

Whenever a game finishes, if certain conditions are met, an achievements may be unlocked. If that's the case then
a dialog pops up with the text that identifies the achievement that was unlocked, and offers the options to either
share on FaceBook, either close the dialog.

In case multiple achievements are unlocked simultaneously, then the dialog will display the first achievement, then
after closing it (by either selecting close, either selecting the facebook share), the other dialog that waits in
the queue will pop up.

In case the user clicks "Facebook Share", the facebook notification integration comes into play that will authenticate
the user, and post the achievement on his feed. Obviously this is platform specific.

Implementation Details
----------------------

See diagram ![Achievements Model](achievements_model.png).

