package com.ciplogic.memory.core.view.play.component.achievements;

public enum Achievement {
// FIXME: FIRST_GAME, GAMES_PLAYED_5 and FIRST_GAME_DIFFICULT should be enabled when the models
//        are available.
//    FIRST_GAME,
    FINISHED_ALL_TILES_IMAGES,
    FINISHED_ALL_TILES_IMAGES_ON_DIFFICULT,
//    GAMES_PLAYED_5,
    GAMES_PLAYED_10,
    GAMES_PLAYED_20,
    GAMES_PLAYED_100,
//    FIRST_GAME_DIFFICULT,
    FLOWERS_DIFFICULT
}
