package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.utils.XmlReader;

public class Distance {
    public float value;
    public Unit unit;

    public static enum Unit {
        PIXELS,
        PERCENTAGE
    }

    public static Distance readDistanceElement(XmlReader.Element child, String attributeName) {
        String value = child.getAttribute(attributeName);

        return readDistanceValue(value);
    }

    public static Distance readDistanceValue(String value) {
        Distance result = new Distance();

        if (value.endsWith("%")) {
            result.unit = Unit.PERCENTAGE;
            value = value.substring(0, value.length() - 1);
        } else {
            result.unit = Unit.PIXELS;
        }

        result.value = Float.valueOf( value );

        return result;
    }

    @Override
    public String toString() {
        return value + (unit == Unit.PERCENTAGE ? "%" : "");
    }
}
