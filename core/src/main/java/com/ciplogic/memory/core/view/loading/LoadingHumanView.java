package com.ciplogic.memory.core.view.loading;

import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.actors.components.TextComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.BuilderProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.*;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.logic.loading.LoadingGameWorld;
import com.ciplogic.memory.core.time.TimeShift;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoadingHumanView extends HumanGameView implements EventListener {
    @Override
    public void onInit() {
        super.onInit();

        EventBus.INSTANCE.registerListener(this);

        LoadingGameWorld gameWorld = LoadingGameWorld.INSTANCE;

        CameraComponent camera = gameWorld.get("camera").getComponent(CameraComponent.class);

        camera.fitBoth(16f, 16f);
        camera.resetCamera();

        Actor cube = gameWorld.get("cube");

        cube.getComponent(ModelInstanceComponent.class).getPosition().x = - camera.getPosition().z - 2;

        gameProcessManager.addProcess(
            new BuilderProcess().then(
                new LinearTranslateAnimationProcess(cube, 0)
                        .to(-10.0f, -0.8f, 4f).then(
                    new LinearTranslateAnimationProcess(cube, 500, TimeShift.QUADT ).to( 0f, -0.8f, 4f)
                ),

                new UpdateLifeCycledActorsProcess(gameWorld),

                new ClearScreenProcess(0.3f, 0.3f, 0.3f),
                new RenderSpritesProcess(gameWorld.backgroundSprites, new HashMap<Integer, Map<String, TextComponent>>()),

                new ClearDepthBufferProcess(),
                new RenderSceneProcess(gameWorld),

                new ClearDepthBufferProcess(),
                new RenderSpritesProcess(gameWorld.foregroundSprites, gameWorld.textLayers)
            )
        );
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        super.onFinish();

        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof ScreenResizeEvent) {
            LoadingGameWorld.INSTANCE.get("camera").getComponent(CameraComponent.class).resetCamera();
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(ScreenResizeEvent.class);
    }

    @Override
    public GameWorld getGameWorld() {
        return LoadingGameWorld.INSTANCE;
    }
}
