package com.ciplogic.memory.core;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyTypedEvent;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.logic.loading.LoadingGameWorld;

import java.util.Collections;
import java.util.List;

public class SimulatePauseResumeProcess extends GameProcess implements EventListener {
    private final MemoryGame memoryGame;

    public SimulatePauseResumeProcess(MemoryGame memoryGame) {
        this.memoryGame = memoryGame;
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onEvent(Event event) {
        KeyTypedEvent keyEvent = ((KeyTypedEvent) event);

        if (keyEvent.keyTyped == 'p') {
            memoryGame.pause();
            LoadingGameWorld.INSTANCE.resourceCache.dispose();
            LoadingGameWorld.INSTANCE = new LoadingGameWorld();
            memoryGame.resume();
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(KeyTypedEvent.class);
    }
}
