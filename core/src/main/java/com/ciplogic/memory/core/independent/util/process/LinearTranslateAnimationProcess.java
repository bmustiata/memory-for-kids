package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.math.Vector3;
import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.actors.components.Positionable;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.time.TimeShift;

public class LinearTranslateAnimationProcess extends GameProcess {
    private final long totalAnimationMillisLength;
    private long startingDelta;
    private Positionable positionable;

    private Vector3 originalPosition;
    private Vector3 finalPosition;
    private Vector3 intermediaryPosition = new Vector3();

    private TimeShift[] timeShifts = new TimeShift[] {
        TimeShift.LINEAR,
        TimeShift.LINEAR,
        TimeShift.LINEAR
    };

    public LinearTranslateAnimationProcess(Actor actor, long totalAnimationMillisLength, TimeShift ... timeShifts) {
        positionable = findPositionable(actor);

        this.totalAnimationMillisLength = totalAnimationMillisLength;

        for (int i = 0; i < timeShifts.length; i++) {
            this.timeShifts[i] = timeShifts[i];
        }
    }

    private Positionable findPositionable(Actor actor) {
        ModelInstanceComponent modelInstance = actor.getComponent(ModelInstanceComponent.class);
        if (modelInstance != null) {
            return modelInstance;
        }

        CameraComponent cameraComponent = actor.getComponent(CameraComponent.class);
        if (cameraComponent != null) {
            return cameraComponent;
        }

        throw new IllegalArgumentException("Actor " + actor.id + " has no positionable components.");
    }


    @Override
    public void onInit(long delta) {
        super.onInit(delta);

        originalPosition = positionable.getPosition();
        startingDelta = delta;
    }

    @Override
    public void onUpdate(long delta) {
        long passedMillis = delta - startingDelta;

        if (passedMillis >= totalAnimationMillisLength) {
            finish();
            return;
        }

        intermediaryPosition.x = timeShifts[0].compute(startingDelta, totalAnimationMillisLength, delta) *
                (finalPosition.x - originalPosition.x) + originalPosition.x;
        intermediaryPosition.y =  timeShifts[1].compute(startingDelta, totalAnimationMillisLength, delta) *
                (finalPosition.y - originalPosition.y) + originalPosition.y;
        intermediaryPosition.z =  timeShifts[2].compute(startingDelta, totalAnimationMillisLength, delta) *
                (finalPosition.z - originalPosition.z) + originalPosition.z;

        positionable.setPosition(intermediaryPosition);
    }

    @Override
    public void onFinish() {
        positionable.setPosition(finalPosition);
    }

    public LinearTranslateAnimationProcess to(float x, float y, float z) {
        this.finalPosition = new Vector3(x, y, z);

        return this;
    }
}
