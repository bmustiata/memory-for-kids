package com.ciplogic.memory.core.independent.application.event;

import com.ciplogic.memory.core.lang.ModifiableLinkedHashSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The event bus of the game.
 * The listeners actually tell what events they're interested upon, for faster event dispatching.
 */
public class EventBus {
    public static EventBus INSTANCE = new EventBus();

    private Map<Class, Set<EventListener>> eventTypeToListeners = new HashMap<Class, Set<EventListener>>();

    public <T extends Event> void fire(T event) {
        Set<EventListener> listeners = eventTypeToListeners.get(event.getClass());

        if (listeners != null && !listeners.isEmpty()) {
            for (EventListener listener : listeners) {
                listener.onEvent(event);
            }
        } else {
//            Gdx.app.error("Memory", "Received event " + event.getClass().getCanonicalName() + " but no listeners " +
//                    "were listening for it.");
        }
    }

    public void registerListener( EventListener listener ) {
        List<Class<Event>> listenedEvents = listener.getListenedEvents();

        for (Class<Event> listenedEvent : listenedEvents) {
            Set<EventListener> eventListeners = eventTypeToListeners.get(listenedEvent);

            if (eventListeners == null) {
                eventListeners = new ModifiableLinkedHashSet<EventListener>();
                eventTypeToListeners.put(listenedEvent, eventListeners);
            }

            if (eventListeners.contains(listener)) {
                throw new IllegalStateException("Listener already registered: " + listener);
            }

            eventListeners.add(listener);
        }
    }

    public void unregisterListener( EventListener listener ) {
        for (Class<Event> listenedEventType : listener.getListenedEvents()) {
            eventTypeToListeners.get(listenedEventType).remove(listener);
        }
    }
}
