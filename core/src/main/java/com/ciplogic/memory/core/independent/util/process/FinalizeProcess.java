package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

/**
 * Shuts down another game process, and only after the other process its marked as FINISHED
 * or FAILED, this process completes.
 */
public class FinalizeProcess extends GameProcess {
    private final GameProcess processToShutdown;

    public FinalizeProcess(GameProcess processToShutdown) {
        if (processToShutdown == null) {
            throw new IllegalArgumentException("Process to shutdown can not be null.");
        }

        this.processToShutdown = processToShutdown;
    }


    @Override
    public void onInit() {
        processToShutdown.finish();
    }

    @Override
    public void onUpdate(long delta) {
        if (processToShutdown.state == State.FINISHED ||
            processToShutdown.state == State.FAILED) {
            finish();
        }
    }
}
