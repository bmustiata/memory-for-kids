package com.ciplogic.memory.core.logic.test;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.logic.GameWorldParser;
import com.ciplogic.memory.core.view.menu.MenuComponentFactory;

public class TestGameWorld extends GameWorld {
    public static TestGameWorld INSTANCE = new TestGameWorld();

    @Override
    public ActorFactory getActorFactory() {
        return new GameWorldParser(this, "test.world.xml", new MenuComponentFactory());
    }
}
