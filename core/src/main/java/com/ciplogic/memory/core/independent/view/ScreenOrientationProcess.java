package com.ciplogic.memory.core.independent.view;

import com.badlogic.gdx.Gdx;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.ScreenOrientation;

import java.util.Collections;
import java.util.List;

/**
 * This waits for resolution changed, and updates the current ScreenOrientation
 * accordingly.
 */
public class ScreenOrientationProcess extends GameProcess implements EventListener {
    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
        if (Gdx.graphics.getWidth() <  Gdx.graphics.getHeight()) {
            ScreenOrientation.CURRENT = ScreenOrientation.PORTRAIT;
        } else {
            ScreenOrientation.CURRENT = ScreenOrientation.LANDSCAPE;
        }

        EventBus.INSTANCE.fire(new ScreenOrientationChangedEvent( ScreenOrientation.CURRENT, null ));
    }

    @Override
    public void onUpdate(long delta) {
        // noop on purpose, actor?
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        ScreenResizeEvent screenResizeEvent = (ScreenResizeEvent) event;

        ScreenOrientation oldScreenOrientation = ScreenOrientation.CURRENT;

        if (screenResizeEvent.width < screenResizeEvent.height ) {
            ScreenOrientation.CURRENT = ScreenOrientation.PORTRAIT;
        } else {
            ScreenOrientation.CURRENT = ScreenOrientation.LANDSCAPE;
        }

        if (oldScreenOrientation != ScreenOrientation.CURRENT) {
            EventBus.INSTANCE.fire(new ScreenOrientationChangedEvent( ScreenOrientation.CURRENT, oldScreenOrientation ));
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList( ScreenResizeEvent.class );
    }
}
