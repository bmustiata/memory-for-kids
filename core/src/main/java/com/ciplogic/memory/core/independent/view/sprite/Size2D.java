package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.XmlReader;

/**
 * <p>Holds the size of an object. The size can be represented as either pixels (default),
 * or percentage from the screen size. For example 5%, etc.</p>
 *
 * <p>There also can be a keepAspect attribute, set to any of:</p>
 * <ul>
 *     <li>none (default)</li>
 *     <li>width</li>
 *     <li>height</li>
 * </ul>
 * <p>That specifies what is the main direction for keeping the aspect ratio,
 * and the other attribute if set is ignored in order to make the calculation
 * work</p>
 * <p>It is implicitly expected that both distances have the same measurement unit.</p>
 */
public class Size2D {
    public Distance width;
    public Distance height;

    /**
     * What is the main coordinate for keeping the aspect ratio relative to.
     */
    public enum KeepAspect {
        /**
         * Do not keep the aspect ration. Both height and width from the
         * Size2D will be used, and some strethcing might occur.
         */
        NONE,

        /**
         * Keep the aspect ratio, considering the width as the base. The
         * height from the Size2D will be ignored.
         */
        WIDTH,

        /**
         * Keep the aspect ratio, considering the height as the base. The
         * width from the Size2D will be ignored.
         */
        HEIGHT;

        static KeepAspect parseString(String value) {
            return KeepAspect.valueOf( value.toUpperCase() );
        }

        @Override
        public String toString() {
            switch (this) {
                case NONE: return "none";
                case WIDTH: return "width";
                case HEIGHT: return "height";
                default: return null;
            }
        }
    }

    public KeepAspect keepAspect;
    float aspectRatio;

    public static Size2D readFromNode(XmlReader.Element child, float aspectRatio) {
        Size2D size2D = new Size2D();

        size2D.width = Distance.readDistanceElement(child, "width");
        size2D.height = Distance.readDistanceElement(child, "height");

        size2D.aspectRatio = aspectRatio;

        String keepAspect = child.getAttribute("keepAspect", "none");
        size2D.keepAspect = KeepAspect.parseString(keepAspect);

        // we expect the same unit of measurement, you can't have different units for width and height
        assert size2D.width.unit == size2D.height.unit;

        return size2D;
    }


    public static Size2D readFromValues(String width, String height, KeepAspect keepAspectRatio, float aspectRatio) {
        Size2D size2D = new Size2D();

        size2D.width = Distance.readDistanceValue(width);
        size2D.height = Distance.readDistanceValue(height);

        size2D.aspectRatio = aspectRatio;

        size2D.keepAspect = keepAspectRatio;

        // we expect the same unit of measurement, you can't have different units for width and height
        assert size2D.width.unit == size2D.height.unit;

        return size2D;
    }

    public float getScreenWidth() {
        switch (keepAspect) {
            case NONE:
            case WIDTH:
                if (width.unit == Distance.Unit.PIXELS) {
                    return width.value;
                } else {
                    return Gdx.graphics.getWidth() * width.value / 100.0f;
                }
            case HEIGHT:
                return getScreenHeight() * aspectRatio;
            default:
                throw new IllegalStateException("keepAspect should be set to something known.");
        }
    }

    public float getScreenHeight() {
        switch (keepAspect) {
            case WIDTH:
                return getScreenWidth() / aspectRatio;
            case NONE:
            case HEIGHT:
                if (height.unit == Distance.Unit.PIXELS) {
                    return height.value;
                } else {
                    return Gdx.graphics.getHeight() * height.value / 100.0f;
                }
            default:
                throw new IllegalStateException("keepAspect should be set to something known.");
        }
    }
}
