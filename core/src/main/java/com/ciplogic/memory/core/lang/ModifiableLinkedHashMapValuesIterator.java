package com.ciplogic.memory.core.lang;

import java.util.AbstractCollection;
import java.util.Iterator;

public class ModifiableLinkedHashMapValuesIterator<V> extends AbstractCollection<V> implements ModifiableHashMapOperationListener {
    private final UnregisterCallback unregisterCallback;
    private ModifiableLinkedHashMap.Entry currentValue;

    public ModifiableLinkedHashMapValuesIterator(ModifiableLinkedHashMap.Entry first, UnregisterCallback unregisterCallback) {
        this.currentValue = first;
        this.unregisterCallback = unregisterCallback;
    }

    @Override
    public Iterator<V> iterator() {
        return new Iterator<V>() {
            @Override
            public boolean hasNext() {
                boolean result = currentValue != null;

                if (!result) {
                    unregisterCallback.unregister();
                }

                return result;
            }

            @Override
            public V next() {
                ModifiableLinkedHashMap.Entry result = currentValue;
                currentValue = currentValue.after;

                return (V) result.value;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Unsupported");
            }
        };
    }

    @Override
    public int size() {
        return Integer.MAX_VALUE;
    }

    @Override
    public void onEntryAdded(ModifiableLinkedHashMap.Entry entry) {
        if (currentValue == null) {
            currentValue = entry;
        }
    }

    @Override
    public void onEntryRemoved(ModifiableLinkedHashMap.Entry entry) {
        if (currentValue == entry) {
            currentValue = currentValue.after;
        }
    }
}
