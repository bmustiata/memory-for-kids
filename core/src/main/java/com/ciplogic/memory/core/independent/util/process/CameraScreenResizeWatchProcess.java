package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;

import java.util.Collections;
import java.util.List;

public class CameraScreenResizeWatchProcess extends GameProcess implements EventListener {
    private final GameWorld gameWorld;

    public CameraScreenResizeWatchProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener( this );
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onEvent(Event event) {
        gameWorld.get("camera").getComponent(CameraComponent.class).resetCamera();
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(ScreenResizeEvent.class);
    }
}
