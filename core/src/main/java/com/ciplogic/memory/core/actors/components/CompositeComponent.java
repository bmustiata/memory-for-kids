package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class CompositeComponent extends ActorComponent {
    private final XmlReader.Element node;

    /**
     * A variable that marks if the IDs from the composite components
     * should be kept as is, without prefixing it with the current actor ID.
     */
    private boolean keepIds;

    public Actor[] actors;

    public CompositeComponent(Actor actor, boolean keepIds, Actor ... childActors) {
        super(actor, null);

        this.node = null;
        this.keepIds = keepIds;
        this.actors = childActors;

        if (!keepIds) {
            for (int i = 0; i < this.actors.length; i++) {
                this.actors[i].id = actor.id + "-" + this.actors[i].id;
            }
        }

        for (int i = 0; i < this.actors.length; i++) {
            this.actor.gameWorld.addActor( this.actors[i] );
        }
    }

    public CompositeComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);
        this.node = child;
        this.actors = new Actor[ child.getChildCount() ];

        keepIds = Boolean.parseBoolean( child.getAttribute("keep-ids", "false") );

        for (int i = 0; i < node.getChildCount(); i++) {
            Actor compositeActor = actor.gameWorld.getActorFactory().createActor(node.getChild(i));
            compositeActor.id = keepIds ? compositeActor.id : actor.id + "-" + compositeActor.id;

            actors[i] = compositeActor;

            actor.gameWorld.addActor(compositeActor);
        }
    }

    @Override
    public void onFinish() {
        for (int i = 0; i < actors.length; i++) {
            actor.gameWorld.removeActor( actors[i] );
        }
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<composite>\n");

        for (int i = 0; i < actors.length; i++) {
            actors[i].serializeXml(indentLevel + 1, stringBuilder);
        }

        stringBuilder.append(indent(indentLevel))
                .append("</composite>\n");
    }
}
