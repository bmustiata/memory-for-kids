package com.ciplogic.memory.core.lang;

public interface ModifiableHashMapOperationListener {
    public <K, V> void onEntryAdded(ModifiableLinkedHashMap<K, V>.Entry entry);

    public <K, V> void onEntryRemoved(ModifiableLinkedHashMap<K, V>.Entry entry);
}
