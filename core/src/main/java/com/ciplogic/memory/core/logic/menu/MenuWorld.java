package com.ciplogic.memory.core.logic.menu;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.logic.GameWorldParser;
import com.ciplogic.memory.core.view.menu.MenuComponentFactory;

public class MenuWorld extends GameWorld {
    public static MenuWorld INSTANCE = new MenuWorld();

    @Override
    public ActorFactory getActorFactory() {
        return new GameWorldParser(this, "menu.world.xml", new MenuComponentFactory());
    }
}
