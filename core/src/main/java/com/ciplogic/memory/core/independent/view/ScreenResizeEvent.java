package com.ciplogic.memory.core.independent.view;

import com.ciplogic.memory.core.independent.application.event.Event;

public class ScreenResizeEvent extends Event {
    public final int width;
    public final int height;

    public ScreenResizeEvent(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
