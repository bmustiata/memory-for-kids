package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.actors.components.TextComponent;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

import java.util.HashMap;
import java.util.Map;

/**
 * A composite process that does the updating of the actors, clearing the screen, rendering the scene and sprites,
 * and also monitors for resolution changes.
 */
public class DefaultGameWorldProcess extends GameProcess {
    public DefaultGameWorldProcess(GameWorld gameWorld) {
        then(
            new UpdateLifeCycledActorsProcess(gameWorld),

            new CameraScreenResizeWatchProcess(gameWorld),
            new SpriteScreenResizeWatchProcess(gameWorld),

            new ScreenOrientationChangeWatchProcess(gameWorld),

            // start fresh, clear the screen, render the background sprites
            new ClearDepthBufferProcess(),
            new RenderSpritesProcess(gameWorld.backgroundSprites, new HashMap<Integer, Map<String, TextComponent>>()),

            // the sprites screw up the depth buffer, clear the depth buffer
            new ClearDepthBufferProcess(),
            new RenderSceneProcess(gameWorld),

            // just to be sure clear the depth buffer yet again.
            new ClearDepthBufferProcess(),
            new RenderSpritesProcess(gameWorld.foregroundSprites, gameWorld.textLayers)
        );
    }

    @Override
    public void onUpdate(long delta) {
        finish();
    }
}
