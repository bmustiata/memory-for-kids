package com.ciplogic.memory.core.independent.application.event;

import java.util.List;

public interface EventListener {
    public void onEvent(Event event);

    public <T extends Event> List<Class<T>> getListenedEvents();
}
