package com.ciplogic.memory.core.independent.application.process;

import com.ciplogic.memory.core.independent.application.event.Event;

public class CreateGameProcess extends Event {
    public GameProcess gameProcess;

    public CreateGameProcess(GameProcess gameProcess) {
        this.gameProcess = gameProcess;
    }
}
