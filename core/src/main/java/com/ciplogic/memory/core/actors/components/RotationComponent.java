package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;

public class RotationComponent extends ActorComponent {
    public Rotatable rotatable;
    public Vector3 original_rotation = new Vector3();

    public RotationComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        for (ActorComponent component : actor.components.values()) {
            if (component instanceof Rotatable) {
                rotatable = (Rotatable) component;
            }
        }

        assert rotatable != null;

        original_rotation.set(
                floatAttribute(child, "x"),
                floatAttribute(child, "y"),
                floatAttribute(child, "z"));

        rotatable.setRotation(original_rotation);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<rotation x=\"" +
                    rotatable.getRotation().x +
                    "\" y=\"" +
                    rotatable.getRotation().y +
                    "\" z=\"" +
                    rotatable.getRotation().z +
                    "\"/>\n");

    }

    public RotationComponent(Actor actor, float x, float y, float z) {
        super(actor, null);

        for (ActorComponent component : actor.components.values()) {
            if (component instanceof Rotatable) {
                rotatable = (Rotatable) component;
            }
        }

        assert rotatable != null;

        original_rotation.set(x, y, z);

        rotatable.setRotation(original_rotation);

    }

    @Override
    public void restart() {
        rotatable.setRotation(original_rotation);
    }
}
