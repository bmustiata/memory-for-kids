package com.ciplogic.memory.core.time;

public class LinearTimeShift extends TimeShift {
    @Override
    public float compute(long start, long duration, long current) {
        if (start + duration < current) {
            return 1.0f;
        }

        return ((float)(current - start)) / duration;
    }
}
