package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

/**
 * <p>A camera is an object through which the scene can be viewed.</p>
 * <p>The camera is positionable, so it can be positioned in space (including translations).</p>
 */
public class CameraComponent extends ActorComponent implements Positionable {

    public PerspectiveCamera camera;

    public Vector3 position;
    public Vector3 lookAt;

    private Float fitWidth;
    private Float fitHeight;

    public CameraComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        position = new Vector3(5.0f, 5.0f, 5.0f);

        XmlReader.Element lookAtNode = child.getChildByName("lookAt");
        if (lookAtNode != null) {
            lookAt = new Vector3(
                    floatAttribute(lookAtNode, "x"),
                    floatAttribute(lookAtNode, "y"),
                    floatAttribute(lookAtNode, "z"));
        } else {
            lookAt = new Vector3(0.0f, 0.0f, 0.0f);
        }

        resetCamera();
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<camera>\n")
                .append(indent(indentLevel + 1))
                .append("<lookAt x=\"")
                .append(lookAt.x)
                .append("\" y=\"")
                .append(lookAt.y)
                .append("\" z=\"")
                .append(lookAt.z)
                .append("\"/>\n")
            .append("   </camera>\n");
    }

    @Override
    public void restart() {
        this.resetCamera();
    }

    public void resetCamera() {
        if (fitWidth != null && fitHeight != null) {
            this.fitBoth(fitWidth, fitHeight);
        } else if (fitWidth != null) {
            this.fitWidth(fitWidth);
        } else if (fitHeight != null) {
            this.fitHeight(fitHeight);
        }

        camera = new PerspectiveCamera(90, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(position);
        camera.lookAt(lookAt);
        camera.update();
    }

    @Override
    public void setPosition(Vector3 position) {
        this.position = position;

        resetCamera();
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    /**
     * <p>Sets the position of the camera to a (0, 0, z) where z is computed such as the scene width
     * will fit into the camera view.</p>
     * <p>If there is already a lookAt position, the position of the camera will be translated with
     * that position.</p>
     */
    public void fitWidth(float targetWidth) {
        fitWidth = targetWidth;
        fitHeight = null;

        float z = computeFitWidth(targetWidth);
        position.set(lookAt.x + 0, lookAt.y + 0, lookAt.z + z);
    }

    /**
     * <p>Sets the position of the camera to a (0, 0, z) where z is computed such as the scene height
     * will fit into the camera view.</p>
     * <p>If there is already a lookAt position, the position of the camera will be translated with
     * that position.</p>
     */
    public void fitHeight(float targetHeight) {
        fitWidth = null;
        fitHeight = targetHeight;

        float z = computeFitHeight(targetHeight);
        position.set(lookAt.x + 0, lookAt.y + 0, lookAt.z + z);
    }

    /**
     * <p>Sets the position of the camera to a (0, 0, z) where z is computed such as both the targetWidth
     * and targetHeight will fit into the camera view.</p>
     * <p>If there is already a lookAt position, the position of the camera will be translated with
     * that position.</p>
     */
    public void fitBoth(float targetWidth, float targetHeight) {
        fitWidth = targetWidth;
        fitHeight = targetHeight;

        float z = Math.max( computeFitWidth(targetWidth), computeFitHeight(targetHeight) );
        position.set(lookAt.x + 0, lookAt.y + 0, lookAt.z + z);
    }

    private float computeFitWidth(float targetWidth) {
        return targetWidth / 2.0f * Gdx.graphics.getHeight() / Gdx.graphics.getWidth();
    }

    private float computeFitHeight(float targetHeight) {
        return targetHeight / 2.0f;
    }

    public Vector3 getWorldPositionFrom2d(int screenX, int screenY) {
        Vector3 result = new Vector3(screenX, screenY, 1);

        // FIXME: this looks like a bug in libGDX since it doesn't substracts the camera
        // position from the unproject, but blindly assumes it's in (0, 0, 0)
        camera.unproject(result);

        result.sub( camera.position );

        return result;
    }
}
