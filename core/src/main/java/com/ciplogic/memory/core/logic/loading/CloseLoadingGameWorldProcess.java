package com.ciplogic.memory.core.logic.loading;

import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.independent.application.process.BuilderProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcessManager;
import com.ciplogic.memory.core.independent.util.process.LinearTranslateAnimationProcess;
import com.ciplogic.memory.core.time.TimeShift;

public class CloseLoadingGameWorldProcess extends GameProcess {
    private GameProcessManager gameProcessManager = new GameProcessManager();

    private boolean initialized;

    @Override
    public void onUpdate(long delta) {
        if (!initialized) {
            initialized = true;
            LoadingGameWorld gameWorld = LoadingGameWorld.INSTANCE;

            gameProcessManager.addProcess(new BuilderProcess().then(
                    new LinearTranslateAnimationProcess(gameWorld.get("cube"), 500, TimeShift.QUAD).to(12f, -0.8f, 4f)
            ));
        }

        gameProcessManager.updateProcesses(delta);
        if (gameProcessManager.isEmpty()) {
            finish();
        }
    }
}
