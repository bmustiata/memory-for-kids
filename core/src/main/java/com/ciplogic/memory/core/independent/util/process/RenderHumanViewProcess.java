package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;

public class RenderHumanViewProcess extends GameProcess {
    private final HumanGameView humanGameView;

    public static RenderHumanViewProcess INSTANCE = null;

    private boolean initialized = false;

    public RenderHumanViewProcess(HumanGameView humanGameView) {
        this.humanGameView = humanGameView;
    }

    @Override
    public void onUpdate(long delta) {
        if (!initialized) {
            initialized = true;
            humanGameView.onInit();

            HumanGameView.INSTANCE = humanGameView;
            RenderHumanViewProcess.INSTANCE = this;
        }

        if (HumanGameView.INSTANCE != humanGameView) {
            throw new IllegalStateException("Views differ. The rendering process should be shut down before changing " +
                    "the human view to another human view. Expected " + humanGameView + " got " + HumanGameView.INSTANCE);
        }

        humanGameView.onUpdate(delta);
    }

    @Override
    public void onFinish() {
        if (initialized) {
            humanGameView.onFinish();
        }

        RenderHumanViewProcess.INSTANCE = null;
    }
}
