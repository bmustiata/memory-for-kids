package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.PreferencesComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.RandomGenerator;
import com.ciplogic.memory.core.view.menu.MenuType;
import com.ciplogic.memory.core.view.menu.component.event.MenuOptionChangedEvent;
import com.ciplogic.memory.core.view.play.component.event.*;

import java.util.*;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class BoardComponent extends ActorComponent implements EventListener {
    private BoardPieceComponent previouslyTurned;
    public Map<String, BoardPieceComponent> piecesOnBoard = new HashMap<String, BoardPieceComponent>();
    public List<String> shuffledPieces = new ArrayList<String>();

    boolean createNewGame = true;
    public int turnedPairs = 0;
    public int totalPairs = 0;
    private String deck;

    public BoardComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                    .append("<board/>\n");
    }

    @Override
    public void onInit() {
        PreferencesComponent preferences = actor.gameWorld.get("preferences").getComponent(PreferencesComponent.class);
        totalPairs = Integer.parseInt( MenuType.getOption(preferences, MenuType.PAIRS) );
        deck = MenuType.getOption(preferences, MenuType.DECK);

        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onUpdate(long delta) {
        if (createNewGame) {
            createNewGame = false;

            newGame();
        }
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    public void newGame() {
        List<Actor> removedPieces = removeExistingPieces();
        createPieces();
        findPiecesOnBoard();
        shufflePieces();

        turnedPairs = 0;

        firePiecesRemovedEvent(removedPieces);
        fireNewPiecesCreatedEvent();
    }

    private List<Actor> removeExistingPieces() {
        List<Actor> oldActorsToRemove = new ArrayList<Actor>();

        for (Actor otherActor : actor.gameWorld.getAllActors()) {
            BoardPieceComponent boardPiece = otherActor.getComponent(BoardPieceComponent.class);
            if (boardPiece != null && !boardPiece.removed) {
                boardPiece.removed = true;
                oldActorsToRemove.add( boardPiece.actor );
            }
        }
        piecesOnBoard.clear();

        return oldActorsToRemove;
    }

    private void createPieces() {
        actor.getComponent( GenerateInitialPiecesComponent.class ).generatePieces();
    }

    private void findPiecesOnBoard() {
        for (Actor otherActor : actor.gameWorld.getAllActors()) {
            BoardPieceComponent boardPiece = otherActor.getComponent(BoardPieceComponent.class);
            if (boardPiece != null) {
                if (!boardPiece.removed) {
                    piecesOnBoard.put(otherActor.id, boardPiece);
                }
            }
        }
    }

    private void shufflePieces() {
        shuffledPieces.clear();
        shuffledPieces.addAll( piecesOnBoard.keySet() );

        for (int i = shuffledPieces.size() - 1; i >= 1; i--) {
            int nextPosition = RandomGenerator.nextInt(i);

            String tmp = shuffledPieces.get(nextPosition);
            shuffledPieces.set(nextPosition, shuffledPieces.get(i) );
            shuffledPieces.set(i, tmp);
        }
    }

    private void firePiecesRemovedEvent(List<Actor> removedPieces) {
        EventBus.INSTANCE.fire( new RemovePiecesEvent(removedPieces) );
    }

    private void fireNewPiecesCreatedEvent() {
        List<Actor> createdActors = new ArrayList<Actor>();
        for (String actorId : shuffledPieces) {
            createdActors.add( actor.gameWorld.get(actorId) );
        }

        EventBus.INSTANCE.fire( new CreatedPiecesEvent(createdActors) );
    }

    public void turn(BoardPieceComponent boardPieceComponent) {
        if (boardPieceComponent.turned || boardPieceComponent.paired) {
            return;
        }

        EventBus.INSTANCE.fire(new ShowPiecesEvent(boardPieceComponent));
        boardPieceComponent.turned = true;

        if (previouslyTurned != null) {
            if (previouslyTurned.gameTileDefinition.id.equals(boardPieceComponent.gameTileDefinition.id)) {
                previouslyTurned.paired = true;
                boardPieceComponent.paired = true;
                EventBus.INSTANCE.fire(new PiecesPairedEvent(previouslyTurned, boardPieceComponent));
                turnedPairs++;
            } else {
                boardPieceComponent.turned = false;
                previouslyTurned.turned = false;
                EventBus.INSTANCE.fire(new HidePiecesEvent(previouslyTurned, boardPieceComponent));
            }
            previouslyTurned = null;
        } else {
            previouslyTurned = boardPieceComponent;
        }

        if (turnedPairs == totalPairs) { // FIXME: read the settings
            EventBus.INSTANCE.fire(new GameWonEvent(deck, totalPairs));
        }
    }

    @Override
    public void onEvent(Event event) {
        MenuOptionChangedEvent menuOptionChangedEvent = (MenuOptionChangedEvent) event;
        if (menuOptionChangedEvent.menuType == MenuType.PAIRS) {
            totalPairs = Integer.parseInt( menuOptionChangedEvent.currentOption );
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList( MenuOptionChangedEvent.class );
    }
}
