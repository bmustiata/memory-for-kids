package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class ModelInstanceComponent extends ActorComponent implements Positionable, Rotatable, Scalable {
    private ModelInstance modelInstance;
    public String modelId;

    public Vector3 position = new Vector3();
    public Quaternion rotation = new Quaternion();
    public Vector3 savedRotation = new Vector3();
    public Vector3 scaling = new Vector3(1, 1, 1);
    private boolean frozen;

    public ModelInstanceComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        if (child != null) {
            this.modelId = child.getAttribute("modelId");
        }
    }

    public ModelInstanceComponent(Actor actor, String modelId) {
        super(actor, null);
        this.modelId = modelId;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<modelInstance modelId=\"")
            .append(modelId)
            .append("\"/>\n");
    }

    @Override
    public void setPosition(Vector3 position) {
        if (this.frozen) {
            throw new IllegalArgumentException("Actor is frozen");
        }

        this.position = position;
        if (modelInstance != null) {
            modelInstance.transform.set(position, rotation, scaling);
        }
    }

    @Override
    public Vector3 getPosition() {
        return position;
    }

    public void setModelInstance(ModelInstance modelInstance) {
        this.modelInstance = modelInstance;
        modelInstance.transform.set(position, rotation, scaling);
    }

    public ModelInstance getModelInstance() {
        return modelInstance;
    }

    @Override
    public void setRotation(Vector3 rotation) {
        savedRotation = rotation;
        this.rotation.setEulerAngles(rotation.x, rotation.y, rotation.z);
        if (modelInstance != null) {
            modelInstance.transform.set(position, this.rotation, scaling);
        }
    }

    @Override
    public Vector3 getRotation() {
        return this.savedRotation;
    }

    @Override
    public void setScaling(Vector3 scaling) {
        this.scaling = scaling;
        if (modelInstance != null) {
            modelInstance.transform.set(position, rotation, scaling);
        }
    }

    @Override
    public Vector3 getScaling() {
        return scaling;
    }

    @Override
    public void restart() {
        if (modelInstance != null) {
            modelInstance.transform.set(position, rotation, scaling);
        }
    }

    public void freezePosition() {
        this.frozen = true;
    }
}
