package com.ciplogic.memory.core.view.play.component.achievements;

import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class AchievementDefinitionsActorComponent extends ActorComponent {
    public final static String achievementsXml = "chest/achievements.xml";
    public static Map<Achievement, AchievementDefinition> achievementDefinitions = readAchievementDefinitions();

    public AchievementDefinitionsActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<achievementDefinitions/>\n");
    }

    public String getAchievementDescription(Achievement achievement) {
        return achievementDefinitions.get(achievement).description;
    }

    public String getAchievementTitle(Achievement achievement) {
        return achievementDefinitions.get(achievement).title;
    }

    /**
     * Read the achievement definitions from their XML file.
     * @return
     */
    private static Map<Achievement, AchievementDefinition> readAchievementDefinitions() {
        Map<Achievement, AchievementDefinition> result = new HashMap<Achievement, AchievementDefinition>();

        XmlReader.Element rootNode = parseXml();

        for (int i = 0; i < rootNode.getChildCount(); i++) {
            XmlReader.Element node = rootNode.getChild(i);

            result.put(
                    Achievement.valueOf(node.getAttribute("id")),
                    new AchievementDefinition(
                            node.getAttribute("title"),
                            node.getAttribute("description")
            ));
        }

        return result;
    }

    private static XmlReader.Element parseXml() {
        InternalFileHandleResolver internalFileResolver = new InternalFileHandleResolver();

        try {
            return new XmlReader().parse(internalFileResolver.resolve(achievementsXml));
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to parse " + achievementsXml, e);
        }
    }
}
