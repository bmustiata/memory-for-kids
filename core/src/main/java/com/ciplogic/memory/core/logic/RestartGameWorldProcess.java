package com.ciplogic.memory.core.logic;

import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.*;

import java.util.Map;

public class RestartGameWorldProcess extends GameProcess {
    private final GameWorld gameWorld;

    public RestartGameWorldProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onUpdate(long delta) {
        for (Actor actor : gameWorld.getAllActors()) {
            for (ActorComponent component : actor.components.values()) {
                component.restart();
            }

            for (Map<ScreenOrientation, ScreenActorComponent> screenMap : actor.screenAwareComponents.values()) {
                screenMap.get(ScreenOrientation.CURRENT).restart();
            }
        }

        finish();
    }
}
