package com.ciplogic.memory.core.view.play.component.achievements;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementsGameFinishedEvent;
import com.ciplogic.memory.core.view.play.component.event.GameWonEvent;
import com.ciplogic.memory.core.view.play.component.event.TimedGameWonEvent;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

/**
 * This components fires AchievementsGameFinishedEvents, only when all the data is available.
 *
 * It does that by waiting for both GameWonEvent and TimedGameWonEvent, since the later is fired
 * by the board timer component.
 */
public class AchievementsGameFinishedEventLauncherActorComponent extends ActorComponent implements EventListener {
    private int totalPairs;
    private long time;
    private String deck;

    public AchievementsGameFinishedEventLauncherActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        resetState();
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<achievementsGameFinishedEventLauncher/>\n");
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof TimedGameWonEvent) {
            TimedGameWonEvent castEvent = (TimedGameWonEvent) event;
            time = castEvent.ellapsedTime;
        }

        if (event instanceof GameWonEvent) {
            GameWonEvent castEvent = (GameWonEvent) event;
            totalPairs = castEvent.totalPairs;
            deck = castEvent.deck;
        }

        if (totalPairs >= 0 && time >= 0) {
            EventBus.INSTANCE.fire( new AchievementsGameFinishedEvent(deck, totalPairs, time) );
            resetState();
        }
    }

    private void resetState() {
        deck = null;
        totalPairs = -1;
        time = -1;
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List events = new ArrayList<Event>();

        events.add( TimedGameWonEvent.class );
        events.add( GameWonEvent.class );

        return events;
    }
}
