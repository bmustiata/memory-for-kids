package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class ClearScreenComponent extends ActorComponent {
    private float red;
    private float green;
    private float blue;

    public ClearScreenComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        lifecycled = true;

        red = floatAttribute(child, "red", "0.1f");
        green = floatAttribute(child, "green", "0.1f");
        blue = floatAttribute(child, "blue", "0.1f");
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<clearScreen red=\"")
                .append(red)
                .append("\" green=\"")
                .append(green)
                .append("\" blue=\"")
                .append(blue)
                .append("\"/>\n");
    }

    @Override
    public void onUpdate(long delta) {
        Gdx.gl.glClearColor(red, green, blue, 0);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }
}
