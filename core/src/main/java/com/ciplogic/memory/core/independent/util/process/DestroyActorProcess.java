package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.Gdx;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;

public class DestroyActorProcess extends GameProcess {
    private final Actor actor;

    public DestroyActorProcess(Actor actor) {
        this.actor = actor;
    }

    @Override
    public void onInit() {
        actor.gameWorld.removeActor(actor);
    }

    @Override
    public void onUpdate(long delta) {
        finish();
    }
}
