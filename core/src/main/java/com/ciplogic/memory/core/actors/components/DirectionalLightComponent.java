package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class DirectionalLightComponent extends ActorComponent {
    public final DirectionalLight light;

    public DirectionalLightComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        Color color;

        XmlReader.Element colorNode = child.getChildByName("color");
        if (colorNode != null) {
            color = new Color(
                    floatAttribute(colorNode, "red"),
                    floatAttribute(colorNode, "green"),
                    floatAttribute(colorNode, "blue"),
                    floatAttribute(colorNode, "alpha", "1.0")
            );
        } else {
            color = Color.WHITE;
        }

        Vector3 direction;
        XmlReader.Element directionNode = child.getChildByName("direction");
        if (directionNode != null) {
            direction = new Vector3(
                    floatAttribute(directionNode, "x"),
                    floatAttribute(directionNode, "y"),
                    floatAttribute(directionNode, "z")
            ).nor();
        } else {
            direction = new Vector3(-1, -0.5f, 0).nor();
        }

        light = new DirectionalLight().set(color, direction);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
            .append("<directionalLight>\n")
                .append(indent(indentLevel + 1))
                .append("<color red=\"")
                .append(light.color.r)
                .append("\" green=\"")
                .append(light.color.g)
                .append("\" blue=\"")
                .append(light.color.b)
                .append("\" alpha=\"")
                .append(light.color.a)
                .append("\"/>\n")

            .append(indent(indentLevel + 1))
                .append("<direction x=\"")
                .append(light.direction.x)
                .append("\" y=\"")
                .append(light.direction.y)
                .append("\" z=\"")
                .append(light.direction.z)
                .append("\"/>\n")
        .append(indent(indentLevel))
        .append("</directionalLight>\n");
    }
}
