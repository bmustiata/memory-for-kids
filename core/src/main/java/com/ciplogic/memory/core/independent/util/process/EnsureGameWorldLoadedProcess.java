package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.event.GameWorldLoadedEvent;

/**
 * This process ensures that the initial actors from a GameWorld are loaded.
 */
public class EnsureGameWorldLoadedProcess extends GameProcess {
    private final GameWorld gameWorld;
    private boolean worldEmpty;
    private ActorFactory actorFactory;

    public EnsureGameWorldLoadedProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
        worldEmpty = gameWorld.isEmpty();
    }

    @Override
    public void onInit() {
        if (!worldEmpty) {
            finish();
        } else {
            actorFactory = gameWorld.getActorFactory();
        }
    }

    @Override
    public void onUpdate(long delta) {
        while (actorFactory.hasNext()) {
            gameWorld.addActor( actorFactory.next() );
        }

        finish();
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.fire(new GameWorldLoadedEvent(gameWorld));
    }
}
