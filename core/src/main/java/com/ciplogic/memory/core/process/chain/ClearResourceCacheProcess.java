package com.ciplogic.memory.core.process.chain;

import com.ciplogic.memory.core.independent.application.cache.ResourceCache;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

public class ClearResourceCacheProcess extends GameProcess {
    private final GameWorld currentGameWorld;

    public ClearResourceCacheProcess(GameWorld currentGameWorld) {
        this.currentGameWorld = currentGameWorld;
    }

    @Override
    public void onUpdate(long delta) {
        currentGameWorld.resourceCache.dispose();
        currentGameWorld.resourceCache = new ResourceCache();
        finish();
    }
}
