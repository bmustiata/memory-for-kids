package com.ciplogic.memory.core.lang;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <p>This is a map that allows iterating its values in the order they were added,
 * while allowing adding and removing elements into the map, while iterated.</p>
 * @param <K>
 * @param <V>
 */
public class ModifiableLinkedHashMap<K, V> implements Map<K, V> {
    class Entry {
        Entry before;
        Entry after;

        V value;
    }

    private Entry first = null;
    private Entry last = null;

    private Map<K, V> storage = new HashMap<K, V>();
    private Map<K, Entry> knownEntries = new HashMap<K, Entry>();

    private int iteratorId;
    private Map<Integer, ModifiableHashMapOperationListener> iterators = new HashMap<Integer, ModifiableHashMapOperationListener>();

    @Override
    public int size() {
        return storage.size();
    }

    @Override
    public boolean isEmpty() {
        return storage.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return storage.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return storage.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return storage.get(key);
    }

    @Override
    public V put(K key, V value) {
        Entry entry = knownEntries.get(key);

        if (entry != null) {
            entry.value = value;

            return storage.put(key, value);
        }

        entry = new Entry();
        entry.value = value;

        // if there was no element before, yay we got one now.
        if (first == null) {
            first = entry;
        }

        if (last != null) {
            // just link it as it is supposed to be
            last.after = entry;
            entry.before = last;
        }

        last = entry;

        notifyEntryAdded(entry);

        knownEntries.put(key, entry);
        storage.put(key, value);

        return null;
    }

    @Override
    public V remove(Object key) {
        Entry entry = knownEntries.get(key);

        if (entry == null) {
            throw new IllegalStateException("Unknown key: " + key);
        }

        if (entry.before != null) {
            entry.before.after = entry.after;
        }
        if (entry.after != null) {
            entry.after.before = entry.before;
        }

        if (first == entry) {
            first = entry.after;
        }
        if (last == entry) {
            last = entry.before;
        }

        notifyEntryRemoved(entry);

        entry.before = null;
        entry.after = null;

        knownEntries.remove(key);
        return storage.remove(key);
    }

    private void notifyEntryRemoved(Entry entry) {
        for (ModifiableHashMapOperationListener iterator : iterators.values()) {
            iterator.onEntryRemoved(entry);
        }
    }

    private void notifyEntryAdded(Entry entry) {
        for (ModifiableHashMapOperationListener iterator : iterators.values()) {
            iterator.onEntryAdded(entry);
        }
    }


    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        storage.clear();
        knownEntries.clear();
        first = null;
        last = null;
    }

    @Override
    public Set<K> keySet() {
        return storage.keySet();
    }

    @Override
    public Collection<V> values() {
        final int currentId = iteratorId++;

        ModifiableLinkedHashMapValuesIterator<V> values = new ModifiableLinkedHashMapValuesIterator<V>(first,
            new UnregisterCallback() {
                @Override
                public void unregister() {
                    iterators.remove(currentId);
                }
            });

        iterators.put(currentId, values);

        return values;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return storage.entrySet();
    }
}
