package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.independent.application.cache.ResourceCache;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.play.component.event.CreatedPiecesEvent;

import java.util.Collections;
import java.util.List;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

/**
 * Stuff that does the UI layouting, or setting the textures right for the
 * pieces from the board.
 */
public class BoardUiComponent extends ActorComponent implements EventListener {
    private final BoardComponent boardComponent;
    private boolean initialized;

    public BoardUiComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        boardComponent = actor.getComponent(BoardComponent.class);
        lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<boardUi/>\n");
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onUpdate(long delta) {
        if (!initialized) {
            initialized = true;
            remapTexturesForPieces();
        }
    }

    @Override
    public void restart() {
        remapTexturesForPieces();
    }

    private void remapTexturesForPieces() {
        Gdx.app.log("App", "remap textures");

        ResourceCache resourceCache = actor.gameWorld.resourceCache;

        for(BoardPieceComponent piece : boardComponent.piecesOnBoard.values()) {
            ModelInstance modelInstance = piece.actor.getComponent(ModelInstanceComponent.class).getModelInstance();
            Texture texture = resourceCache.getTexture("play/pack/" + piece.gameTileDefinition.pack + "/" + piece.gameTileDefinition.image);
            modelInstance.materials.get(0).get(TextureAttribute.class, TextureAttribute.Diffuse).textureDescription.texture = texture;
        }
    }

    @Override
    public void onEvent(Event event) {
        remapTexturesForPieces();
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(CreatedPiecesEvent.class);
    }
}
