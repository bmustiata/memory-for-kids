package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class SpinningModelInstanceComponent extends ActorComponent {
    private final int millisPer360Rotation;
    private long initialDelta = -1;

    private Rotatable rotatable;
    private Vector3 computationRotation;

    public SpinningModelInstanceComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);
        this.millisPer360Rotation = Integer.parseInt(child.getAttribute("millisPer360Rotation"));
        this.lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<spinningModelInstance millisPer360Rotation=\"")
                .append(millisPer360Rotation)
                .append("\"/>\n");

    }

    public SpinningModelInstanceComponent(Actor actor, int millisPer360Rotation) {
        super(actor, null);
        this.millisPer360Rotation = millisPer360Rotation;
        this.lifecycled = true;
    }

    @Override
    public void onInit() {
        this.rotatable = actor.getComponent(ModelInstanceComponent.class);
        computationRotation = this.rotatable.getRotation().cpy();
    }

    @Override
    public void onUpdate(long delta) {
        if (initialDelta == -1) {
            initialDelta = delta;
        }

        float rotationalDelta = 1.0f * (delta - initialDelta) % millisPer360Rotation / millisPer360Rotation;

        computationRotation.set(360f * rotationalDelta, 0, 0);

        rotatable.setRotation(computationRotation);
    }

    @Override
    public void onFinish() {
        rotatable.setRotation( new Vector3(0, 0, 0) );
    }
}
