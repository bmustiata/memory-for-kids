package com.ciplogic.memory.core.actors.components.util;

import com.badlogic.gdx.utils.XmlReader;

public class XmlUtil {
    public static float floatAttribute(XmlReader.Element colorNode, String attributeName) {
        return Float.parseFloat(colorNode.getAttribute(attributeName));
    }

    public static float floatAttribute(XmlReader.Element node, String attributeName, String defaultValue) {
        String attribute = node.getAttribute(attributeName, defaultValue);

        return Float.parseFloat(attribute);
    }

    public static int intAttribute(XmlReader.Element node, String attributeName, String defaultValue) {
        String attribute = node.getAttribute(attributeName, defaultValue);

        return Integer.parseInt(attribute);
    }
}
