package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.process.DelayProcess;
import com.ciplogic.memory.core.independent.util.process.DestroyActorProcess;
import com.ciplogic.memory.core.view.util.StringUtil;

public class SelfDestroyComponent extends ActorComponent {
    private final long timeout;

    public SelfDestroyComponent(Actor actor, long timeout) {
        super(actor, null);

        this.timeout = timeout;

        addDestructProcess(actor);
    }

    public SelfDestroyComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        timeout = Long.parseLong(child.getAttribute("timeout"));

        addDestructProcess(actor);
    }

    private void addDestructProcess(Actor actor) {
        actor.getComponent(ProcessManagerComponent.class).addProcess(
                new DelayProcess(timeout).then(
                        new DestroyActorProcess(actor)
                )
        );
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<selfDestroy timeout=\"")
                .append(timeout)
                .append("\"/>\n");
    }
}
