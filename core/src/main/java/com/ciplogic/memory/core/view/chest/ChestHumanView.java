package com.ciplogic.memory.core.view.chest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.collision.Ray;
import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.actors.components.PositionComponent;
import com.ciplogic.memory.core.actors.components.intersect.SphereBoundingActorComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.input.event.mouse.TouchDownEvent;
import com.ciplogic.memory.core.independent.view.gameview.DefaultHumanGameView;
import com.ciplogic.memory.core.logic.chest.ChestGameWorld;
import com.ciplogic.memory.core.view.menu.MenuHumanView;
import com.ciplogic.memory.core.view.util.HumanViewSwitcher;
import com.ciplogic.memory.core.view.util.SpriteTest;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ChestHumanView extends DefaultHumanGameView {
    public ChestHumanView() {
        super(ChestGameWorld.INSTANCE);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof TouchDownEvent) {
            TouchDownEvent touchDownEvent = (TouchDownEvent) event;

            if (SpriteTest.isActorInTouchEvent( gameWorld.get("button-back"), touchDownEvent)) {
                HumanViewSwitcher.changeHumanView( new MenuHumanView() );
                return;
            }

            Ray ray = createRay(touchDownEvent);

            Set<SphereBoundingActorComponent> achievementsBoundingSpheres =
                    gameWorld.findComponents(SphereBoundingActorComponent.class);

            // FIXME: utility method?
            for (SphereBoundingActorComponent achievementBounding : achievementsBoundingSpheres) {
                PositionComponent position = achievementBounding.actor.getComponent(PositionComponent.class);

                if (Intersector.intersectRaySphere(ray,
                        position.positionable.getPosition(),
                        achievementBounding.radius,
                        null)) {

                    Gdx.app.log("Click:", achievementBounding.actor.id);
                }
            }
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(TouchDownEvent.class);
    }

    private Ray createRay(TouchDownEvent event) {
        CameraComponent camera = gameWorld.get("camera").getComponent(CameraComponent.class);

        return new Ray(
            camera.getPosition(),
            camera.getWorldPositionFrom2d(event.screenX, event.screenY)
        );
    }
}
