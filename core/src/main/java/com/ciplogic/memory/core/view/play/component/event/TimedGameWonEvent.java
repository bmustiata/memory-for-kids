package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;

public class TimedGameWonEvent extends Event {
    public long ellapsedTime;

    public TimedGameWonEvent(long ellapsedTime) {
        this.ellapsedTime = ellapsedTime;
    }
}
