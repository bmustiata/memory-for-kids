This package is not truly independent, but since the implementation is in GDX itself,
it makes more sense to have it in the core (accessible for everyone) instead of copying
it around.