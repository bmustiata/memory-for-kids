package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class RunnableProcess extends GameProcess {
    private final Runnable runnable;

    public RunnableProcess(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    public void onInit() {
        this.runnable.run();
    }

    @Override
    public void onUpdate(long delta) {
        finish();
    }
}
