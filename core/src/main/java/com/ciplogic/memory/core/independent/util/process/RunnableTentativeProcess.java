package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

/**
 * Unlike the RunnableProcess that always will run its process since
 * it executes on the onInit() of the lifecycle, the RunnableTentativeProcess
 * will run its runnable inside its onUpdate() method, allowing for recursive
 * chain adding of processes to end.
 */
public class RunnableTentativeProcess extends GameProcess {
    private final Runnable runnable;

    public RunnableTentativeProcess(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    public void onUpdate(long delta) {
        this.runnable.run();
        finish();
    }
}
