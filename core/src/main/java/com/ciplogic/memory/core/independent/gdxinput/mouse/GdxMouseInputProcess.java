package com.ciplogic.memory.core.independent.gdxinput.mouse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.input.event.mouse.*;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

/**
 * A mouse input process is an application layer process that monitors the mouse,
 * and generates event on the game bus regarding mouse moves, and clicks.
 */
public class GdxMouseInputProcess extends GameProcess implements InputProcessor {
    @Override
    public void onInit() {
        state = State.PAUSED; // make sure we're not calling onUpdate() for no reason

        InputMultiplexer inputMultiplexer = (InputMultiplexer) Gdx.input.getInputProcessor();
        inputMultiplexer.addProcessor(this);
    }

    @Override
    public void onFinish() {
        removeGdxInputProcessor();
    }

    @Override
    public void onFail() {
        removeGdxInputProcessor();
    }

    private void removeGdxInputProcessor() {
        InputMultiplexer inputMultiplexer = (InputMultiplexer) Gdx.input.getInputProcessor();
        // since we only add from processes, removal should be the last oen.
        inputMultiplexer.removeProcessor(inputMultiplexer.getProcessors().size - 1);
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        EventBus.INSTANCE.fire( new TouchDownEvent(screenX, screenY, pointer, button) );
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        EventBus.INSTANCE.fire( new TouchUpEvent(screenX, screenY, pointer, button) );
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        EventBus.INSTANCE.fire( new TouchDraggedEvent(screenX, screenY, pointer) );
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        EventBus.INSTANCE.fire( new MouseMovedEvent(screenX, screenY) );
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        EventBus.INSTANCE.fire( new MouseScrolledEvent(amount) );
        return true;
    }
}
