package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class ValidateProcess extends GameProcess {
    public String name;

    public int initialized;
    public int updated;
    public int shutdown;

    public ValidateProcess() {
    }

    public ValidateProcess(String name) {
        this.name = name;
    }

    @Override
    public void onInit() {
        initialized++;
    }

    @Override
    public void onUpdate(long delta) {
        updated++;
    }

    @Override
    public void onFinish() {
        shutdown++;
    }
}
