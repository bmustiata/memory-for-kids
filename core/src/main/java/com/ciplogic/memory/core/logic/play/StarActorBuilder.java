package com.ciplogic.memory.core.logic.play;

import com.ciplogic.memory.core.actors.components.CompositeComponent;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.actors.components.SelfDestroyComponent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.IdGenerator;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2D;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;
import com.ciplogic.memory.core.view.play.component.MovingSpritesComponent;

import java.util.UUID;

public class StarActorBuilder {
    private final GameWorld gameWorld;

    public StarActorBuilder(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public Actor create(String starColor, String starY) {
        Actor result = new Actor(gameWorld, IdGenerator.INSTANCE.generateId());

        return result.withComponent(new CompositeComponent(result, false,
                    createStar(starColor, starY),
                    createStarShadow(starColor, starY)
                ))
                .withComponent(new ProcessManagerComponent(result))
                .withComponent(new SelfDestroyComponent(result, 10000));
    }

    private Actor createStar(String starColor, String starY) {
        Actor starActor = new Actor(gameWorld, "star");

        return starActor.withComponent(new Sprite2DComponent(starActor, -5, "play/background/" + starColor + "-star.png", 64, 64))
                .withComponent(new Size2DComponent(starActor, "0%", "5%", Size2D.KeepAspect.HEIGHT))
                .withComponent(new Position2DComponent(starActor, "56%", starY))
                .withComponent(new ProcessManagerComponent(starActor))
                .withComponent(new MovingSpritesComponent(starActor, 0));
    }

    private Actor createStarShadow(String starColor, String starY) {
        Actor starShadowActor = new Actor(gameWorld, "star-shadow");

        return starShadowActor.withComponent(new Sprite2DComponent(starShadowActor, -6, "play/background/shadow.png", 64, 64))
                .withComponent(new Size2DComponent(starShadowActor, "0%", "5%", Size2D.KeepAspect.HEIGHT))
                .withComponent(new Position2DComponent(starShadowActor, "56.2%", starY))
                .withComponent(new ProcessManagerComponent(starShadowActor))
                .withComponent(new MovingSpritesComponent(starShadowActor, 0.2f));
    }
}
