package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.view.util.StringUtil;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.actors.components.util.XmlUtil.intAttribute;

public class Sprite2DComponent extends ModelInstanceComponent {
    public Texture texture;
    public String textureId;
    public final float width;
    public final float height;
    public final Integer layer;

    public Sprite2DComponent(Actor actor, XmlReader.Element child) {
        super(actor, (XmlReader.Element) null);

        this.modelId = "core/sprite.g3dj";

        this.layer = intAttribute(child, "layer", "0");
        this.textureId = child.getAttribute("texture");
        this.width = floatAttribute(child, "width", "1");
        this.height = floatAttribute(child, "height", "1");

        this.setPosition(new Vector3(0, 0, -1));
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<sprite2d layer=\"")
                .append(layer)
                .append("\" texture=\"")
                .append(textureId)
                .append("\" width=\"")
                .append(width)
                .append("\" height=\"")
                .append(height)
                .append("\"/>\n");

    }

    public Sprite2DComponent(Actor actor, int layer, String textureId, float width, float height) {
        super(actor, (XmlReader.Element) null);

        this.modelId = "core/sprite.g3dj";

        this.layer = layer;
        this.textureId = textureId;
        this.width = width;
        this.height = height;

        this.setPosition(new Vector3(0, 0, -1));
    }

    public void setTexture(String textureId) {
        this.textureId = textureId;

        if (actor.gameWorld != null) {
            actor.gameWorld.reloadActor(actor);
        }
    }
}
