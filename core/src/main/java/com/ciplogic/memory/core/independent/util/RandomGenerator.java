package com.ciplogic.memory.core.independent.util;

import java.util.Random;

public class RandomGenerator {
    static Random random = new Random();

    public static long nextLong() {
        return random.nextLong();
    }

    public static int nextInt(int n) {
        return random.nextInt(n);
    }

    public static float nextFloat() {
        return random.nextFloat();
    }
}
