package com.ciplogic.memory.core.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.actors.components.DefaultComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * The default game world parser class. This basically reads a world.xml file, and
 * creates all the actors defined into it.
 */
public class GameWorldParser implements ActorFactory { // FIXME: move into independent.
    private XmlReader.Element element;
    private int index;
    private XmlActorBuilder xmlActorBuilder;
    private ExternActorParser externActorParser;
    private final String fileName;

    protected ComponentFactory componentFactory;

    public GameWorldParser(GameWorld gameWorld, String fileName) {
        this(gameWorld, fileName, new DefaultComponentFactory());
    }

    /**
     * In case custom components exist in the world, we should probably create them a bit differently.
     * @param gameWorld
     * @param fileName
     * @param componentFactory
     */
    public GameWorldParser(GameWorld gameWorld, String fileName, ComponentFactory componentFactory) {
        try {
            this.componentFactory = componentFactory;
            this.xmlActorBuilder = new XmlActorBuilder(componentFactory, gameWorld);
            this.externActorParser = new ExternActorParser(xmlActorBuilder);
            this.fileName = fileName;

            InternalFileHandleResolver internalFileResolver = new InternalFileHandleResolver();

            element = new XmlReader().parse(internalFileResolver.resolve(fileName));
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to parse file: " + fileName + " for the game world.", e);
        }
    }

    @Override
    public boolean hasNext() {
        return index < element.getChildCount();
    }

    @Override
    public Actor next() {
        XmlReader.Element childElement = element.getChild(index++);

        return createActor(childElement);
    }

    private boolean isCoreActor(String name) {
        return "text".equals(name) ||
                "loadFont".equals(name);
    }

    private String getWorldResourcesPath(String fileName) {
        return fileName.substring(0, fileName.length() - ".world.xml".length()) + "/";
    }

    @Override
    public Actor createActor(String resource, Map<String, String> actorParameters) {
        try {
            return externActorParser.createActor(resource, actorParameters);
        } catch (Throwable e) {
            String message = "Failure loading " + resource + " in world from " + fileName;
            Gdx.app.error("LightBox", message, e);
            throw new IllegalArgumentException(message, e);
        }
    }

    @Override
    public Actor createActor(XmlReader.Element actorElement) {
        try {
            return createActorFromElement(actorElement);
        } catch (Throwable e) {
            String elementName = actorElement != null ? actorElement.getName() : "<null-element>";
            String elementId = actorElement != null ? actorElement.getAttribute("id", "<null>") : "<null>";
            String message = "Failure loading " + elementName + "#" + elementId + " in world from " + fileName;
            Gdx.app.error("LightBox", message, e);
            throw new IllegalArgumentException(message, e);
        }
    }

    @Override
    public Actor createActor(String actorCode) {
        try {
            XmlReader.Element element = new XmlReader().parse(actorCode);

            return createActorFromElement(element);
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to parse XML code: " + actorCode, e);
        }
    }

    private Actor createActorFromElement(XmlReader.Element childElement) {
        if ("actor".equals(childElement.getName())) {
            return createInternalActor(childElement);
        } else {
            return createExternActor(childElement);
        }
    }

    private Actor createInternalActor(XmlReader.Element childElement) {
        return xmlActorBuilder.readActor(childElement);
    }

    private Actor createExternActor(XmlReader.Element childElement) {
        Map<String, String> actorParameters = new HashMap<String, String>();

        String resource;

        if (isCoreActor(childElement.getName())) {
            resource = "core/" + childElement.getName();
        } else {
            resource = getWorldResourcesPath(this.fileName) + childElement.getName();
        }

        ObjectMap<String, String> attributes = childElement.getAttributes();
        if (attributes != null) {
            for (ObjectMap.Entry<String, String> attribute : attributes) {
                actorParameters.put( attribute.key, attribute.value );
            }
        }

        return createActor(resource, actorParameters);
    }
}
