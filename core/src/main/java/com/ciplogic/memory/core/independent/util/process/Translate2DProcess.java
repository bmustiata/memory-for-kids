package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.time.TimeShift;

public class Translate2DProcess extends GameProcess {
    private final long totalMillisDuration;

    private final Position2DComponent position2DComponent;

    private final float endX;
    private final float endY;

    private float startX;
    private float startY;

    private long startingDelta;

    private TimeShift[] timeShifts = new TimeShift[] {
        TimeShift.LINEAR,
        TimeShift.LINEAR
    };

    /**
     *
     * @param position2DComponent
     * @param endX
     * @param endY
     * @param millisDuration
     * @param timeShifts The X, and Y time shifts respectively.
     */
    public Translate2DProcess(Position2DComponent position2DComponent, float endX, float endY, long millisDuration, TimeShift ... timeShifts) {
        this.totalMillisDuration = millisDuration;

        this.position2DComponent = position2DComponent;
        this.endX = endX;
        this.endY = endY;

        for (int i = 0; i < timeShifts.length; i++) {
            this.timeShifts[i] = timeShifts[i];
        }
    }

    @Override
    public void onInit(long delta) {
        super.onInit(delta);

        this.startingDelta = delta;

        startX = position2DComponent.position2D.x.value;
        startY = position2DComponent.position2D.y.value;
    }

    @Override
    public void onUpdate(long delta) {
        if (delta - startingDelta >= totalMillisDuration) {
            finish();

            return;
        }

        position2DComponent.position2D.x.value = (endX - startX) * timeShifts[0].compute(startingDelta, totalMillisDuration, delta) + startX;
        position2DComponent.position2D.y.value = (endY - startY) * timeShifts[1].compute(startingDelta, totalMillisDuration, delta) + startY;

        position2DComponent.restart();
    }

    @Override
    public void onFinish() {
        position2DComponent.position2D.x.value = endX;
        position2DComponent.position2D.y.value = endY;

        position2DComponent.restart();
    }
}
