package com.ciplogic.memory.core.test;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyTypedEvent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.play.component.event.GameWonEvent;
import com.ciplogic.memory.core.view.play.component.event.TimedGameWonEvent;

import java.util.Collections;
import java.util.List;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class TestCodeActorComponent extends ActorComponent implements EventListener {

    private long startingDelta = -1;
    private boolean eventFired = false;
    private boolean processedFire = false;

    public TestCodeActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<testCode/>\n");
    }

    @Override
    public void onUpdate(long delta) {
        if (startingDelta < 0) {
            startingDelta = delta;
        }

        if (delta - startingDelta > 1000 && ! eventFired) { // more then one second passed
            EventBus.INSTANCE.fire( new GameWonEvent("flowers", 12) );
            EventBus.INSTANCE.fire( new TimedGameWonEvent(1) );

            eventFired = true;
        }

        if (delta - startingDelta > 2000 && ! processedFire) { // more then two seconds passed
            // the dialog should close.
            processedFire = true;
            //EventBus.INSTANCE.fire( new AchievementProcessedEvent() );
        }
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof KeyTypedEvent) {
            KeyTypedEvent keyTypedEvent = (KeyTypedEvent) event;
            if (keyTypedEvent.keyTyped == 'r') {
                EventBus.INSTANCE.fire( new GameWonEvent("flowers", 12) );
                EventBus.INSTANCE.fire( new TimedGameWonEvent(1) );
            }
        }

    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(KeyTypedEvent.class);
    }
}
