package com.ciplogic.memory.core.independent.gdxinput;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class GdxInputSetupProcess extends GameProcess {
    private static boolean initialized = false;

    @Override
    public void onInit() {
        assert ! initialized;

        initialized = true; // only a single instance of GdxInputSetupProcess should exist.

        Gdx.input.setInputProcessor(new InputMultiplexer());
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onFinish() {
        assert initialized;

        initialized = false;
    }
}
