package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ScreenActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class Position2DComponent extends ScreenActorComponent {
    public String valign = "middle";
    public String align = "center";

    public Position2D position2D;

    private Sprite2DComponent sprite2d;

    public Position2DComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        position2D = Position2D.readFromNode(child);
        sprite2d = actor.getComponent(Sprite2DComponent.class);

        valign = child.getAttribute("valign", "middle");
        align = child.getAttribute("align", "center");
    }

    public Position2DComponent(Actor actor, String x, String y) {
        super(actor, null);

        position2D = Position2D.readFromValues(x, y);
        sprite2d = actor.getComponent(Sprite2DComponent.class);
    }

    @Override
    public void onInit() {
        restart();
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<position2d x=\"")
                .append(position2D.x)
                .append("\" y=\"")
                .append(position2D.y)
                .append("\" align=\"")
                .append(align)
                .append("\" valign=\"")
                .append(valign)
                .append("\" screen=\"")
                .append(screenOrientation.toString().toLowerCase())
                .append("\"/>\n");
    }

    @Override
    public void restart() {
        float deltaX, deltaY;
        Size2D size2d = actor.getComponent(Size2DComponent.class).size2D;

        if ("left".equals(align)) {
            deltaX = size2d.getScreenWidth() / 2;
        } else if ("right".equals(align)) {
            deltaX = size2d.getScreenWidth() / -2;
        } else {
            deltaX = 0;
        }

        if ("top".equals(align)) {
            deltaY = size2d.getScreenHeight() / -2;
        } else if ("bottom".equals(align)) {
            deltaY = size2d.getScreenHeight() / 2;
        } else {
            deltaY = 0;
        }

        if (sprite2d != null) {
            sprite2d.setPosition(new Vector3(position2D.getScreenX() + deltaX,
                    position2D.getScreenY() + deltaY, -1));
        }
    }
}
