package com.ciplogic.memory.core.actors;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

public interface ComponentFactory {
    ActorComponent fromXmlNode(Actor actor, XmlReader.Element child);
}
