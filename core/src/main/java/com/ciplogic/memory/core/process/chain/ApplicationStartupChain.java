package com.ciplogic.memory.core.process.chain;

import com.ciplogic.memory.core.independent.application.process.BuilderProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.gdxinput.GdxInputSetupProcess;
import com.ciplogic.memory.core.independent.gdxinput.keyboard.GdxKeyboardInputProcess;
import com.ciplogic.memory.core.independent.gdxinput.mouse.GdxMouseInputProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.*;
import com.ciplogic.memory.core.independent.view.ScreenOrientationProcess;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.logic.RestartGameWorldProcess;
import com.ciplogic.memory.core.logic.loading.CloseLoadingGameWorldProcess;
import com.ciplogic.memory.core.logic.loading.LoadingGameWorld;
import com.ciplogic.memory.core.view.loading.LoadingHumanView;

public class ApplicationStartupChain {
    private GameWorld gameWorld;
    private HumanGameView gameView;

    public ApplicationStartupChain(GameWorld gameWorld, HumanGameView gameView) {
        this.gameWorld = gameWorld;
        this.gameView = gameView;
    }

    public GameProcess create() {
        ClearScreenProcess clearScreenProcess;
        RenderHumanViewProcess renderProcess;

        return new BuilderProcess().then(
            new GdxInputSetupProcess(),
            new GdxKeyboardInputProcess(),
            new GdxMouseInputProcess(),
            new ScreenOrientationProcess(),
            clearScreenProcess = new ClearScreenProcess(0.7f, 0.7f, 0.7f),
            new EnsureGameWorldLoadedProcess(LoadingGameWorld.INSTANCE).then(
                new EnsureActorsModelsLoadedProcess(LoadingGameWorld.INSTANCE).then(
                    new RestartGameWorldProcess(LoadingGameWorld.INSTANCE),
                    new FinalizeProcess(clearScreenProcess),
                    renderProcess = new RenderHumanViewProcess(new LoadingHumanView()),
                    new EnsureGameWorldLoadedProcess(gameWorld).then(
                        new EnsureActorsModelsLoadedProcess(gameWorld).then(
                            new RestartGameWorldProcess(gameWorld).then(
                                new CloseLoadingGameWorldProcess().then(
                                    new FinalizeProcess(clearScreenProcess),
                                    new FinalizeProcess(renderProcess).then(
                                        renderProcess = new RenderHumanViewProcess(gameView)
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }
}
