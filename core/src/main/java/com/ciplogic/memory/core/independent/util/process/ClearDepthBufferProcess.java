package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class ClearDepthBufferProcess extends GameProcess {
    @Override
    public void onUpdate(long delta) {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
    }
}
