package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.utils.RenderableSorter;
import com.badlogic.gdx.utils.Array;
import com.ciplogic.memory.core.actors.components.ColorComponent;
import com.ciplogic.memory.core.actors.components.TextComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.independent.view.sprite.Position2D;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2D;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;

import java.util.*;

public class RenderSpritesProcess extends GameProcess implements RenderableSorter, EventListener {
    private final Map<Integer, Map<String, RenderableProvider>> spriteLayers;
    private final Map<Integer, Map<String, TextComponent>> textLayers;

    private ModelBatch modelBatch;
    private Batch textBatch;

    private OrthographicCamera camera;

    private Set<Integer> layers = new TreeSet<Integer>();

    public RenderSpritesProcess(Map<Integer, Map<String, RenderableProvider>> spriteLayers,
                                Map<Integer, Map<String, TextComponent>> textLayers) {
        this.spriteLayers = spriteLayers;
        this.textLayers = textLayers;
    }

    @Override
    public void onInit() {
        modelBatch = new ModelBatch(this);
        textBatch = new SpriteBatch();

        this.camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onUpdate(long delta) {
        Gdx.gl.glDisable(GL20.GL_DEPTH_BUFFER_BIT);

        layers.clear();
        layers.addAll(spriteLayers.keySet());
        layers.addAll(textLayers.keySet());

        int screenHeight = Gdx.app.getGraphics().getHeight();

        for (Integer layer : layers) {
            modelBatch.begin( camera );
            textBatch.begin();

            Map<String, RenderableProvider> spriteLayer = spriteLayers.get(layer);
            if (spriteLayer != null) {
                modelBatch.render(spriteLayer.values());
            }

            Map<String, TextComponent> textLayer = textLayers.get(layer);
            if (textLayer != null) {
                for (TextComponent textComponent : textLayer.values()) {
                    Position2D position = textComponent.actor.getComponent(Position2DComponent.class).position2D;
                    Size2D size2d = textComponent.actor.getComponent(Size2DComponent.class).size2D;
                    ColorComponent colorComponent = textComponent.actor.getComponent(ColorComponent.class);

                    BitmapFont bitmapFont = textComponent.font.bitmapFont;
                    float scaleX = textComponent.font.xScaling;
                    float scaleY = textComponent.font.yScaling;
                    float scaledScreenHeight = scaleY * screenHeight;

                    bitmapFont.setScale(scaleX * size2d.getScreenWidth() / textComponent.font.fontSize,
                            scaleY * size2d.getScreenHeight() / textComponent.font.fontSize);

                    if (colorComponent != null) {
                        bitmapFont.setColor(colorComponent.red,
                                colorComponent.green,
                                colorComponent.blue,
                                colorComponent.alpha);
                    } else {
                        bitmapFont.setColor(Color.WHITE);
                    }

                    // for some reason draw multi line works in a different axis on Y, so we need
                    // to adjust the fonts position.
                    bitmapFont.drawMultiLine(textBatch,
                            textComponent.text.replaceAll("\\\\n", "\n"),
                            position.getScreen2dX() * textComponent.font.xScaling,
                            scaledScreenHeight -  position.getScreen2dY() * textComponent.font.yScaling);

                    bitmapFont.setScale(scaleX, scaleY);
                }
            }

            modelBatch.end();
            textBatch.end();
        }

        Gdx.gl.glEnable(GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void onFinish() {
        modelBatch.dispose();
        modelBatch = null;

        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void sort(Camera camera, Array<Renderable> renderables) {
        // noop on purpose, should be already sorted, by order of addition.
    }

    @Override
    public void onEvent(Event event) {
        this.camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(ScreenResizeEvent.class);
    }
}
