package com.ciplogic.memory.core.logic;

import java.util.Map;

public class StringTemplate {
    public static String format(String source, Map<String, String> variables) {
        int startIndex = -1;
        int foundIndex, endIndex;
        String variableName;

        for (Map.Entry<String, String> entry : variables.entrySet()) {
            if (entry.getValue() == null) {
                throw new IllegalArgumentException("Value is null for parameter " +
                        entry.getKey() + " in format() function. ");
            }
        }

        while( (foundIndex = source.indexOf("{", startIndex)) >= 0 ) {
            endIndex = source.indexOf("}", foundIndex + 1);

            if (endIndex < 0) { // no more variables to find.
                break;
            }

            variableName = source.substring(foundIndex + 1, endIndex);

            if (!variables.containsKey(variableName)) { // if there is no such variable, just skip and keep its value
                startIndex = endIndex + 1;
                continue;
            }

            String variableValue = variables.get(variableName);

            source = source.substring(0, foundIndex) + variableValue + source.substring(endIndex + 1);

            // since the variable is replaced, the start Index should be updated with the length of the replacement.
            startIndex = foundIndex + variableValue.length();
        }

        return source;
    }
}
