package com.ciplogic.memory.core.independent.logic.actor;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ActorFactory;

import java.util.Map;

public class EmptyGameWorld extends GameWorld {
    @Override
    public ActorFactory getActorFactory() {
        return new ActorFactory() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Actor next() {
                return null;
            }

            @Override
            public Actor createActor(String resource, Map<String, String> actorParameters) {
                return null;
            }

            @Override
            public Actor createActor(XmlReader.Element actorElement) {
                return null;
            }

            @Override
            public Actor createActor(String actorCode) {
                return null;
            }
        };
    }
}
