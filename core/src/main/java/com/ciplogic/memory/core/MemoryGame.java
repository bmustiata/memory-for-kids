package com.ciplogic.memory.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.process.GlobalGameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.OpenGlContextLostEvent;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.logic.play.PlayGameWorld;
import com.ciplogic.memory.core.process.chain.ApplicationStartupChain;
import com.ciplogic.memory.core.view.play.PlayGameHumanView;

public class MemoryGame implements ApplicationListener {
	float elapsed;

    GlobalGameProcessManager gameProcessManager = new GlobalGameProcessManager();

    @Override
	public void create () {
        gameProcessManager.addProcess(new ApplicationStartupChain(PlayGameWorld.INSTANCE, new PlayGameHumanView()).create());
        gameProcessManager.addProcess(new SimulatePauseResumeProcess( this ));
    }

    @Override
	public void resize (int width, int height) {
        EventBus.INSTANCE.fire( new ScreenResizeEvent(width, height) );
	}

	@Override
	public void render () {
		elapsed += Gdx.graphics.getDeltaTime();
        gameProcessManager.updateProcesses(System.currentTimeMillis());
	}

	@Override
	public void pause () {
        EventBus.INSTANCE.fire(new OpenGlContextLostEvent());
        gameProcessManager.shutdown();
	}

	@Override
	public void resume () {
        gameProcessManager.restart();
        gameProcessManager.addProcess(new ApplicationStartupChain(PlayGameWorld.INSTANCE, new PlayGameHumanView()).create());
	}

	@Override
	public void dispose () {
	}
}
