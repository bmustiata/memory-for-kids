package com.ciplogic.memory.core.logic;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.IdGenerator;

/**
 * Builds an actor from a XML node, including its components.
 */
public class XmlActorBuilder {
    private final ComponentFactory componentFactory;
    private final GameWorld gameWorld;

    public XmlActorBuilder(ComponentFactory componentFactory, GameWorld gameWorld) {
        this.componentFactory = componentFactory;
        this.gameWorld = gameWorld;
    }

    public Actor readActor(XmlReader.Element actorElement) {
        return readActor(actorElement, null);
    }

    public Actor readActor(XmlReader.Element actorElement, String actorId) {
        String defaultId = actorId != null ? actorId :  IdGenerator.INSTANCE.generateId();
        Actor actor = new Actor(gameWorld, actorElement.getAttribute("id", defaultId));
        actor.gameWorld = gameWorld;

        for (int i = 0; i < actorElement.getChildCount(); i++) {
            actor.withComponent(componentFactory.fromXmlNode(actor, actorElement.getChild(i)));
        }

        return actor;
    }
}
