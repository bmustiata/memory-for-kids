package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class PreferencesComponent extends ActorComponent {
    public Preferences preferences;

    public PreferencesComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<preferences/>\n");
    }

    @Override
    public void onInit() {
        preferences = Gdx.app.getPreferences("memory-preferences");
    }

    public void flush() {
        preferences.flush();
    }

    public String getString(String key, String defaultValue) {
        try {
            return preferences.getString(key, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public void putString(String key, String value) {
        try {
            preferences.putString(key, value);
        } catch (Throwable e) {
            // FIXME: only on development mode this should not fail.
            e.printStackTrace();
        }
    }
}
