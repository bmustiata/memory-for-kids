package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;

public class PositionComponent extends ActorComponent {
    public Positionable positionable;
    public Vector3 original_position;

    public PositionComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        for (ActorComponent component : actor.components.values()) {
            if (component instanceof Positionable) {
                positionable = (Positionable) component;
            }
        }

        assert positionable != null;

        original_position = new Vector3(
                floatAttribute(child, "x", "0"),
                floatAttribute(child, "y", "0"),
                floatAttribute(child, "z", "0"));

        positionable.setPosition(original_position);
    }

    public PositionComponent(Actor actor, float x, float y, float z) {
        super(actor, null);

        for (ActorComponent component : actor.components.values()) {
            if (component instanceof Positionable) {
                positionable = (Positionable) component;
            }
        }

        assert positionable != null;

        original_position = new Vector3(x, y, z);

        positionable.setPosition(original_position);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<position x=\"" +
                    positionable.getPosition().x +
                    "\" y=\"" +
                    positionable.getPosition().y +
                    "\" z=\"" +
                    positionable.getPosition().z +
                    "\"/>\n");

    }

    @Override
    public void restart() {
//        positionable.setPosition(original_position);
    }
}
