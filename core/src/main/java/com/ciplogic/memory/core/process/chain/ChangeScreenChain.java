package com.ciplogic.memory.core.process.chain;

import com.ciplogic.memory.core.independent.application.process.BuilderProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.*;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.logic.RestartGameWorldProcess;
import com.ciplogic.memory.core.logic.loading.CloseLoadingGameWorldProcess;
import com.ciplogic.memory.core.logic.loading.LoadingGameWorld;
import com.ciplogic.memory.core.view.loading.LoadingHumanView;

public class ChangeScreenChain {
    private final GameWorld gameWorld;
    private final HumanGameView humanGameView;

    public ChangeScreenChain(GameWorld gameWorld, HumanGameView humanGameView) {
        this.gameWorld = gameWorld;
        this.humanGameView = humanGameView;
    }

    public GameProcess create() {
        RenderHumanViewProcess renderProcess = RenderHumanViewProcess.INSTANCE;

//        if (gameWorld.previouslyLoaded) {
//            return createQuickLoading(renderProcess);
//        } else {
            return createNewLoading(renderProcess);
//        }
    }

    private GameProcess createQuickLoading(RenderHumanViewProcess renderProcess) {
        ClearScreenProcess clearScreenProcess;

        return new BuilderProcess().then(
            clearScreenProcess = new ClearScreenProcess(0.7f, 0.7f, 0.7f),
            new FinalizeProcess(renderProcess).then(
                new EnsureGameWorldLoadedProcess(gameWorld).then(
                    new EnsureActorsModelsLoadedProcess(gameWorld).then(
                        new RestartGameWorldProcess(gameWorld).then(
                            new FinalizeProcess(clearScreenProcess).then(
                                renderProcess = new RenderHumanViewProcess(humanGameView)
                            )
                        )
                    )
                )
            )
        );
    }

    private GameProcess createNewLoading(RenderHumanViewProcess renderProcess) {
        ClearScreenProcess clearScreenProcess;
        return new BuilderProcess().then(
            clearScreenProcess = new ClearScreenProcess(0.7f, 0.7f, 0.7f),
            new FinalizeProcess(renderProcess).then(
                new EnsureGameWorldLoadedProcess(LoadingGameWorld.INSTANCE).then(
                    new EnsureActorsModelsLoadedProcess(LoadingGameWorld.INSTANCE).then(
                        new RestartGameWorldProcess(LoadingGameWorld.INSTANCE),
                        new FinalizeProcess(clearScreenProcess),
                        renderProcess = new RenderHumanViewProcess(new LoadingHumanView()),
                        new EnsureGameWorldLoadedProcess(gameWorld).then(
                            new EnsureActorsModelsLoadedProcess(gameWorld).then(
                                new RestartGameWorldProcess(gameWorld).then(
                                    new CloseLoadingGameWorldProcess().then(
                                        new FinalizeProcess(clearScreenProcess),
                                        new FinalizeProcess(renderProcess).then(
                                            renderProcess = new RenderHumanViewProcess(humanGameView)
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
    }
}
