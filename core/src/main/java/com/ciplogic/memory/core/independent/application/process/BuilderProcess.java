package com.ciplogic.memory.core.independent.application.process;

/**
 * An empty process that is used just to construct a list of processes to be executed.
 */
public class BuilderProcess extends GameProcess {
    @Override
    public void onUpdate(long delta) {
        finish();
    }
}
