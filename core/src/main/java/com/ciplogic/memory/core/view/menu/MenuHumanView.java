package com.ciplogic.memory.core.view.menu;
// FIXME: this package is definitelly not core.

import com.badlogic.gdx.Input;
import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyDownEvent;
import com.ciplogic.memory.core.independent.application.input.event.mouse.TouchDownEvent;
import com.ciplogic.memory.core.independent.application.process.BuilderProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.DefaultGameWorldProcess;
import com.ciplogic.memory.core.independent.util.process.FlushPreferencesOnShutdownProcess;
import com.ciplogic.memory.core.independent.util.process.event.GameWorldLoadedEvent;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.logic.menu.MenuWorld;
import com.ciplogic.memory.core.view.chest.ChestHumanView;
import com.ciplogic.memory.core.view.menu.component.MenuOptionComponent;
import com.ciplogic.memory.core.view.menu.component.MenuOptionSelectionComponent;
import com.ciplogic.memory.core.view.play.PlayGameHumanView;
import com.ciplogic.memory.core.view.util.HumanViewSwitcher;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.memory.core.view.util.SpriteTest.isActorInTouchEvent;

public class MenuHumanView extends HumanGameView implements EventListener {
    private static final String[] MENU_ACTORS = {
        "deckOption",
        "pairsOption"
//        "soundOption",
//        "musicOption"
    };

    private MenuWorld gameWorld;

    public MenuHumanView() {
        this.gameWorld = MenuWorld.INSTANCE;
    }

    @Override
    public void onInit() {
        CameraComponent camera = gameWorld.get("camera").getComponent(CameraComponent.class);

        camera.fitBoth(16f, 16f);
        camera.resetCamera();

        EventBus.INSTANCE.registerListener(this);
        gameProcessManager.addProcess(
            new BuilderProcess().then(
                new DefaultGameWorldProcess(gameWorld),
                new FlushPreferencesOnShutdownProcess(gameWorld)
            )
        );
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        gameProcessManager.shutdown();
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof TouchDownEvent) {
            MenuOptionComponent menuOptionComponent = findSelectedMenu((TouchDownEvent) event);
            if (menuOptionComponent != null) {
                MenuOptionSelectionComponent selectionComponent = findSelectionComponent();

                if (selectionComponent.selected == menuOptionComponent) {
                    menuOptionComponent.setNextValue();
                }

                selectionComponent.selectOption(menuOptionComponent);
            }

            if (isBackButtonClicked((TouchDownEvent) event)) {
                saveSettings();
                goToGameScreen();
            }

            if (isTreasureChestButtonClicked((TouchDownEvent) event)) {
                goToTreasureChestScreen();
            }
        }

        if (event instanceof KeyDownEvent) {
            KeyDownEvent keyDownEvent = (KeyDownEvent) event;

            switch (keyDownEvent.keyCode) {
                case Input.Keys.UP:    selectPreviousOption(); break;
                case Input.Keys.DOWN:  selectNextOption(); break;
                case Input.Keys.ENTER: changeSelectedOption(); break;
            }
        }

        if (event instanceof ScreenResizeEvent) {
            gameWorld.get("camera").getComponent(CameraComponent.class).resetCamera();
        }

        if (event instanceof GameWorldLoadedEvent && // is the current game world reloaded
           ((GameWorldLoadedEvent)event).gameWorld == gameWorld) {
            gameWorld.get("camera").getComponent(CameraComponent.class).resetCamera();
        }
    }

    private void goToTreasureChestScreen() {
        HumanViewSwitcher.changeHumanView(new ChestHumanView());
    }

    private boolean isTreasureChestButtonClicked(TouchDownEvent event) {
        return isActorInTouchEvent(gameWorld.get("button-treasure-chest"), event);
    }

    private boolean isBackButtonClicked(TouchDownEvent event) {
        return isActorInTouchEvent(gameWorld.get("backButton"), event);
    }

    private void goToGameScreen() {
        HumanViewSwitcher.changeHumanView(new PlayGameHumanView());
    }

    private void saveSettings() {
    }

    private void selectPreviousOption() {
        MenuOptionSelectionComponent selectionComponent = findSelectionComponent();
        String actorName;

        if (selectionComponent.selected == null) {
            actorName = MENU_ACTORS[MENU_ACTORS.length - 1];
        } else {
            int index = menuOptionIndexOf(selectionComponent.selected.actor.id);
            actorName =  index == 0 ? MENU_ACTORS[MENU_ACTORS.length - 1] : MENU_ACTORS[index - 1];
        }

        selectionComponent.selectOption( getMenuOption(actorName) );
    }

    private void selectNextOption() {
        MenuOptionSelectionComponent selectionComponent = findSelectionComponent();
        String actorName;

        if (selectionComponent.selected == null) {
            actorName = MENU_ACTORS[0];
        } else {
            int index = menuOptionIndexOf(selectionComponent.selected.actor.id);
            actorName =  index == MENU_ACTORS.length - 1 ? MENU_ACTORS[0] : MENU_ACTORS[index + 1];
        }

        selectionComponent.selectOption( getMenuOption(actorName) );
    }

    private void changeSelectedOption() {
        MenuOptionSelectionComponent selectionComponent = findSelectionComponent();

        if (selectionComponent.selected != null) {
            selectionComponent.selected.setNextValue();
        }
    }

    private int menuOptionIndexOf(String id) {
        for (int i = 0; i < MENU_ACTORS.length; i++) {
            if (MENU_ACTORS[i].equals(id)) {
                return i;
            }
        }

        return -1;
    }

    private MenuOptionComponent getMenuOption(String actorName) {
        return gameWorld.get(actorName).getComponent(MenuOptionComponent.class);
    }

    private MenuOptionSelectionComponent findSelectionComponent() {
        return gameWorld.get("currentSelection").getComponent(MenuOptionSelectionComponent.class);
    }

    private MenuOptionComponent findSelectedMenu(TouchDownEvent event) {
        for (String menuActor : MENU_ACTORS) {
            Actor actor = gameWorld.get(menuActor);

            if (isActorInTouchEvent(actor, event)) {
                return actor.getComponent( MenuOptionComponent.class );
            }
        }

        return null;
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List<Class<T>> items = new ArrayList<Class<T>>();

        items.add((Class<T>) TouchDownEvent.class);
        items.add((Class<T>) KeyDownEvent.class);
        items.add((Class<T>) ScreenResizeEvent.class);
        items.add((Class<T>) GameWorldLoadedEvent.class);

        return items;
    }

    @Override
    public GameWorld getGameWorld() {
        return gameWorld;
    }
}
