package com.ciplogic.memory.core.view.menu.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.IdGenerator;
import com.ciplogic.memory.core.independent.util.RandomGenerator;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2D;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class CloudGeneratorComponent extends ActorComponent {
    public static final int EVERY_HOW_MANY_MILLIS = 1000;

    private IdGenerator cloudIdGenerator = new IdGenerator("generated-id-cloud-");

    private long lastDelta = 0;

    enum PossibleCloud {
        CLOUD1("menu/cloud1.png", 370, 215),
        CLOUD2("menu/cloud2.png", 371, 228),
        CLOUD3("menu/cloud3.png", 538, 237),
        CLOUD4("menu/cloud4.png", 441, 280);

        public final String name;
        public final float width;
        public final float height;

        PossibleCloud(String name, float width, float height) {
            this.name = name;
            this.width = width;
            this.height = height;
        }
    }

    public CloudGeneratorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        lifecycled = true;
    }

    @Override
    public void onUpdate(long delta) {
        if (delta - lastDelta < 0) {
            lastDelta = delta;
        }

        if (delta - lastDelta > EVERY_HOW_MANY_MILLIS) {
            generateCloud();
            lastDelta = delta;
        }

        if (delta - lastDelta <= 0) { // check if the timer was restarted.
            lastDelta = delta;
        }
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<cloudGenerator/>\n");
    }

    private void generateCloud() {
        /**
         *         <sprite2d texture="menu/cloud1.png" width="416" height="212"/>
         <position2d x="35%" y="30%"/>
         <size2d width="20%" height="0%" keepAspect="true"/>
         <movingCloud/>
         */

        Actor cloud = new Actor(actor.gameWorld, cloudIdGenerator.generateId());
        PossibleCloud possibleCloud = getPossibleCloud();
        cloud.withComponent(new Sprite2DComponent(cloud, 10, possibleCloud.name, possibleCloud.width, possibleCloud.height))
                .withComponent(new Size2DComponent(cloud, "0%", getRandomSize(), Size2D.KeepAspect.HEIGHT))
                .withComponent(new Position2DComponent(cloud, getRandomXPosition(), getRandomYPOsition()))
                .withComponent(new MovingCloudComponent(cloud, getCloudSpeed()));

        actor.gameWorld.addActor(cloud);
    }

    private long getCloudSpeed() {
        return 20000 + (RandomGenerator.nextLong() % 5000);
    }

    private PossibleCloud getPossibleCloud() {
        return PossibleCloud.values()[ RandomGenerator.nextInt(PossibleCloud.values().length) ];
    }

    private String getRandomXPosition() {
        return 90 + RandomGenerator.nextInt(10) + "%"; // to get a different cloud speed.
    }

    private String getRandomSize() {
        return 15 + RandomGenerator.nextInt(10) + "%";
    }

    private String getRandomYPOsition() {
        return 10 + RandomGenerator.nextInt(38) + "%";
    }
}
