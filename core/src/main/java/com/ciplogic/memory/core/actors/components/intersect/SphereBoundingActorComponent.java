package com.ciplogic.memory.core.actors.components.intersect;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.util.XmlUtil;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

/**
 * A component that allows creating sphere bounding
 * containers for simple collision testing.
 */
public class SphereBoundingActorComponent extends ActorComponent {
    public float radius = 1.0f;

    public SphereBoundingActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        radius = XmlUtil.floatAttribute(child, "radius", "1.0");
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<sphereBounding radius=\"")
                .append(radius)
                .append("\"/>\n");
    }
}
