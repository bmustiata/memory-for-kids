package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class DelayProcess extends GameProcess {
    private final long howLongMillis;
    private long startingDelta;

    public DelayProcess(long howLongMillis) {
        this.howLongMillis = howLongMillis;
    }

    @Override
    public void onInit(long startingDelta) {
        super.onInit(startingDelta);

        this.startingDelta = startingDelta;
    }

    @Override
    public void onUpdate(long delta) {
        if (delta - startingDelta >= howLongMillis) {
            finish();
        }
    }
}
