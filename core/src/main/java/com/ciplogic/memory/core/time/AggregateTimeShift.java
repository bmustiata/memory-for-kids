package com.ciplogic.memory.core.time;

import java.util.ArrayList;
import java.util.List;

public class AggregateTimeShift extends TimeShift {
    class ManagedTimeShift {
        long startTime;
        TimeShift timeShift;
        long duration;
        float weight;
        float startValue;

        ManagedTimeShift(TimeShift timeShift, long duration, long startTime) {
            this.timeShift = timeShift;
            this.duration = duration;
            this.startTime = startTime;
        }
    }

    private List<ManagedTimeShift> managedTimeShifts = new ArrayList<ManagedTimeShift>();
    private boolean computed;

    public AggregateTimeShift addTimeShift(TimeShift timeShift, long duration) {
        long startTime;

        computed = false;

        if (!this.managedTimeShifts.isEmpty()) {
            ManagedTimeShift lastTimeShift = this.managedTimeShifts.get(this.managedTimeShifts.size() - 1);
            startTime = lastTimeShift.startTime + lastTimeShift.duration;
        } else {
            startTime = 0;
        }

        this.managedTimeShifts.add(new ManagedTimeShift(timeShift, duration, startTime));

        computed = true;

        return this;
    }

    public AggregateTimeShift compute() {
        long totalDuration = 0;
        float startValue = 0f;

        for (ManagedTimeShift managedTimeShift : managedTimeShifts) {
            totalDuration += managedTimeShift.duration;
        }

        for (ManagedTimeShift managedTimeShift : managedTimeShifts) {
            managedTimeShift.weight = ((float)managedTimeShift.duration) / ((float)totalDuration);
            managedTimeShift.startValue = startValue;
            startValue += managedTimeShift.weight;
        }

        computed = true;

        return this;
    }

    @Override
    public float compute(long start, long duration, long current) {
        if (!computed) {
            throw new IllegalStateException("Call .compute() first");
        }

        if (start + duration < current) {
            return 1.0f;
        }

        ManagedTimeShift selectedTimeShift = null;
        for (ManagedTimeShift managedTimeShift : managedTimeShifts) {
            if (managedTimeShift.startTime <= current - start ) {
                selectedTimeShift = managedTimeShift;
            } else {
                break;
            }
        }

        return selectedTimeShift.startValue +
               selectedTimeShift.timeShift.compute( selectedTimeShift.startTime, selectedTimeShift.duration, current - start) *
               selectedTimeShift.weight;
    }
}
