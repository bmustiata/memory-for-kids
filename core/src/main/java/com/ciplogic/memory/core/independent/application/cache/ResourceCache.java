package com.ciplogic.memory.core.independent.application.cache;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.utils.Disposable;

import java.util.Map;
import java.util.Set;

public class ResourceCache implements Disposable {
    AssetManager assetManager = new AssetManager();

    // FIXME: add some production, to actually die in case the resources are not there
    public Model get(String name) {
        try {
            return assetManager.get(name, Model.class);
        } catch (Throwable t) {
            Gdx.app.log("LightBox", "Resource not loaded!: " + name);
            return null;
        }
    }

    public void load(Map<Class, Set<String>> usedResourcesIds) {
        for (Map.Entry<Class, Set<String>> entry : usedResourcesIds.entrySet()) {
            for (String resource : entry.getValue()) {
                try {
                    assetManager.load(resource, entry.getKey());
                } catch (Throwable e) {
                    throw new IllegalStateException("Unable to load resource: " + resource, e);
                }
            }
        }
    }

    public boolean isLoaded() {
        return assetManager.update();
    }


    @Override
    public void dispose() {
        assetManager.dispose();
    }

    // FIXME: add some production, to actually die in case the resources are not there
    public Texture getTexture(String textureId) {
        try {
            return assetManager.get(textureId, Texture.class);
        } catch (Throwable t) {
            Gdx.app.log("LightBox", "Texture not loaded!: " + textureId);
            return null;
        }
    }
}
