package com.ciplogic.memory.core.independent.logic.actor;

import java.util.*;

public class LifecycleActorsCollection implements Iterable<Actor> {
    private List<Actor> actorList = new ArrayList<Actor>();

    void put(String actorId, Actor actor) {
        actorList.add(actor);
    }

    void remove(String actorId) {
        actorList.remove( findActorIndex(actorId) );
    }

    private int findActorIndex(String actorId) {
        for (int i = 0; i < actorList.size(); i++) {
            if (actorList.get(i).id.equals(actorId)) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public Iterator<Actor> iterator() {
        return new Iterator<Actor>() {
            int index = actorList.size() - 1;

            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public Actor next() {
                index = index >= actorList.size() ? index = actorList.size() - 1 : index;

                return actorList.get( index-- );
            }

            @Override
            public void remove() {
                actorList.remove(index);
            }
        };
    }
}
