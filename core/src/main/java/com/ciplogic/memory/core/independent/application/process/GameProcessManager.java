package com.ciplogic.memory.core.independent.application.process;

import com.ciplogic.memory.core.lang.ModifiableLinkedHashMap;

import java.util.Map;

/**
 * <p>A GameProcessManager manages a bunch of processes, and calls their lifecycle functions.</p>
 * <p>The key functions are updateProcesses and shutdown.</p>
 * <p>The updateProcesses is the function that is normally called in a loop, and updates
 * the processes according to their lifecycle.</p>
 * <p>In case the game process manager goes down, the shutdown method will still run the full
 * process flows, including all the dependent processes, but without calling the update method.</p>
 */
public class GameProcessManager {
    private Map<Integer, GameProcess> gameProcesses = new ModifiableLinkedHashMap<Integer, GameProcess>();
    private int currentPid = 1;

    public void updateProcesses(long delta) {
        for (GameProcess process : gameProcesses.values()) {
            if (process.state == GameProcess.State.UNINITIALIZED) {
                //Gdx.app.log("Memory:", "Starting process " + process);
                process.state = GameProcess.State.INITIALIZING;

                process.onInit( delta );

                // if the onInit changed the state, use that state instead, otherwise it's running.
                if (process.state == GameProcess.State.INITIALIZING) {
                    process.state = GameProcess.State.RUNNING;
                }
            }

            if (process.state == GameProcess.State.RUNNING) {
                process.onUpdate(delta);
            }

            if (process.state == GameProcess.State.FINISHING) {
                gameProcesses.remove( process.pid );

                process.onFinish();

                // if the onFinish changed the state, then use that state.
                if (process.state == GameProcess.State.FINISHING) {
                    process.state = GameProcess.State.FINISHED;
                }

                if (process.children != null) {
                    for (GameProcess childProcess : process.children) {
                        addProcess(childProcess);
                    }
                }
            }

            if (process.state == GameProcess.State.FAILING) {
                gameProcesses.remove( process.pid );

                process.onFail();

                process.state = GameProcess.State.FAILED;
            }
        }
    }

    public void addProcess(GameProcess process) {
        process.pid = currentPid++;
        gameProcesses.put(process.pid, process);
    }

    public void shutdown() {
        for (GameProcess process : gameProcesses.values()) {
            if (process.state == GameProcess.State.UNINITIALIZED) {
                process.state = GameProcess.State.INITIALIZING;

                process.onInit( -1 );

                // if the onInit changed the state, use that state instead, otherwise it's running.
                if (process.state == GameProcess.State.INITIALIZING) {
                    process.state = GameProcess.State.RUNNING;
                }
            }

            process.finish();

            if (process.state == GameProcess.State.FINISHING) {
                gameProcesses.remove( process.pid );

                process.onFinish();

                // if the onFinish changed the state, then use that state.
                if (process.state == GameProcess.State.FINISHING) {
                    process.state = GameProcess.State.FINISHED;
                }

                if (process.children != null) {
                    for (GameProcess childProcess : process.children) {
                        addProcess(childProcess);
                    }
                }
            }

            if (process.state == GameProcess.State.FAILING) {
                gameProcesses.remove( process.pid );

                process.onFail();

                process.state = GameProcess.State.FAILED;
            }
        }

        gameProcesses.clear();
    }

    public boolean isEmpty() {
        return gameProcesses.isEmpty();
    }
}
