package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class PackDataComponent extends ActorComponent {
    public String packPrefix;
    public List<GameTileDefinition> availableGameTileDefinitions = new ArrayList<GameTileDefinition>();

    public PackDataComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<packData/>\n");
    }

    public void setPackPrefix(String packPrefix) {
        this.packPrefix = packPrefix;
    }

    public void addDataFromNode(String packName, XmlReader.Element pieceElement) {
        availableGameTileDefinitions.add( new GameTileDefinition(packName, pieceElement.getAttribute("image")) );
    }
}
