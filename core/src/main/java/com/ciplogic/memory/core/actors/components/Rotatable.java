package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

/**
 * A thing that can be rotated around its axis.
 * All angles are euler angles.
 */
public interface Rotatable {
    void setRotation(Vector3 rotation);

    Vector3 getRotation();
}
