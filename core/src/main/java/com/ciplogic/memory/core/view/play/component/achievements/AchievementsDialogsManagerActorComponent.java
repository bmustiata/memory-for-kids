package com.ciplogic.memory.core.view.play.component.achievements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.CompositeComponent;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.process.DestroyActorProcess;
import com.ciplogic.memory.core.independent.util.process.GroupProcess;
import com.ciplogic.memory.core.independent.util.process.RunnableProcess;
import com.ciplogic.memory.core.independent.util.process.Translate2DProcess;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementProcessedEvent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementUnlockedEvent;
import com.ciplogic.memory.core.view.play.component.achievements.events.DialogClosedEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class AchievementsDialogsManagerActorComponent extends ActorComponent implements EventListener {
    private LinkedList<Achievement> unlockedAchievements = new LinkedList<Achievement>();

    private final AchievementDefinitionsActorComponent achievementDefinitions;

    public AchievementsDialogsManagerActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        this.achievementDefinitions = actor.getComponent(AchievementDefinitionsActorComponent.class);

        if (achievementDefinitions == null) {
            throw new IllegalArgumentException("<achievementDefinitions/> component must be first registered into" +
                    " the actor for this component to function.");
        }
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<achievementsDialogsManager/>\n");
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof AchievementUnlockedEvent) {
            AchievementUnlockedEvent castEvent = (AchievementUnlockedEvent) event;
            Gdx.app.log("Memory", "Achievement unlocked: " + castEvent.achievement);

            unlockedAchievements.add( castEvent.achievement );
        }

        if (event instanceof AchievementProcessedEvent) {
            unlockedAchievements.removeFirst();
            closeDialog();
        }

        if (!unlockedAchievements.isEmpty() && actor.gameWorld.hasActor("share-dialog") == null) {
            Achievement achievement = getCurrentAchievement();

            openDialog(achievement);
        }
    }

    /**
     * Returns the current achievement that the dialog was opened for.
     * @return Achievement
     */
    public Achievement getCurrentAchievement() {
        if (unlockedAchievements.isEmpty()) {
            throw new IllegalArgumentException("There are no achievements to process.");
        }

        return unlockedAchievements.get(0);
    }

    private void closeDialog() {
        Actor shareDialog = actor.gameWorld.get("share-dialog");

        CompositeComponent composite = shareDialog.getComponent(CompositeComponent.class);
        GroupProcess groupProcess = new GroupProcess();
        for (Actor compositeActor : composite.actors) {
            Translate2DProcess translation = new Translate2DProcess(compositeActor.getComponent(Position2DComponent.class),
                    -100, 0, 300);

            groupProcess.addProcess( translation );
        }

        groupProcess.then(
            new DestroyActorProcess(shareDialog).then(
                new RunnableProcess(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.INSTANCE.fire( new DialogClosedEvent() );
                    }
                })
            )
        );

        actor.getComponent(ProcessManagerComponent.class).addProcess( groupProcess );
    }

    private void openDialog(Achievement achievement) {
        HashMap<String, String> dialogParameters = new HashMap<String, String>();
        dialogParameters.put("title", achievementDefinitions.getAchievementTitle(achievement));
        dialogParameters.put("message", achievementDefinitions.getAchievementDescription(achievement));
        dialogParameters.put("id", "share-dialog");

        Actor shareDialogActor = actor.gameWorld.getActorFactory().createActor("play/share-dialog", dialogParameters);
        actor.gameWorld.addActor(shareDialogActor);
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List result = new ArrayList();

        result.add( AchievementUnlockedEvent.class );
        result.add( AchievementProcessedEvent.class );
        result.add( DialogClosedEvent.class );

        return result;
    }
}
