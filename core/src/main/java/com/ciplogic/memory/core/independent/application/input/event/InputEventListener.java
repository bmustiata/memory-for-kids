package com.ciplogic.memory.core.independent.application.input.event;

import com.ciplogic.memory.core.independent.application.event.EventListener;

public interface InputEventListener extends EventListener {
}
