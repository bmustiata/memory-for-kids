package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class TextComponent extends ActorComponent {
    public String text;
    public final int layer;
    public final String fontName;
    public BitmapFontComponent font;

    public TextComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        text = child.getAttribute("text");
        layer = Integer.parseInt(child.getAttribute("layer", "0"));
        fontName = child.getAttribute("font", "default-font");
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<text text=\"")
                .append(text)
                .append("\" layer=\"")
                .append(layer)
                .append("\" font=\"")
                .append(fontName)
                .append("\"/>\n");

    }
}
