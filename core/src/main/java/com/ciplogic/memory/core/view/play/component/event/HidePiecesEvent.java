package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.view.play.component.BoardPieceComponent;

public class HidePiecesEvent extends Event {
    public final BoardPieceComponent firstPiece;
    public final BoardPieceComponent secondPiece;

    public HidePiecesEvent(BoardPieceComponent fistPiece, BoardPieceComponent secondPiece) {
        this.firstPiece = fistPiece;
        this.secondPiece = secondPiece;
    }
}
