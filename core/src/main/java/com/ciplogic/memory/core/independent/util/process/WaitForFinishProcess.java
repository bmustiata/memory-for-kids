package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class WaitForFinishProcess extends GameProcess {
    private final GameProcess process;

    public WaitForFinishProcess(GameProcess process) {
        this.process = process;
    }

    @Override
    public void onInit() {
        if (process == null) {
            finish();
        }
    }

    @Override
    public void onUpdate(long delta) {
        if (process.state == State.FAILED || process.state == State.FINISHED) {
            finish();
        }
    }
}
