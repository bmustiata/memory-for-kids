package com.ciplogic.memory.core.view.play.component.achievements;

public class AchievementDefinition {
    public String title;
    public String description;

    public AchievementDefinition(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
