package com.ciplogic.memory.core.independent.view.gameview;

import com.ciplogic.memory.core.independent.application.process.GameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

/**
 * A human game view is a view of the game for a player.
 *
 * It is composed of several Screen elements that can overlay, and also has
 * its own processes for rendering controls, and managing user input.
 */
public abstract class HumanGameView extends GameView {
    public static HumanGameView INSTANCE = null;

    protected GameProcessManager gameProcessManager = new GameProcessManager();

    @Override
    public void onInit() {
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onFinish() {
    }

    /**
     * Obtain their attached game world.
     * @return
     */
    public abstract GameWorld getGameWorld();
}
