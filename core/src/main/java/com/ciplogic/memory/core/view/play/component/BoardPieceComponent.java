package com.ciplogic.memory.core.view.play.component;

import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class BoardPieceComponent extends ActorComponent {
    public final GameTileDefinition gameTileDefinition;
    public boolean paired;
    public boolean turned;
    public boolean removed;

    public BoardPieceComponent(Actor actor, GameTileDefinition gameTileDefinition) {
        super(actor, null);

        this.gameTileDefinition = gameTileDefinition;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<boardPiece/>\n");
    }
}
