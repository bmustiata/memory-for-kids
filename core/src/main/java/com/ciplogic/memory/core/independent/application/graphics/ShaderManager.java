package com.ciplogic.memory.core.independent.application.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.*;

public class ShaderManager {
    public static String vertexShaderCode;
    public static String fragmentShaderCode;

    public static ShaderManager INSTANCE = new ShaderManager();

    private ShaderManager() {
        vertexShaderCode = readFileAsString("vertex.shader.glsl");
        fragmentShaderCode = readFileAsString("fragment.shader.glsl");
    }

    private String readFileAsString(String path) {
        return Gdx.files.internal("shaders/default/" + path).readString();
    }
}
