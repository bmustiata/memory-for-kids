package com.ciplogic.memory.core.view.util;

import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.process.CreateGameProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.process.chain.ChangeScreenChain;

public class HumanViewSwitcher {
    public static void changeHumanView(HumanGameView humanGameView) {
        GameProcess memoryGameProcess = new ChangeScreenChain(humanGameView.getGameWorld(), humanGameView).create();
        EventBus.INSTANCE.fire( new CreateGameProcess(memoryGameProcess) );
    }
}
