package com.ciplogic.memory.core.independent.view.gameview;

import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.DefaultGameWorldProcess;

/**
 * The default human game view does the registering and unregistering itself
 * to the event bus, and knows to unhook itself, when its get destroyed.
 */
public abstract class DefaultHumanGameView extends HumanGameView implements EventListener {
    protected GameWorld gameWorld;

    protected DefaultHumanGameView(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);

        gameProcessManager.addProcess(
            new DefaultGameWorldProcess(gameWorld)
        );
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);

        gameProcessManager.shutdown();
    }

    @Override
    public GameWorld getGameWorld() {
        return gameWorld;
    }
}
