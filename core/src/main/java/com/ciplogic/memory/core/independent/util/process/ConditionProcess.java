package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.Gdx;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

/**
 * Runs a process as long as the condition evaluates to false.
 */
public class ConditionProcess extends GameProcess {
    public interface FinishCondition {
        boolean check();
    }

    public FinishCondition finishCondition;

    public ConditionProcess(FinishCondition finishCondition) {
        this.finishCondition = finishCondition;
    }

    @Override
    public void onUpdate(long delta) {
        if (this.finishCondition.check()) {
            finish();
        }
    }
}
