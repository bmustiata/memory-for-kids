package com.ciplogic.memory.core.independent.application.input.event.mouse;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class MouseMovedEvent extends InputEvent {
    public final int screenX;
    public final int screenY;

    public MouseMovedEvent(int screenX, int screenY) {
        this.screenX = screenX;
        this.screenY = screenY;
    }
}
