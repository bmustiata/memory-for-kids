package com.ciplogic.memory.core.independent.application.input.event.keyboard;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class KeyDownEvent extends InputEvent {
    public int keyCode;

    public KeyDownEvent(int keyCode) {
        this.keyCode = keyCode;
    }
}
