package com.ciplogic.memory.core.view.play.component.achievements;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.menu.MenuType;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementUnlockedEvent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementsGameFinishedEvent;

import java.awt.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

// FIXME: hack. Since we're using for easier implementation bits, we trade readability.
/**
 * This class checks when games are finished, if a new achievement was unlocked or not.
 * In case it was, a new AchievementUnlockedEvent will be fired on the EventBus.
 */
public class AchievementsActorComponent extends ActorComponent implements EventListener {
    private final AchievementStorageActorComponent achievementsStorage;

    private static Map<String, Integer> DECK_SHIFTS = new HashMap<String, Integer>();

    // mask that marks the maximum difficulty for all the available decks.
    private static int ALL_DIFFICULT = 0;

    // FIXME: hack
    // since the flowers deck it's the first deck, the difficult bit location
    // depends on how many available decks are there.
    private static int FLOWERS_DECK_DIFFICULT = 8;

    static {
        int shift = 0;
        for (String value : MenuType.DECK.possibleValues) {
            ALL_DIFFICULT <<= 4;
            DECK_SHIFTS.put(value, shift);
            shift += 4;
            ALL_DIFFICULT |= 8;
        }
    }

    public AchievementsActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        this.achievementsStorage = actor.getComponent( AchievementStorageActorComponent.class );
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
            .append("<achievementsActorComponent/>\n");
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        AchievementsGameFinishedEvent castEvent = (AchievementsGameFinishedEvent) event;

        int gamesPlayed = Integer.parseInt(achievementsStorage.get("gamesPlayed", "0")) + 1;
        achievementsStorage.put("gamesPlayed", "" + gamesPlayed); // update the games played

        int currentDifficulty = 1 << ((castEvent.totalPairs - 6) / 2); // this gives 1-easy, 2-medium, 4-hard, 8-difficult.

        // played difficulties stores difficulties played across all decks.
        int playedDifficulties = Integer.parseInt(achievementsStorage.get("playedDifficulties", "0"));
        playedDifficulties |= currentDifficulty << DECK_SHIFTS.get(castEvent.deck);

//        // FIXME: This is disabled until a model is available for it.
//        if (gamesPlayed >= 1 && achievementsStorage.get(Achievement.FIRST_GAME.toString()) == null) {
//            achievementsStorage.put(Achievement.FIRST_GAME.toString(), "true");
//            EventBus.INSTANCE.fire( new AchievementUnlockedEvent( Achievement.FIRST_GAME ) );
//        }

//        // FIXME: This is disabled until a model is available for it.
//        if (gamesPlayed >= 5 && achievementsStorage.get(Achievement.GAMES_PLAYED_5.toString()) == null) {
//            achievementsStorage.put(Achievement.GAMES_PLAYED_5.toString(), "true");
//            EventBus.INSTANCE.fire( new AchievementUnlockedEvent( Achievement.GAMES_PLAYED_5 ) );
//        }

        if (gamesPlayed >= 10 && achievementsStorage.get(Achievement.GAMES_PLAYED_10.toString()) == null) {
            achievementsStorage.put(Achievement.GAMES_PLAYED_10.toString(), "true");
            EventBus.INSTANCE.fire( new AchievementUnlockedEvent( Achievement.GAMES_PLAYED_10 ) );
        }

        if (gamesPlayed >= 20 && achievementsStorage.get(Achievement.GAMES_PLAYED_20.toString()) == null) {
            achievementsStorage.put(Achievement.GAMES_PLAYED_20.toString(), "true");
            EventBus.INSTANCE.fire( new AchievementUnlockedEvent( Achievement.GAMES_PLAYED_20 ) );
        }

        if (gamesPlayed >= 100 && achievementsStorage.get(Achievement.GAMES_PLAYED_100.toString()) == null) {
            achievementsStorage.put(Achievement.GAMES_PLAYED_100.toString(), "true");
            EventBus.INSTANCE.fire( new AchievementUnlockedEvent( Achievement.GAMES_PLAYED_100 ) );
        }

//        // FIXME: This is disabled until a model is available for it.
//        if (((playedDifficulties & ALL_DIFFICULT) != 0) && achievementsStorage.get(Achievement.FIRST_GAME_DIFFICULT.toString()) == null) {
//            achievementsStorage.put(Achievement.FIRST_GAME_DIFFICULT.toString(), "true");
//            EventBus.INSTANCE.fire( new AchievementUnlockedEvent(Achievement.FIRST_GAME_DIFFICULT) );
//        }

        if (((playedDifficulties & FLOWERS_DECK_DIFFICULT) != 0) &&
                achievementsStorage.get(Achievement.FLOWERS_DIFFICULT.toString()) == null) {
            achievementsStorage.put(Achievement.FLOWERS_DIFFICULT.toString(), "true");
            EventBus.INSTANCE.fire( new AchievementUnlockedEvent(Achievement.FLOWERS_DIFFICULT) );
        }

        if (((playedDifficulties & ALL_DIFFICULT) == ALL_DIFFICULT) &&
                achievementsStorage.get(Achievement.FINISHED_ALL_TILES_IMAGES_ON_DIFFICULT.toString()) == null) {
            achievementsStorage.put(Achievement.FINISHED_ALL_TILES_IMAGES_ON_DIFFICULT.toString(), "true");
            EventBus.INSTANCE.fire( new AchievementUnlockedEvent(Achievement.FINISHED_ALL_TILES_IMAGES_ON_DIFFICULT) );
        }

        checkAllDecksPlayed(playedDifficulties);
    }

    private void checkAllDecksPlayed(int playedDifficulties) {
        int mask = 0x0f;

        for (int i = 0; i < DECK_SHIFTS.size(); i++) {
            if ((playedDifficulties & mask) == 0) {
                return;
            }

            mask <<= 4;
        }

        if (achievementsStorage.get(Achievement.FINISHED_ALL_TILES_IMAGES.toString()) == null) {
            achievementsStorage.put(Achievement.FINISHED_ALL_TILES_IMAGES.toString(), "true");
            EventBus.INSTANCE.fire( new AchievementUnlockedEvent(Achievement.FINISHED_ALL_TILES_IMAGES) );
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(AchievementsGameFinishedEvent.class);
    }
}
