package com.ciplogic.memory.core.logic;

import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A Xml that knows what nodes contain expressions, in order to allow replacing them quickly
 * with actual values.
 */
public class ParsedXmlTree {
    public XmlReader.Element rootNode;
    public List<VariableAttributeNode> variableNodes;
    public Map<String, String> defaultValues = new HashMap<String, String>();

    /**
     * Create a new tree, finding all the node elements that have dynamic values in attributes.
     * @param rootNode
     */
    public ParsedXmlTree(XmlReader.Element rootNode) {
        this.rootNode = rootNode;
        this.variableNodes = new ArrayList<VariableAttributeNode>();

        if (this.rootNode.getAttributes() != null) {
            for (ObjectMap.Entry<String, String> entry : this.rootNode.getAttributes().entries()) {
                defaultValues.put( entry.key, entry.value );
            }
        }

        findVariableAttributes(this.rootNode);
    }

    /**
     * Replaces all the values of the dynamic attributes with their actual values against
     * the variables argument.
     * @param variables
     * @return
     */
    public ParsedXmlTree transform(Map<String, String> variables) {
        for (VariableAttributeNode node : variableNodes) {
            node.element.setAttribute(node.attributeName,
                    StringTemplate.format(node.expression, variables));
        }

        return this;
    }

    /**
     * Recursively populate the variableNodes with all the attribute nodes
     * from the given XML node that have dynamic variables.
     * @param node
     */
    private void findVariableAttributes(XmlReader.Element node) {
        if (node.getAttributes() != null) {
            for (ObjectMap.Entry<String, String> attributeEntry : node.getAttributes()) {
                if (isExpression(attributeEntry.value)) {
                    this.variableNodes.add(new VariableAttributeNode(
                            node,
                            attributeEntry.key,
                            attributeEntry.value
                    ));
                }
            }
        }

        for (int i = 0; i < node.getChildCount(); i++) {
            findVariableAttributes(node.getChild(i));
        }
    }

    private boolean isExpression(String value) {
        return value.contains("{");
    }

}
