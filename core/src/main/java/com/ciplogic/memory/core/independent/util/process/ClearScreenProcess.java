package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class ClearScreenProcess extends GameProcess {
    private final float red;
    private final float green;
    private final float blue;

    public ClearScreenProcess() {
        this(0.3f, 0.3f, 0.3f);
    }

    public ClearScreenProcess(float red, float green, float blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public ClearScreenProcess(int red, int green, int blue) {
        this.red = ((float) red) / 255.0f;
        this.green = ((float) green) / 255.0f;
        this.blue = ((float) blue) / 255.0f;
    }

    @Override
    public void onUpdate(long delta) {
        Gdx.gl.glClearColor(red, green, blue, 0);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }
}
