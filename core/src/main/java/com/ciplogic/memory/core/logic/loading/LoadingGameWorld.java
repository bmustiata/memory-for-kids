package com.ciplogic.memory.core.logic.loading;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.logic.GameWorldParser;

public class LoadingGameWorld extends GameWorld {
    public static LoadingGameWorld INSTANCE = new LoadingGameWorld();

    @Override
    public ActorFactory getActorFactory() {
        return new GameWorldParser(this, "loading.world.xml");
    }
}
