package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.logic.actor.ScreenActorComponent;

import java.util.Collections;
import java.util.List;

public class ScreenOrientationChangeWatchProcess extends GameProcess implements EventListener {
    private final GameWorld gameWorld;

    public ScreenOrientationChangeWatchProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onEvent(Event event) {
        for (Actor actor: gameWorld.getAllActors()) {
            for (ScreenActorComponent screenAwareComponent : actor.getScreenAwareComponents()) {
                screenAwareComponent.callScreenOnInit();
            }
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList( ScreenOrientationChangeWatchProcess.class );
    }
}
