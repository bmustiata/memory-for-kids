package com.ciplogic.memory.core.independent.application.input.event.keyboard;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class KeyTypedEvent extends InputEvent {
    public char keyTyped;

    public KeyTypedEvent(char keyTyped) {
        this.keyTyped = keyTyped;
    }
}
