package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.Gdx;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

public class EnsureActorsModelsLoadedProcess extends GameProcess {
    private final GameWorld gameWorld;
    private boolean loadingStarted;

    public EnsureActorsModelsLoadedProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onUpdate(long delta) {
        if (!loadingStarted) {
            loadingStarted = true;
            gameWorld.resourceCache.load( gameWorld.getUsedResourcesIds() );
        }

        if (gameWorld.resourceCache.isLoaded()) {
            Gdx.app.log("Memory", "Resource cache is loaded.");
            gameWorld.createRenderables();
            finish();
        }
    }
}
