package com.ciplogic.memory.core.independent.application.input.event.mouse;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class TouchDownEvent extends InputEvent {
    public final int screenX;
    public final int screenY;
    public final int pointer;
    public final int button;

    public TouchDownEvent(int screenX, int screenY, int pointer, int button) {
        this.screenX = screenX;
        this.screenY = screenY;
        this.pointer = pointer;
        this.button = button;
    }
}
