package com.ciplogic.memory.core.independent.util;

/**
 * A class that allows generating IDs, that are guaranteed to be unique for the
 * execution of the application.
 */
public class IdGenerator {
    private int currentId;

    private String prefix;

    public static final IdGenerator INSTANCE = new IdGenerator("generated-id-");

    public IdGenerator(String prefix) {
        this.prefix = prefix;
    }

    public synchronized String generateId() {
        return this.prefix + (currentId++);
    }
}
