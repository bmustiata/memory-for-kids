package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.RandomGenerator;
import com.ciplogic.memory.core.logic.play.StarActorBuilder;
import com.ciplogic.memory.core.view.util.StringUtil;

public class StarsGeneratorComponent extends ActorComponent {
    private long lastDelta = -1;

    private final StarActorBuilder actorBuilder;

    private static final String[] STAR_COLORS = {"blue", "green", "magenta", "yellow"};

    public StarsGeneratorComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        this.lifecycled = true;
        this.actorBuilder = new StarActorBuilder(actor.gameWorld);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<starsGenerator/>\n");
    }

    @Override
    public void onUpdate(long delta) {
        if (delta - lastDelta > 400) {
            lastDelta = delta;

            float randomY = getRandomY();

            Actor newActor = actorBuilder.create(STAR_COLORS[RandomGenerator.nextInt(4)], randomY + "%");

            this.actor.gameWorld.addActor(newActor);
        }

        if (delta - lastDelta < 0) {
            lastDelta = delta;
        }
    }

    private float getRandomY() {
        return RandomGenerator.nextFloat() * 100f - 50f;
    }
}
