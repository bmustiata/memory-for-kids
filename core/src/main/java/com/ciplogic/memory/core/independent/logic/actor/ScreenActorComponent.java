package com.ciplogic.memory.core.independent.logic.actor;

import com.badlogic.gdx.utils.XmlReader;

/**
 * <p>
 * A screen actor component, is an actor component that is configured for a specific screen orientation.
 * They are handled differently in the Actor registration.
 * </p>
 * <p>
 * Since a screen actor component depends on its resolution, and might be in an invalid state
 * when added, it should do its initialization only on onInit(), and not on its constructor.
 * </p>
 * <p>
 * There might be cases if the screen orientation never matches the Actor's one, where
 * onInit/onFinish will not be called. But whenever a component will be accessed
 * will have its onInit called, and if its onInit was called, its onFinish will be
 * called as well when the actor is removed from the world.
 * </p>
 */
public abstract class ScreenActorComponent extends ActorComponent {
    /**
     * The screen orientation where this actor component applies.
     */
    public ScreenOrientation screenOrientation;

    public boolean initialized;

    public ScreenActorComponent(Actor actor) {
        super(actor, null);

        screenOrientation = ScreenOrientation.ANY;
    }

    public ScreenActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        if (child == null) {
            screenOrientation = ScreenOrientation.ANY;
            return;
        }

        String screenOrientationString = child.getAttribute("screen", "any");
        screenOrientation = ScreenOrientation.valueOf( screenOrientationString.toUpperCase() );
    }

    /**
     * The onInit for screen components should be called only once, either when the actor
     * was added, either when the resolution changes.
     */
    public void callScreenOnInit() {
        if (!this.initialized) {
            this.onInit();
        }
    }

    /**
     * The onFinish will be called when the actor was removed, only if it was initialized
     * for the given screen.
     */
    public void callScreenOnFinish() {
        if (this.initialized) {
            this.onFinish();
        }
    }
}
