package com.ciplogic.memory.core.view.menu;

import com.ciplogic.memory.core.actors.components.PreferencesComponent;

public enum MenuType {
    DECK("flowers", "letters", "numbers"),
    PAIRS("6", "8", "10", "12"),
    SOUND("On", "Off"),
    MUSIC("On", "Off");

    public String[] possibleValues;

    MenuType(String ... possibleValues) {
        this.possibleValues = possibleValues;
    }

    public static MenuType fromString(String type) {
        return MenuType.valueOf(type.toUpperCase());
    }

    public static String getOption(PreferencesComponent preferencesComponent, MenuType menuType) {
        return getOption(preferencesComponent, menuType, menuType.possibleValues[0]);
    }

    public static String getOption(PreferencesComponent preferencesComponent, MenuType menuType, String defaultValue) {
        String setting = preferencesComponent.getString("PREFERENCE-" + menuType.toString(), defaultValue);

        if (menuType.getIndex(setting) < 0) {
            return menuType.possibleValues[0];
        }

        return setting;
    }

    public static void setOption(PreferencesComponent preferencesComponent, MenuType menuType, String value) {
        if (menuType.getIndex(value) < 0) {
            throw new IllegalArgumentException("MenuType " + menuType + " has no available value named: " + value);
        }

        preferencesComponent.putString("PREFERENCE-" + menuType.toString(), value);
    }

    public int getIndex(String currentOption) {
        for (int i = 0; i < possibleValues.length; i++) {
            if (possibleValues[i].equals(currentOption)) {
                return i;
            }
        }

        return -1;
    }
}
