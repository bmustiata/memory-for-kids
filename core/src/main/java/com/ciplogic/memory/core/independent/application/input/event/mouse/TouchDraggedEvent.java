package com.ciplogic.memory.core.independent.application.input.event.mouse;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class TouchDraggedEvent extends InputEvent {
    public final int screenX;
    public final int screenY;
    public final int pointer;

    public TouchDraggedEvent(int screenX, int screenY, int pointer) {
        this.screenX = screenX;
        this.screenY = screenY;
        this.pointer = pointer;
    }
}
