package com.ciplogic.memory.core.logic.chest;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.logic.GameWorldParser;
import com.ciplogic.memory.core.view.play.PlayComponentFactory;

public class ChestGameWorld extends GameWorld {
    public static ChestGameWorld INSTANCE = new ChestGameWorld();

    @Override
    public ActorFactory getActorFactory() {
        return new GameWorldParser(this, "chest.world.xml", new PlayComponentFactory());
    }
}
