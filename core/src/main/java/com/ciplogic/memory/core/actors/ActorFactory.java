package com.ciplogic.memory.core.actors;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;

import java.util.Map;

/**
 * An ActorFactory allows creating actors for a GameWorld.
 */
public interface ActorFactory {
    /**
     * Are there more actors to be created?
     * @return
     */
    boolean hasNext();

    /**
     * Create and get the next actor.
     * @return
     */
    Actor next();

    /**
     * Create an actor from an external resource, with the given parameters.
     * @param resource The full name to the resource, except actor.xml should be passed. (e.g. /play/star)
     * @param actorParameters A modifiable map that will contain the parameters given to the actor.
     *                        The map is modifiable in order not to always copy from one map to another, since
     *                        the default parameters that are in the root node (of the external actor.xml file),
     *                        need also be stored somewhere for the parsing of the resource.
     * @return
     */
    Actor createActor(String resource, Map<String, String> actorParameters);

    /**
     * Create an actor from an XML element.
     * @param actorElement
     * @return
     */
    Actor createActor(XmlReader.Element actorElement);

    /**
     * Create an actor using the XML code given as root node for that actor.
     * @param actorCode
     * @return
     */
    Actor createActor(String actorCode);
}
