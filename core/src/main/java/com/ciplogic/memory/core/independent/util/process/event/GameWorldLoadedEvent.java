package com.ciplogic.memory.core.independent.util.process.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

public class GameWorldLoadedEvent extends Event {
    public final GameWorld gameWorld;

    public GameWorldLoadedEvent(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }
}
