package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

/**
 * Update all the components from actors that are lifecycled.
 */
public class UpdateLifeCycledActorsProcess extends GameProcess {
    private final GameWorld gameWorld;

    public UpdateLifeCycledActorsProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onUpdate(long delta) {
        for (Actor actor : gameWorld.getLifecycledActors()) {
            for (ActorComponent component : actor.lifecycledComponents) {
                component.onUpdate(delta);
            }
        }
    }
}
