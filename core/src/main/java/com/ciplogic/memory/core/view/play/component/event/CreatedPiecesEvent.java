package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.view.play.component.BoardPieceComponent;

import java.util.List;

public class CreatedPiecesEvent extends Event {
    public final List<Actor> createdPieces;

    public CreatedPiecesEvent(List<Actor> createdPieces) {
        this.createdPieces = createdPieces;
    }
}
