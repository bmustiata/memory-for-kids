package com.ciplogic.memory.core.view.chest.component;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.PositionComponent;
import com.ciplogic.memory.core.actors.components.Positionable;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.play.component.achievements.AchievementStorageActorComponent;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class AchievementVisualActorComponent extends ActorComponent implements Positionable {
    public String lockedModelActorId;
    public String unlockedModelActorId;
    public String achievementEnumId;

    private AchievementStorageActorComponent achievementStorage;

    // somewhere offscreen, normally a boolean flag should exist to disable rendering.
    private static final Vector3 awayFromScreen = new Vector3(1000, 1000, 0);

    private Vector3 achievementPosition;

    public AchievementVisualActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        lockedModelActorId = child.getAttribute("lockedModelActorId");
        unlockedModelActorId = child.getAttribute("unlockedModelActorId");
        achievementEnumId = child.getAttribute("achievement");

        achievementStorage = actor.gameWorld.get("achievement-storage").getComponent(AchievementStorageActorComponent.class);
    }

    @Override
    public void restart() {
        boolean unlocked = achievementStorage.get(achievementEnumId) != null;

        Positionable lockedModelPositionable = actor.gameWorld.get(lockedModelActorId)
                .getComponent(PositionComponent.class).positionable;
        Positionable unlockedModelPositionable = actor.gameWorld.get(unlockedModelActorId)
                .getComponent(PositionComponent.class).positionable;

        if (unlocked) { // show the right model instance.
            lockedModelPositionable.setPosition(awayFromScreen);
            unlockedModelPositionable.setPosition(achievementPosition);
        } else {
            lockedModelPositionable.setPosition(achievementPosition);
            unlockedModelPositionable.setPosition(awayFromScreen);
        }
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<achievementVisual achievement=\"")
                .append(achievementEnumId)
                .append("\" lockedModelActorId=\"")
                .append(lockedModelActorId)
                .append("\" unlockedModelActorId=\"")
                .append(unlockedModelActorId)
                .append("\"/>\n");
    }

    @Override
    public void setPosition(Vector3 position) {
        this.achievementPosition = position;
    }

    @Override
    public Vector3 getPosition() {
        return achievementPosition;
    }
}
