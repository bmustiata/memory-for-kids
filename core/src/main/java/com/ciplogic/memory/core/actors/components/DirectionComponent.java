package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class DirectionComponent extends ActorComponent {
    public Rotatable rotatable;

    public DirectionComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        for (ActorComponent component : actor.components.values()) {
            if (component instanceof Rotatable) {
                rotatable = (Rotatable) component;
            }
        }

        assert rotatable != null;


        Vector3 vector3 = new Vector3(
            floatAttribute(child, "x"),
            floatAttribute(child, "y"),
            floatAttribute(child, "z")
        ).nor();

        rotatable.setRotation(vector3);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<direction x=\"")
                .append(rotatable.getRotation().x)
                .append("\" y=\"")
                .append(rotatable.getRotation().y)
                .append("\" z=\"")
                .append(rotatable.getRotation().z)
                .append("\"/>\n");
    }
}
