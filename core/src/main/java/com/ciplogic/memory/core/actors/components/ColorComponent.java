package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

/**
 * Specifies a color for the actor.
 */
public class ColorComponent extends ActorComponent {
    public float red;
    public float green;
    public float blue;
    public float alpha;

    public ColorComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        this.red = floatAttribute(child, "red");
        this.green = floatAttribute(child, "green");
        this.blue = floatAttribute(child, "blue");

        this.alpha = floatAttribute(child, "alpha", "1.0");
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<color red=\"")
                .append(red)
                .append("\" green=\"")
                .append(green)
                .append("\" blue=\"")
                .append(blue)
                .append("\" alpha=\"")
                .append(alpha)
                .append("\"/>\n");
    }

    public void setRGBA(float red, float green, float blue, float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }
}
