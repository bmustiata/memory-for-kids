package com.ciplogic.memory.core.view.play;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.input.event.mouse.TouchDownEvent;
import com.ciplogic.memory.core.independent.util.process.DefaultGameWorldProcess;
import com.ciplogic.memory.core.independent.util.process.event.GameWorldLoadedEvent;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.logic.play.PlayGameWorld;
import com.ciplogic.memory.core.view.menu.MenuHumanView;
import com.ciplogic.memory.core.view.play.component.BoardComponent;
import com.ciplogic.memory.core.view.play.component.BoardPieceComponent;
import com.ciplogic.memory.core.view.play.component.TileAnimatorProcessComponent;
import com.ciplogic.memory.core.view.play.component.achievements.Achievement;
import com.ciplogic.memory.core.view.play.component.achievements.AchievementsDialogsManagerActorComponent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementProcessedEvent;
import com.ciplogic.memory.core.view.play.component.achievements.events.FacebookShareAchievementEvent;
import com.ciplogic.memory.core.view.util.HumanViewSwitcher;
import com.ciplogic.memory.core.view.util.SpriteTest;

import java.util.ArrayList;
import java.util.List;

public class PlayGameHumanView extends HumanGameView implements EventListener {
    private PlayGameWorld gameWorld;

    public PlayGameHumanView() {
        gameWorld = PlayGameWorld.INSTANCE;
    }

    @Override
    public void onInit() {
        resetCamera();

        EventBus.INSTANCE.registerListener(this);

        gameProcessManager.addProcess(
            new DefaultGameWorldProcess(gameWorld)
        );
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);

        gameProcessManager.shutdown();
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof TouchDownEvent) {
            // if there is a share dialog open, process only the share dialog click events.
            TouchDownEvent touchDownEvent = (TouchDownEvent) event;
            if (gameWorld.hasActor("share-dialog") != null) {
                if (isButtonClicked(touchDownEvent, "facebook-share-button")) {
                    fireFacebookShareEvent();
                } else if (isButtonClicked(touchDownEvent, "close-share-button")) {
                    fireAchievementProcessedEvent();
                }

                return; // if there is a share dialog, return early.
            }

            TileAnimatorProcess tileAnimatorProcess = gameWorld.get("playWorldProcesses")
                    .getComponent( TileAnimatorProcessComponent.class ).tileAnimatorProcess;

            if (!tileAnimatorProcess.animating) { // allow selecting pieces only when no other animation is running.
                if (isButtonClicked(touchDownEvent, "settingsButton")) {
                    goToSettingsScreen();
                    return;
                }

                if (isButtonClicked(touchDownEvent, "newGameButton")) {
                    startNewGame();
                    return;
                }

                BoardPieceComponent boardPiece = getPieceClicked(touchDownEvent);
                if (boardPiece != null) {
                    BoardComponent boardComponent = gameWorld.get("board").getComponent(BoardComponent.class);
                    boardComponent.turn(boardPiece);
                }
            }
        } else if (event instanceof ScreenResizeEvent) {
            resetCamera();
        } else if (event instanceof GameWorldLoadedEvent && ((GameWorldLoadedEvent)event).gameWorld == gameWorld) {
            resetCamera();
        }
    }

    private void fireFacebookShareEvent() {
        AchievementsDialogsManagerActorComponent achievementsDialogs =
                gameWorld.get("achievementsManager").getComponent(AchievementsDialogsManagerActorComponent.class);
        Achievement achievement = achievementsDialogs.getCurrentAchievement();

        EventBus.INSTANCE.fire( new FacebookShareAchievementEvent(achievement) );
    }

    private void fireAchievementProcessedEvent() {
        Gdx.app.log("Memory", "Event processed");
        EventBus.INSTANCE.fire( new AchievementProcessedEvent() );
    }

    private void startNewGame() {
        gameWorld.get("board").getComponent(BoardComponent.class).newGame();
    }

    /**
     * Gets the piece that was clicked by computing the bounding box for all the nodes. Since I already
     * know the sizes of the pieces, this can be heavily optimized.
     * @param event
     * @return
     */
    private BoardPieceComponent getPieceClicked(TouchDownEvent event) {
        BoardComponent board = gameWorld.get("board").getComponent(BoardComponent.class);

        Ray ray = createRay(event);
        for (BoardPieceComponent piece : board.piecesOnBoard.values()) {
            ModelInstanceComponent modelInstance = piece.actor.getComponent(ModelInstanceComponent.class);

            Vector3 position = modelInstance.getPosition();
            BoundingBox boundingBox = new BoundingBox(
                new Vector3(position.x - 1f, position.y - 1f, position.z - 0.086f),
                new Vector3(position.x + 1f, position.y + 1f, position.z + 0.086f)
            );

            if (Intersector.intersectRayBoundsFast(ray, boundingBox)) {
                return piece;
            }
        }

        return null;
    }

    private Ray createRay(TouchDownEvent event) {
        CameraComponent camera = gameWorld.get("camera").getComponent(CameraComponent.class);

        return new Ray(
            camera.getPosition(),
            camera.getWorldPositionFrom2d(event.screenX, event.screenY)
         );
    }

    private boolean isButtonClicked(TouchDownEvent event, String actorId) {
        return SpriteTest.isActorInTouchEvent(gameWorld.get(actorId), event);
    }

    private void goToSettingsScreen() {
        HumanViewSwitcher.changeHumanView(new MenuHumanView());
    }

    private void resetCamera() {
        float rows = gameWorld.get("board").getComponent(BoardComponent.class).totalPairs / 2f;
        CameraComponent camera = gameWorld.get("camera").getComponent(CameraComponent.class);

        // fit the pieces as best as possible into the
        camera.fitBoth((2f + TileAnimatorProcess.PIECE_DISTANCE) * 4 + 2 * TileAnimatorProcess.PIECE_DISTANCE,
                (2f + TileAnimatorProcess.PIECE_DISTANCE) * rows + 2 * TileAnimatorProcess.PIECE_DISTANCE);
        camera.resetCamera();
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List<Class<T>> result = new ArrayList<Class<T>>();

        result.add( (Class<T>) ScreenResizeEvent.class );
        result.add( (Class<T>) TouchDownEvent.class );
        result.add( (Class<T>) GameWorldLoadedEvent.class );

        return result;
    }

    @Override
    public PlayGameWorld getGameWorld() {
        return gameWorld;
    }
}
