package com.ciplogic.memory.core.view.play.component.achievements.events;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.view.play.component.achievements.Achievement;

public class AchievementUnlockedEvent extends Event {
    public Achievement achievement;

    public AchievementUnlockedEvent(Achievement achievement) {
        this.achievement = achievement;
    }
}
