package com.ciplogic.memory.core.lang;

import java.util.HashMap;
import java.util.Map;

public class MapMaker {
    public static class MapEntry<K,V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    public static <K,V> Map<K,V> map(MapEntry<K,V>... entries) {
        Map<K,V> result = new HashMap<K, V>();

        for (MapEntry<K, V> entry : entries) {
            result.put(entry.key, entry.value);
        }

        return result;
    }

    public static <K,V> MapEntry<K,V> entry(K key, V value) {
        return new MapEntry<K,V>(key, value);
    }
}
