package com.ciplogic.memory.core.logic;

import com.badlogic.gdx.utils.XmlReader;

/**
 * This is a pointer to an element, with the attribute name.
 */
public class VariableAttributeNode {
    public XmlReader.Element element;
    public String attributeName;
    public String expression;

    public VariableAttributeNode(XmlReader.Element element, String attributeName, String expression) {
        this.element = element;
        this.attributeName = attributeName;
        this.expression = expression;
    }
}
