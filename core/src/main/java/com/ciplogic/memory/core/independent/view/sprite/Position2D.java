package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.XmlReader;

public class Position2D {
    public Distance x;
    public Distance y;

    public static Position2D readFromNode(XmlReader.Element child) {
        Position2D position2D = new Position2D();

        position2D.x = Distance.readDistanceElement(child, "x");
        position2D.y = Distance.readDistanceElement(child, "y");

        return position2D;
    }

    public static Position2D readFromValues(String x, String y) {
        Position2D position2D = new Position2D();

        position2D.x = Distance.readDistanceValue(x);
        position2D.y = Distance.readDistanceValue(y);

        return position2D;
    }

    public float getScreenX() {
        if (x.unit == Distance.Unit.PIXELS) {
            return x.value;
        } else {
            return Gdx.graphics.getWidth() * x.value / 100.0f;
        }
    }

    public float getScreen2dX() {
        return getScreenX() + (Gdx.graphics.getWidth() / 2);
    }

    public float getScreenY() {
        if (y.unit == Distance.Unit.PIXELS) {
            return y.value;
        } else {
            return Gdx.graphics.getHeight() * y.value / 100.0f;
        }
    }

    public float getScreen2dY() {
        return Gdx.graphics.getHeight() - (getScreenY() + (Gdx.graphics.getHeight() / 2));
    }
}
