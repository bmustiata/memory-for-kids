package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.actors.components.PreferencesComponent;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.RandomGenerator;
import com.ciplogic.memory.core.view.menu.MenuType;

import java.util.HashSet;
import java.util.Set;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class GenerateInitialPiecesComponent extends ActorComponent {
    private int pieceId = 1;

    public GenerateInitialPiecesComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<generateInitialPieces/>\n");
    }

    public void generatePieces() {
        Gdx.app.log("Memory", "generate pieces.");

        GameWorld gameWorld = actor.gameWorld;

        PackDataComponent packData = gameWorld.get("selectedPack").getComponent(PackDataComponent.class);
        Set<Integer> selectedPiecesIndexes = new HashSet<Integer>();

        while (selectedPiecesIndexes.size() < getPairsCount()) {
            selectedPiecesIndexes.add( RandomGenerator.nextInt(packData.availableGameTileDefinitions.size()) );
        }

        for (Integer pickedIndex : selectedPiecesIndexes) {
            GameTileDefinition gameTileDefinition = packData.availableGameTileDefinitions.get( pickedIndex );
            createPiece(gameWorld, gameTileDefinition);
            createPiece(gameWorld, gameTileDefinition);
        }
    }

    private int getPairsCount() {
        PreferencesComponent preferences = actor.gameWorld.get("preferences").getComponent(PreferencesComponent.class);

        return Integer.parseInt( MenuType.getOption(preferences, MenuType.PAIRS) );
    }

    private Actor createPiece(GameWorld gameWorld, GameTileDefinition gameTileDefinition) {
        String actorId = "piece" + getNextPieceId();

        Actor pieceActor = new Actor(actor.gameWorld, actorId);

        pieceActor.withComponent(new ModelInstanceComponent(pieceActor, "play/piece.g3dj"))
                .withComponent(new BoardPieceComponent(pieceActor, gameTileDefinition))
                .withComponent(new ProcessManagerComponent(pieceActor, null));

        pieceActor.getComponent(ModelInstanceComponent.class).setRotation(new Vector3(90, 0, 0));

        gameWorld.addActor( pieceActor );

        return pieceActor;
    }

    public Integer getNextPieceId() {
        return pieceId++;
    }
}
