package com.ciplogic.memory.core.view.test;

import com.badlogic.gdx.Input;
import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyDownEvent;
import com.ciplogic.memory.core.independent.application.input.event.mouse.TouchDownEvent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.util.process.DefaultGameWorldProcess;
import com.ciplogic.memory.core.independent.view.gameview.HumanGameView;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;
import com.ciplogic.memory.core.logic.test.TestGameWorld;
import com.ciplogic.memory.core.view.menu.component.MenuOptionComponent;
import com.ciplogic.memory.core.view.menu.component.MenuOptionSelectionComponent;

import java.util.ArrayList;
import java.util.List;

public class TestHumanView extends HumanGameView implements EventListener {
    private static final String[] MENU_ACTORS = {
        "deckOption", "pairsOption", "soundOption", "musicOption"
    };

    private TestGameWorld gameWorld;

    @Override
    public void onInit() {
        this.gameWorld = TestGameWorld.INSTANCE;

        CameraComponent camera = gameWorld.get("camera").getComponent(CameraComponent.class);

        camera.fitBoth(16f, 16f);
        camera.resetCamera();

        EventBus.INSTANCE.registerListener(this);
        gameProcessManager.addProcess(
            new DefaultGameWorldProcess(gameWorld)
        );
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        gameProcessManager.shutdown();
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public GameWorld getGameWorld() {
        return gameWorld;
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof TouchDownEvent) {
            MenuOptionComponent menuOptionComponent = findSelectedMenu((TouchDownEvent) event);
            if (menuOptionComponent != null) {
                menuOptionComponent.setNextValue();
                findSelectionComponent().selectOption(menuOptionComponent);
            }
        }

        if (event instanceof KeyDownEvent) {
            KeyDownEvent keyDownEvent = (KeyDownEvent) event;

            switch (keyDownEvent.keyCode) {
                case Input.Keys.UP:    selectPreviousOption(); break;
                case Input.Keys.DOWN:  selectNextOption(); break;
                case Input.Keys.ENTER: changeSelectedOption(); break;
            }
        }
    }

    private void selectPreviousOption() {
        MenuOptionSelectionComponent selectionComponent = findSelectionComponent();
        String actorName;

        if (selectionComponent.selected == null) {
            actorName = MENU_ACTORS[MENU_ACTORS.length - 1];
        } else {
            int index = menuOptionIndexOf(selectionComponent.selected.actor.id);
            actorName =  index == 0 ? MENU_ACTORS[MENU_ACTORS.length - 1] : MENU_ACTORS[index - 1];
        }

        selectionComponent.selectOption( getMenuOption(actorName) );
    }

    private void selectNextOption() {
        MenuOptionSelectionComponent selectionComponent = findSelectionComponent();
        String actorName;

        if (selectionComponent.selected == null) {
            actorName = MENU_ACTORS[0];
        } else {
            int index = menuOptionIndexOf(selectionComponent.selected.actor.id);
            actorName =  index == MENU_ACTORS.length - 1 ? MENU_ACTORS[0] : MENU_ACTORS[index + 1];
        }

        selectionComponent.selectOption( getMenuOption(actorName) );
    }

    private void changeSelectedOption() {
        MenuOptionSelectionComponent selectionComponent = findSelectionComponent();

        if (selectionComponent.selected != null) {
            selectionComponent.selected.setNextValue();
        }
    }

    private int menuOptionIndexOf(String id) {
        for (int i = 0; i < MENU_ACTORS.length; i++) {
            if (MENU_ACTORS[i].equals(id)) {
                return i;
            }
        }

        return -1;
    }

    private MenuOptionComponent getMenuOption(String actorName) {
        return gameWorld.get(actorName).getComponent(MenuOptionComponent.class);
    }

    private MenuOptionSelectionComponent findSelectionComponent() {
        return gameWorld.get("currentSelection").getComponent(MenuOptionSelectionComponent.class);
    }

    private MenuOptionComponent findSelectedMenu(TouchDownEvent event) {
        for (String menuActor : MENU_ACTORS) {
            Actor actor = getActorIfInPosition(menuActor, event);

            if (actor != null) {
                return actor.getComponent( MenuOptionComponent.class );
            }
        }

        return null;
    }

    /*
     * <p>Returns true if the actor is inside the given position.</p>
     * <p>Since our items are centered, we need to adjust the computation by size/2</p>
     */
    private Actor getActorIfInPosition(String menuActor, TouchDownEvent event) {
        Actor actor = gameWorld.get(menuActor);

        Position2DComponent position2DComponent = actor.getComponent( Position2DComponent.class );
        Size2DComponent size2DComponent = actor.getComponent( Size2DComponent.class );

        float size2DScreenWidth = size2DComponent.size2D.getScreenWidth();
        float size2DScreenHeight = size2DComponent.size2D.getScreenHeight();

        if (event.screenX >= position2DComponent.position2D.getScreen2dX() - size2DScreenWidth / 2 &&
            event.screenX <= position2DComponent.position2D.getScreen2dX() + size2DScreenWidth - size2DScreenWidth / 2 &&
            event.screenY >= position2DComponent.position2D.getScreen2dY() - size2DScreenHeight / 2 &&
            event.screenY <= position2DComponent.position2D.getScreen2dY() + size2DScreenHeight - size2DScreenHeight / 2) {
            return actor;
        }

        return null;
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List<Class<T>> items = new ArrayList<Class<T>>();

        items.add((Class<T>) TouchDownEvent.class);
        items.add((Class<T>) KeyDownEvent.class);

        return items;
    }
}
