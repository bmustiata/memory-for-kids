package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.logic.play.PlayGameWorld;
import com.ciplogic.memory.core.view.play.TileAnimatorProcess;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

/**
 * A class that just allows adding the TileAnimatorProcess.
 */
public class TileAnimatorProcessComponent extends ActorComponent {
    public final TileAnimatorProcess tileAnimatorProcess;

    public TileAnimatorProcessComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        ProcessManagerComponent processManager = actor.getComponent(ProcessManagerComponent.class);

        if (processManager == null) {
            throw new IllegalStateException("TileAnimatorProcessComponent requires a " +
                    "ProcessManagerComponent already registered into the actor for actor: " + actor.id);
        }

        tileAnimatorProcess = new TileAnimatorProcess(PlayGameWorld.INSTANCE);
        processManager.addProcess(tileAnimatorProcess);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<tileAnimatorProcessComponent/>\n");
    }
}
