package com.ciplogic.memory.core.view.menu;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.actors.components.DefaultComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.menu.component.*;

public class MenuComponentFactory implements ComponentFactory {
    private final DefaultComponentFactory defaultFactory;

    public MenuComponentFactory() {
        this.defaultFactory = new DefaultComponentFactory();
    }

    @Override
    public ActorComponent fromXmlNode(Actor actor, XmlReader.Element child) {
        if ("spinningSun".equals(child.getName())) {
            return new SpinningSunComponent(actor, child);
        } else if ("movingCloud".equals(child.getName())) {
            return new MovingCloudComponent(actor, child);
        } else if ("cloudGenerator".equals(child.getName())) {
            return new CloudGeneratorComponent(actor, child);
        } else if ("menuOption".equals(child.getName())) {
            return new MenuOptionComponent(actor, child);
        } else if ("menuOptionSelection".equals(child.getName())) {
            return new MenuOptionSelectionComponent(actor, child);
        } else if ("menuOptionUi".equals(child.getName())) {
            return new MenuOptionUiComponent(actor, child);
        }

        return defaultFactory.fromXmlNode(actor, child);
    }
}
