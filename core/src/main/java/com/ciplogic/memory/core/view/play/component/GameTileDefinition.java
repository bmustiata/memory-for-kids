package com.ciplogic.memory.core.view.play.component;

public class GameTileDefinition {
    public final String id;
    public final String image;
    public final String pack;

    public GameTileDefinition(String pack, String image) {
        this.pack = pack;
        this.id = image;
        this.image = image;
    }
}
