package com.ciplogic.memory.core.view.menu.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.process.GameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.process.DestroyActorProcess;
import com.ciplogic.memory.core.independent.util.process.Translate2DProcess;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.time.TimeShift;
import com.ciplogic.memory.core.view.util.StringUtil;

public class MovingCloudComponent extends ActorComponent {
    private long totalTimeToPass = 25000;

    private Position2DComponent position2d;

    private GameProcessManager gameProcessManager;

    public MovingCloudComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        initializeCloud(actor);
    }

    public MovingCloudComponent(Actor actor, long totalTimeToPass) {
        super(actor, null);

        this.totalTimeToPass = totalTimeToPass;
        initializeCloud(actor);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<movingCloud/>\n");
    }

    private void initializeCloud(Actor actor) {
        position2d = actor.getComponent(Position2DComponent.class);

        lifecycled = true;

        gameProcessManager = new GameProcessManager();
        gameProcessManager.addProcess(
            new Translate2DProcess(position2d, -90f, position2d.position2D.y.value, totalTimeToPass, TimeShift.CUBIC).then(
                new DestroyActorProcess(actor)
            )
        );
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }
}
