package com.ciplogic.memory.core.lang;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ModifiableLinkedHashSet<E> implements Set<E> {
    private Map<E, E> storage = new ModifiableLinkedHashMap<E, E>();

    @Override
    public int size() {
        return storage.size();
    }

    @Override
    public boolean isEmpty() {
        return storage.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return storage.containsKey(o);
    }

    @Override
    public Iterator<E> iterator() {
        return storage.values().iterator();
    }

    @Override
    public Object[] toArray() {
        return storage.values().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return storage.values().toArray(a);
    }

    @Override
    public boolean add(E e) {
        return e != storage.put(e, e);
    }

    @Override
    public boolean remove(Object o) {
        return null != storage.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return storage.keySet().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        boolean changed = false;

        for (E item : collection) {
            changed |= add(item);
        }

        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new IllegalArgumentException("Not implemented");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;

        for (Object item : c) {
            changed |= remove(item);
        }

        return changed;
    }

    @Override
    public void clear() {
        storage.clear();
    }
}
