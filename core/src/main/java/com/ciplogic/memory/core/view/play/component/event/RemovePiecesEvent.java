package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.logic.actor.Actor;

import java.util.List;

public class RemovePiecesEvent extends Event {
    public final List<Actor> pieces;

    public RemovePiecesEvent(List<Actor> pieces) {
        this.pieces = pieces;
    }
}
