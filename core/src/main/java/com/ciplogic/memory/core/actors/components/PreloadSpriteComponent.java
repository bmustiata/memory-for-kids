package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class PreloadSpriteComponent extends ActorComponent {
    public List<String> spriteList = new ArrayList<String>();

    public PreloadSpriteComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        for (XmlReader.Element spriteElement : child.getChildrenByName("sprite")) {
            String value = spriteElement.getText();

            if (value == null) {
                value = spriteElement.getAttribute("name");
            }

            assert value != null;

            spriteList.add(value);
        }
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<preloadSprite>\n");


        for (int i = 0; i < spriteList.size(); i++) {
            stringBuilder.append(StringUtil.indent(indentLevel + 1))
                .append("<sprite name=\"")
                .append(spriteList.get(i))
                .append("\"/>\n");

        }

        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("</preloadSprite>\n");
    }
}
