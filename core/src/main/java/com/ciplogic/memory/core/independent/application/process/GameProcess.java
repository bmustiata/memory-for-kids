package com.ciplogic.memory.core.independent.application.process;

public abstract class GameProcess {
    public static enum State {
        UNINITIALIZED,
        INITIALIZING,
        PAUSED,
        RUNNING,
        FINISHING,
        FINISHED,
        FAILING,
        FAILED
    }

    public Integer pid;
    public State state = State.UNINITIALIZED;
    GameProcess[] children = null;

    public GameProcess() {
    }

    public GameProcess then(GameProcess ... children) {
        this.children = children;

        return this;
    }

    public void onInit() {
    }

    public void onInit(long delta) {
        onInit();
    }

    public abstract void onUpdate(long delta);

    public void onFinish() {}

    public void onFail() {}

    public void finish() {
        if (state != State.FAILED && state != State.FINISHED && state != State.FAILING) {
            state = State.FINISHING;
        }
    }

    public void fail() {
        if (state != State.FAILED && state != State.FINISHED) {
            state = State.FAILING;
        }
    }
}
