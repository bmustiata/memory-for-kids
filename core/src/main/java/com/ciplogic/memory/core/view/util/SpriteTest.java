package com.ciplogic.memory.core.view.util;

import com.ciplogic.memory.core.independent.application.input.event.mouse.TouchDownEvent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;

public class SpriteTest {
    /*
     * <p>Returns true if the actor is inside the given position.</p>
     * <p>Also takes into account the alignment of the given position2d component.</p>
     */
    public static boolean isActorInTouchEvent(Actor actor, TouchDownEvent event) {
        Position2DComponent position2DComponent = actor.getComponent( Position2DComponent.class );
        Size2DComponent size2DComponent = actor.getComponent( Size2DComponent.class );

        float size2DScreenWidth = size2DComponent.size2D.getScreenWidth();
        float size2DScreenHeight = size2DComponent.size2D.getScreenHeight();

        float spriteX = position2DComponent.position2D.getScreen2dX();
        float spriteY = position2DComponent.position2D.getScreen2dY();

        if ("center".equals(position2DComponent.align)) {
            spriteX -= size2DScreenWidth / 2f;
        }
        if ("right".equals(position2DComponent.align)) {
            spriteX -= size2DScreenWidth;
        }

        if ("middle".equals(position2DComponent.valign)) {
            spriteY -= size2DScreenHeight / 2f;
        }
        if ("top".equals(position2DComponent.valign)) {
            spriteY -= size2DScreenHeight;
        }


        if (event.screenX >= spriteX &&
                event.screenX <= spriteX + size2DScreenWidth&&
                event.screenY >= spriteY &&
                event.screenY <= spriteY + size2DScreenHeight) {
            return true;
        }

        return false;
    }

}
