package com.ciplogic.memory.core.view.play.component.achievements;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import java.util.HashMap;
import java.util.Map;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class AchievementStorageActorComponent extends ActorComponent {
    // FIXME: persist
    private static Map<String, String> storage = new HashMap<String, String>();

    public AchievementStorageActorComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<achievementStorage/>\n");
    }

    /**
     * Gets the value from the storage. The propertyName is converted toString()
     * in order to fetch the data from the storage.
     * @param propertyName
     * @return
     */
    public String get(String propertyName) {
        return get(propertyName, null);
    }

    /**
     * Gets the value from teh storage. The propertyName is converted toString()
     * to fetch the data from the storage.
     * @param propertyName
     * @param defaultValue
     * @return
     */
    public String get(String propertyName, String defaultValue) {
        assert propertyName != null;

        String result = storage.get(propertyName);

        if (result == null) {
            return defaultValue;
        }

        return result;
    }

    /**
     * Puts a property into the storage. The propertyName is converted toString()
     * before storing the data into the storage.
     * @param propertyName
     * @param value
     */
    public void put(String propertyName, String value) {
        assert propertyName != null;

        storage.put(propertyName, value);
    }
}
