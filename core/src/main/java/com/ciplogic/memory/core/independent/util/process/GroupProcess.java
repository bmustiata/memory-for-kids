package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcessManager;

/**
 * A process that finishes only when all the other processes from the given process end.
 */
public class GroupProcess extends GameProcess {
    private GameProcessManager gameProcessManager = new GameProcessManager();

    public GroupProcess() {
    }

    @Override
    public void onUpdate(long delta) {
        if (gameProcessManager.isEmpty()) {
            finish();
        }
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        if (!gameProcessManager.isEmpty()) {
            gameProcessManager.shutdown();
        }
    }

    public void addProcess(GameProcess process) {
        gameProcessManager.addProcess(process);
    }
}
