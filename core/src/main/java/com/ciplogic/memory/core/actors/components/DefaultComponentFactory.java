package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.actors.components.intersect.SphereBoundingActorComponent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;
import com.ciplogic.memory.core.test.TestCodeActorComponent;

public class DefaultComponentFactory implements ComponentFactory {
    @Override
    public ActorComponent fromXmlNode(Actor actor, XmlReader.Element child) {

        String childName = child.getName();

        if (childName.equals("camera")) {
            return new CameraComponent(actor, child);
        } else if (childName.equals("clearScreen")) {
            return new ClearScreenComponent(actor, child);
        } else if (childName.equals("directionalLight")) {
            return new DirectionalLightComponent(actor, child);
        } else if (childName.equals("pointLight")) {
            return new PointLightComponent(actor, child);
        } else if (childName.equals("modelInstance")) {
            return new ModelInstanceComponent(actor, child);
        } else if (childName.equals("spinningModelInstance")) {
            return new SpinningModelInstanceComponent(actor, child);
        } else if (childName.equals("sprite")) {
            return new SpriteComponent(actor, child);
        } else if (childName.equals("position")) {
            return new PositionComponent(actor, child);
        } else if (childName.equals("rotation")) {
            return new RotationComponent(actor, child);
        } else if (childName.equals("scaling")) {
            return new ScalingComponent(actor, child);
        } else if (childName.equals("direction")) {
            return new DirectionComponent(actor, child);
        } else if (childName.equals("preloadSprite")) {
            return new PreloadSpriteComponent(actor, child);
        } else if ("sprite2d".equals(child.getName())) {
            return new Sprite2DComponent(actor, child);
        } else if ("position2d".equals(child.getName())) {
            return new Position2DComponent(actor, child);
        } else if ("size2d".equals(child.getName())) {
            return new Size2DComponent(actor, child);
        } else if ("preferences".equals(child.getName())) {
            return new PreferencesComponent(actor, child);
        } else if ("processManager".equals(child.getName())) {
            return new ProcessManagerComponent(actor, child);
        } else if ("composite".equals(child.getName())) {
            return new CompositeComponent(actor, child);
        } else if ("selfDestroy".equals(child.getName())) {
            return new SelfDestroyComponent(actor, child);
        } else if ("bitmapFont".equals(child.getName())) {
            return new BitmapFontComponent(actor, child);
        } else if ("text".equals(child.getName())) {
            return new TextComponent(actor, child);
        } else if ("color".equals(child.getName())) {
            return new ColorComponent(actor, child);
        } else if ("testCode".equals(child.getName())) {
            return new TestCodeActorComponent(actor, child);
        } else if ("sphereBounding".equals(child.getName())) {
            return new SphereBoundingActorComponent(actor, child);
        }

        throw new IllegalArgumentException("Unable to create component for node: " + child.getName());
    }
}
