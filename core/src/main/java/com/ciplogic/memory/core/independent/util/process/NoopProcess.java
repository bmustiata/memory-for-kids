package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class NoopProcess extends GameProcess {
    private String name;

    public NoopProcess() {
    }

    public NoopProcess(String name) {
        this.name = name;
    }

    @Override
    public void onUpdate(long delta) {
        finish();
    }
}
