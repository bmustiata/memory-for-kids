package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Size2DComponent;

import java.util.Collections;
import java.util.List;

public class SpriteScreenResizeWatchProcess extends GameProcess implements EventListener {
    private final GameWorld gameWorld;

    public SpriteScreenResizeWatchProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener( this );
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void onEvent(Event event) {
        for (String actorId : gameWorld.spriteHolders.keySet()) {
            Actor actor = gameWorld.get(actorId);

            Position2DComponent position2d = actor.getComponent(Position2DComponent.class);
            if (position2d != null) {
                position2d.restart();
            }

            Size2DComponent size2d = actor.getComponent(Size2DComponent.class);
            if (size2d != null) {
                size2d.restart();
            }
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(ScreenResizeEvent.class);
    }
}
