package com.ciplogic.memory.core.independent.application.input.event.mouse;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class MouseScrolledEvent extends InputEvent {
    public final int amount;

    public MouseScrolledEvent(int amount) {
        this.amount = amount;
    }
}
