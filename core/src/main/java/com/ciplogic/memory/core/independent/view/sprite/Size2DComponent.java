package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ScreenActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class Size2DComponent extends ScreenActorComponent {
    public final Size2D size2D;

    public Size2DComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        Sprite2DComponent sprite2DComponent = actor.getComponent(Sprite2DComponent.class);
        float aspectRatio = sprite2DComponent != null ?
                sprite2DComponent.width / sprite2DComponent.height :
                1.0f;

        size2D = Size2D.readFromNode(child, aspectRatio);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<size2d width=\"")
                .append(size2D.width)
                .append("\" height=\"")
                .append(size2D.height)
                .append("\" keepAspect=\"")
                .append(size2D.keepAspect)
                .append("\" screen=\"")
                .append(screenOrientation.toString().toLowerCase())
                .append("\"/>\n");

    }

    public Size2DComponent(Actor actor, String width, String height, Size2D.KeepAspect keepAspectRatio) {
        super(actor, null);

        Sprite2DComponent sprite2DComponent = actor.getComponent(Sprite2DComponent.class);
        float aspectRatio = sprite2DComponent.width / sprite2DComponent.height;

        size2D = Size2D.readFromValues(width, height, keepAspectRatio, aspectRatio);

        restart();
    }

    @Override
    public void onInit() {
        restart();
    }

    @Override
    public void restart() {
        Sprite2DComponent sprite2d = actor.getComponent(Sprite2DComponent.class);

        // FIXME:
        if (sprite2d != null) {
            sprite2d.setScaling(new Vector3(size2D.getScreenWidth(), size2D.getScreenHeight(), 1));
        }
    }
}
