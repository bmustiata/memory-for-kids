package com.ciplogic.memory.core.independent.logic.actor;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.BaseLight;
import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.actors.components.*;
import com.ciplogic.memory.core.independent.application.cache.ResourceCache;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;
import com.ciplogic.memory.core.lang.ModifiableLinkedHashMap;

import java.util.*;

/**
 * A game world keeps all the actors into a huge container.
 */
public abstract class GameWorld implements EventListener {
    public ResourceCache resourceCache = new ResourceCache();

    protected Map<String, Actor> actorMap = new ModifiableLinkedHashMap<String, Actor>();

    protected Map<String, RenderableProvider> renderableMap = new HashMap<String, RenderableProvider>();

    // Sprites are stored in layers. Layers that are < 0, are rendered before the 3D
    // rendering takes place. Layers that are >= 0 are rendered after the 3D rendering.
    public Map<Integer, Map<String, RenderableProvider>> backgroundSprites =
            new TreeMap<Integer, Map<String, RenderableProvider>>();
    public Map<Integer, Map<String, RenderableProvider>> foregroundSprites =
            new TreeMap<Integer, Map<String, RenderableProvider>>();
    public Map<Integer, Map<String, TextComponent>> textLayers = new TreeMap<Integer, Map<String, TextComponent>>();

    public Map<String, BitmapFontComponent> fontMap = new HashMap<String, BitmapFontComponent>();

    public Map<String, ModelInstanceComponent> modelInstanceHolders = new HashMap<String, ModelInstanceComponent>();

    // linked hash map, since the registering of textures should be done in the same order as the creation.
    public Map<String, Sprite2DComponent> spriteHolders = new LinkedHashMap<String, Sprite2DComponent>();

    public Map<String, PreloadSpriteComponent> preloadSpriteHolders = new HashMap<String, PreloadSpriteComponent>();

    protected Map<String, BaseLight> lights = new HashMap<String, BaseLight>();
    protected LifecycleActorsCollection lifecycledActors = new LifecycleActorsCollection();

    protected Environment environment = null;

    public boolean previouslyLoaded;

    protected GameWorld() {
        EventBus.INSTANCE.registerListener(this);
    }

    public Actor get(String id) {
        Actor actor = actorMap.get(id);

        if (actor == null) {
            throw new IllegalArgumentException("Unable to find actor with id `" + id + "` in the game world.");
        }

        return actor;
    }

    public Actor hasActor(String id) {
        return actorMap.get(id);
    }

    public synchronized void addActor(Actor actor) {
        addActor(actor, false);
    }

    public synchronized void addActor(Actor actor, boolean ignoreActorMap) {
        assert actor.id != null;

        if (!ignoreActorMap) {
            actorMap.put(actor.id, actor);
        }

        ModelInstanceComponent modelInstanceComponent = actor.getComponent(ModelInstanceComponent.class);
        if (modelInstanceComponent != null) {
            modelInstanceHolders.put(actor.id, modelInstanceComponent);
            if (previouslyLoaded) {
                registerRenderable( modelInstanceComponent, actor.id );
            }
        }

        Sprite2DComponent spriteComponent = actor.getComponent(Sprite2DComponent.class);
        if (spriteComponent != null) {
            spriteHolders.put(actor.id, spriteComponent);
            if (previouslyLoaded) {
                registerTexture( spriteComponent, actor.id );
            }
        }

        BitmapFontComponent bitmapFontComponent = actor.getComponent(BitmapFontComponent.class);
        if (bitmapFontComponent != null) {
            fontMap.put( bitmapFontComponent.font, bitmapFontComponent );
        }

        TextComponent textComponent = actor.getComponent(TextComponent.class);
        if (textComponent != null) {
            registerText(actor.id, textComponent);
        }

        PreloadSpriteComponent preloadSpriteComponent = actor.getComponent(PreloadSpriteComponent.class);
        if (preloadSpriteComponent != null) {
            preloadSpriteHolders.put(actor.id, preloadSpriteComponent);
        }

        DirectionalLightComponent directionalLightComponent = actor.getComponent(DirectionalLightComponent.class);
        if (directionalLightComponent != null) {
            lights.put(actor.id, directionalLightComponent.light);
            rebuildEnvironment();
        }

        PointLightComponent pointLightComponent = actor.getComponent(PointLightComponent.class);
        if (pointLightComponent != null) {
            lights.put(actor.id, pointLightComponent.light);
            rebuildEnvironment();
        }

        if (!ignoreActorMap) {
            if (actor.lifecycledComponents != null) {
                lifecycledActors.put(actor.id, actor);
            }

            for (ActorComponent actorComponent : actor.components.values()) {
                actorComponent.onInit();
            }

            for (ScreenActorComponent actorComponent : actor.getScreenAwareComponents()) {
                actorComponent.callScreenOnInit();
            }
        }
    }

    private void registerText(String id, TextComponent textComponent) {
        Map<String, TextComponent> textLayer = textLayers.get(textComponent.layer);

        if (textLayer == null) {
            textLayer = new HashMap<String, TextComponent>();
            textLayers.put(textComponent.layer, textLayer);
        }

        textLayer.put(id, textComponent);
        textComponent.font = fontMap.get( textComponent.fontName );

        if (textComponent.font == null) {
            throw new IllegalStateException("Unable to load font " + textComponent.fontName);
        }

        assert textComponent.font != null;
    }

    private void unregisterText(String id, TextComponent textComponent) {
        Map<String, TextComponent> textLayer = textLayers.get(textComponent.layer);

        textLayer.remove(id);
    }

    public synchronized void removeActor(Actor actor) {
        removeActor(actor, false);
    }

    public synchronized void removeActor(Actor actor, boolean ignoreActorMap) {
        if (!actor.alive) { // if the actor is already removed, we're being very likely recursively called
            return;
        }

        if (!ignoreActorMap) {
            actor.alive = false;

            for (ActorComponent actorComponent : actor.components.values()) {
                actorComponent.onFinish();
            }

            // FIXME: some optimizations could be in order.
            for (Map<ScreenOrientation, ScreenActorComponent> screenOrientationComponentMap : actor.screenAwareComponents.values()) {
                for (ScreenActorComponent screenActorComponent : screenOrientationComponentMap.values()) {
                    screenActorComponent.callScreenOnFinish();
                }
            }

            actorMap.remove(actor.id);
        }

        ModelInstanceComponent modelInstanceComponent = actor.getComponent(ModelInstanceComponent.class);
        if (modelInstanceComponent != null) {
            modelInstanceHolders.remove(actor.id);
        }

        Sprite2DComponent spriteComponent = actor.getComponent(Sprite2DComponent.class);
        if (spriteComponent != null) {
            spriteHolders.remove(actor.id);
        }

        BitmapFontComponent bitmapFontComponent = actor.getComponent(BitmapFontComponent.class);
        if (bitmapFontComponent != null) {
            fontMap.remove( bitmapFontComponent.font );
        }

        TextComponent textComponent = actor.getComponent(TextComponent.class);
        if (textComponent != null) {
            unregisterText(actor.id, textComponent);
        }

        PreloadSpriteComponent preloadSpriteComponent = actor.getComponent(PreloadSpriteComponent.class);
        if (preloadSpriteComponent != null) {
            preloadSpriteHolders.remove(actor.id);
        }

        DirectionalLightComponent directionalLightComponent = actor.getComponent(DirectionalLightComponent.class);
        if (directionalLightComponent != null) {
            lights.remove(actor.id);
            rebuildEnvironment();
        }

        if (!ignoreActorMap) {
            if (actor.lifecycledComponents != null) {
                lifecycledActors.remove(actor.id);
            }
        }

        unregisterRenderable(actor.getComponent(ModelInstanceComponent.class), actor.id);
        unregisterTexture(actor.getComponent(Sprite2DComponent.class), actor.id);
    }

    private void unregisterTexture(Sprite2DComponent component, String id) {
        if (component == null) {
            return; // no texture to unregister.
        }

        Map<String, RenderableProvider> spriteRenderableLayer = getSpriteLayer(component.layer);
        spriteRenderableLayer.remove(id);

        if (spriteRenderableLayer.isEmpty()) {
            removeSpriteLayer(component.layer);
        }
    }

    private void unregisterRenderable(ModelInstanceComponent component, String id) {
        renderableMap.remove(id);
    }

    public void createRenderables() {
        for (Map.Entry<String, ModelInstanceComponent> entry : modelInstanceHolders.entrySet()) {
            ModelInstanceComponent modelInstanceComponent = entry.getValue();
            String actorId = entry.getKey();

            registerRenderable(modelInstanceComponent, actorId);
        }

        for (Map.Entry<String, Sprite2DComponent> entry : spriteHolders.entrySet()) {
            Sprite2DComponent spriteComponent = entry.getValue();
            String actorId = entry.getKey();

            registerTexture(spriteComponent, actorId);
        }

        this.previouslyLoaded = true;
    }

    private void registerRenderable(ModelInstanceComponent modelInstanceComponent, String actorId) {
        if (modelInstanceComponent == null) {
            return;
        }

        Model model = resourceCache.get(modelInstanceComponent.modelId);

        if (model != null) {
            ModelInstance modelInstance = new ModelInstance(model);
            modelInstanceComponent.setModelInstance(modelInstance);
            renderableMap.put(actorId, modelInstance);
        }
    }

    private void registerTexture(Sprite2DComponent spriteComponent, String actorId) {
        if (spriteComponent == null) {
            return;
        }

        spriteComponent.texture = resourceCache.getTexture(spriteComponent.textureId);

        if (spriteComponent.texture == null) {
            return;
        }

        ModelInstance modelInstance = new ModelInstance(resourceCache.get(spriteComponent.modelId)); // FIXME: the name of the resource should be in only one place.
        modelInstance.materials.get(0).get(TextureAttribute.class, TextureAttribute.Diffuse).textureDescription.texture = spriteComponent.texture;
        spriteComponent.setModelInstance(modelInstance);

        Map<String, RenderableProvider> layerRenderables = getSpriteLayer(spriteComponent.layer);
        if (layerRenderables == null) {
            layerRenderables = createSpriteLayer(spriteComponent.layer);
        }

        layerRenderables.put(actorId, modelInstance);
    }

    private Map<String, RenderableProvider> createSpriteLayer(Integer layer) {
        Map<String, RenderableProvider> layerRenderables;
        layerRenderables = new ModifiableLinkedHashMap<String, RenderableProvider>();

        if (layer >= 0) {
            foregroundSprites.put(layer, layerRenderables);
        } else {
            backgroundSprites.put(layer, layerRenderables);
        }

        return layerRenderables;
    }

    private Map<String, RenderableProvider> removeSpriteLayer(Integer layer) {
        if (layer >= 0) {
            return foregroundSprites.remove(layer);
        } else {
            return backgroundSprites.remove(layer);
        }
    }

    private Map<String, RenderableProvider> getSpriteLayer(Integer layer) {
        if (layer >= 0) {
            return foregroundSprites.get(layer);
        } else {
            return backgroundSprites.get(layer);
        }
    }

    private void rebuildEnvironment() {
        environment = new Environment();

        for (BaseLight light : lights.values()) {
            environment.add(light);
        }
    }

    public boolean isEmpty() {
        return actorMap.isEmpty();
    }

    public abstract ActorFactory getActorFactory();

    public synchronized Iterable<RenderableProvider> getRenderables() {
        return renderableMap.values();
    }

    public Environment getEnvironment() {
        return environment;
    }

    public Collection<Sprite2DComponent> getSprites() {
        return spriteHolders.values();
    }

    public Map<Class, Set<String>> getUsedResourcesIds() {
        Map<Class, Set<String>> result = new HashMap<Class, Set<String>>();

        Set<String> models = new HashSet<String>();

        // FIXME: a bit to hardcodaisies.
        models.add("core/sprite.g3dj");

        for (ModelInstanceComponent value : modelInstanceHolders.values()) {
            models.add(value.modelId);
        }
        result.put(Model.class, models);

        Set<String> textures = new HashSet<String>();
        for (Sprite2DComponent value : spriteHolders.values()) {
            textures.add(value.textureId);
        }

        for (PreloadSpriteComponent preloadSpriteComponent : preloadSpriteHolders.values()) {
            for (String sprite : preloadSpriteComponent.spriteList) {
                textures.add( sprite );
            }
        }

        result.put(Texture.class, textures);

        return result;
    }

    @Override
    public void onEvent(Event event) {
        // even if we listen for only one event type, we make sure to check the type, in order not to fail
        // if child classes call super.onEvent
        if (event instanceof OpenGlContextLostEvent) {
            resourceCache.dispose();
            resourceCache = new ResourceCache();
        }
    }

    /**
     * Return a modifiable list of listened events, so child classes can override this.
     * @param <T>
     * @return
     */
    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List result = new ArrayList();

        result.add( OpenGlContextLostEvent.class );

        return result;
    }

    public Iterable<Actor> getLifecycledActors() {
        return lifecycledActors;
    }

    public Iterable<Actor> getAllActors() {
        return actorMap.values();
    }

    public synchronized void reloadActor(Actor actor) {
        removeActor(actor, true);
        addActor(actor, true);
        registerRenderable(actor.getComponent(ModelInstanceComponent.class), actor.id);
        registerTexture(actor.getComponent(Sprite2DComponent.class), actor.id);
    }

    public synchronized void nuke() {
        for (Actor actor : actorMap.values()) {
            removeActor(actor);
        }
        previouslyLoaded = false;
    }

    public Actor createActor(String actorResource, Map<String, String> actorParameters) {
        return this.getActorFactory().createActor(actorResource, actorParameters);
    }

    public Actor createActor(String actorCode) {
        return this.getActorFactory().createActor(actorCode);
    }

    /**
     * Allows finding all the components with a specific type across
     * all the actors.
     * @param clazz
     * @param <T>
     * @return
     */
    public <T extends ActorComponent> Set<T> findComponents(Class<T> clazz) {
        Set<T> result = new HashSet<T>();

        for (Actor actor : actorMap.values()) {
            T foundComponent = actor.getComponent(clazz);

            if (foundComponent != null) {
                result.add(foundComponent);
            }
        }

        return result;
    }
}
