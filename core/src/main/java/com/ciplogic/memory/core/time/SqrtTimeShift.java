package com.ciplogic.memory.core.time;

public class SqrtTimeShift extends TimeShift {
    @Override
    public float compute(long start, long duration, long current) {
        if (start + duration < current) {
            return 1.0f;
        }

        float linearValue = ((float) (current - start)) / duration;

        return (float) Math.sqrt(linearValue);
    }
}
