package com.ciplogic.memory.core.independent.application.process;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;

import java.util.Collections;
import java.util.List;

/**
 * This is a process manager that also listen the bus in order to allow components
 * to create other processes.
 */
public class GlobalGameProcessManager extends GameProcessManager implements EventListener {
    public GlobalGameProcessManager() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onEvent(Event event) {
        addProcess( ((CreateGameProcess) event).gameProcess );
    }

    @Override
    public List<Class<CreateGameProcess>> getListenedEvents() {
        return Collections.singletonList(CreateGameProcess.class);
    }

    @Override
    public void shutdown() {
        super.shutdown();
        EventBus.INSTANCE.unregisterListener(this);
    }

    public void restart() {
        // very specific to the global game process manager listening the bus for new processes.
        EventBus.INSTANCE.registerListener(this);
    }
}
