package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;

public class ScalingComponent extends ActorComponent {
    public Scalable scalable;
    public Vector3 original_scaling = new Vector3();

    public ScalingComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        for (ActorComponent component : actor.components.values()) {
            if (component instanceof Scalable) {
                scalable = (Scalable) component;
            }
        }

        assert scalable != null;

        original_scaling.set(
                floatAttribute(child, "x"),
                floatAttribute(child, "y"),
                floatAttribute(child, "z"));

        scalable.setScaling(original_scaling);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<scaling x=\"")
                .append(scalable.getScaling().x)
                .append("\" y=\"")
                .append(scalable.getScaling().y)
                .append("\" z=\"")
                .append(scalable.getScaling().z)
                .append("\"/>\n");
    }

    @Override
    public void restart() {
        scalable.setScaling(original_scaling);
    }
}
