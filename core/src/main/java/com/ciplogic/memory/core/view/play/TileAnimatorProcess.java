package com.ciplogic.memory.core.view.play;

import com.badlogic.gdx.math.Vector3;
import com.ciplogic.memory.core.actors.components.ModelInstanceComponent;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.application.process.CreateGameProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.util.RandomGenerator;
import com.ciplogic.memory.core.independent.util.process.*;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.logic.play.PlayGameWorld;
import com.ciplogic.memory.core.view.play.component.BoardPieceComponent;
import com.ciplogic.memory.core.view.play.component.event.*;

import java.util.ArrayList;
import java.util.List;

/**
 * The TileAnimatorProcess monitors the EventBus for game events (clicked on a tile,
 * game won), and animates the tiles when events happen.
 */
public class TileAnimatorProcess extends GameProcess implements EventListener {
    private final PlayGameWorld gameWorld;
    private GameProcessManager gameProcessManager = new GameProcessManager();
    public boolean animating;

    private GroupProcess removeProcesses;

    public final static float PIECE_DISTANCE = 0.01f;

    public TileAnimatorProcess(PlayGameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);

        if (animating && gameProcessManager.isEmpty()) {
            animating = false;
        }
    }

    @Override
    public void onFinish() {
        gameProcessManager.shutdown();
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof ShowPiecesEvent) {
            ShowPiecesEvent showPiecesEvent = (ShowPiecesEvent) event;
            showActor(showPiecesEvent.boardPieceComponent.actor);
        } else if (event instanceof HidePiecesEvent) {
            HidePiecesEvent hidePiecesEvent = (HidePiecesEvent) event;
            hideActors(hidePiecesEvent.firstPiece.actor, hidePiecesEvent.secondPiece.actor);
        } else if (event instanceof RemovePiecesEvent) {
            RemovePiecesEvent removePiecesEvent = (RemovePiecesEvent) event;
            removeActors(removePiecesEvent.pieces);
        } else if (event instanceof CreatedPiecesEvent) {
            CreatedPiecesEvent createdPiecesEvent = (CreatedPiecesEvent) event;
            createActors(createdPiecesEvent.createdPieces);
        } else if (event instanceof GameWonEvent) {
            gameWon();
        } else if (event instanceof PiecesPairedEvent) {
            PiecesPairedEvent piecesPairedEvent = (PiecesPairedEvent) event;
            piecesPaired(piecesPairedEvent.firstPiece.actor, piecesPairedEvent.secondPiece.actor);
        }

        animating = true;
    }

    private void piecesPaired(Actor ... actor1) {
        for (Actor actor : actor1) {
            Vector3 position = actor.getComponent(ModelInstanceComponent.class).getPosition();
            actor.getComponent(ProcessManagerComponent.class).addProcess(
                new DelayProcess(400).then(
                    new LinearTranslateAnimationProcess(actor, 150).to( position.x, position.y, position.z + 0.2f ).then(
                        new LinearTranslateAnimationProcess(actor, 150).to( position.x, position.y, position.z )
                    )
                )
            );
        }
    }

    private void gameWon() {
        /**
         * When the game is won, we queue the showing of the new game button after the
         * animation of the pieces is done. Since the animation is checked by looking if
         * there is any process still running, the finishedAnimating process will be launched
         * on the global process manager.
         */
        ConditionProcess finishedAnimating = new ConditionProcess(new ConditionProcess.FinishCondition() {
            @Override
            public boolean check() {
                return !animating;
            }
        });
        // run it on the global processes since running it locally will never end the animation.
        EventBus.INSTANCE.fire(new CreateGameProcess(finishedAnimating));

        switch (RandomGenerator.nextInt(3)) {
            case 0: simpleRotateAnimation(finishedAnimating); break;
            case 1: rotateAtDifferentSpeeds(finishedAnimating); break;
            case 2: moveUpAndDown(finishedAnimating); break;
        }

        Actor newGameButton = gameWorld.get("newGameButton");
        newGameButton.getComponent(ProcessManagerComponent.class).addProcess(
            new WaitForFinishProcess(finishedAnimating).then(
                new Translate2DProcess( newGameButton.getComponent(Position2DComponent.class), 40, -45, 500 )
            )
        );
    }

    private void simpleRotateAnimation(ConditionProcess finishedAnimating) {
        for (Actor piece : gameWorld.getAllActors()) {
            if (piece.getComponent(BoardPieceComponent.class) != null) {
                ModelInstanceComponent modelInstanceComponent = piece.getComponent(ModelInstanceComponent.class);
                piece.getComponent(ProcessManagerComponent.class).addProcess(
                    new WaitForFinishProcess(finishedAnimating).then(
                        new ContinuousRotateModelInstanceProcess(modelInstanceComponent, 360, 0, 0, 4000)
                    )
                );
            }
        }
    }

    private void rotateAtDifferentSpeeds(ConditionProcess finishedAnimating) {
        for (Actor piece : gameWorld.getAllActors()) {
            if (piece.getComponent(BoardPieceComponent.class) != null) {
                ModelInstanceComponent modelInstanceComponent = piece.getComponent(ModelInstanceComponent.class);
                piece.getComponent(ProcessManagerComponent.class).addProcess(
                    new WaitForFinishProcess(finishedAnimating).then(
                        new ContinuousRotateModelInstanceProcess(modelInstanceComponent, 360, 0, 0, 500 + RandomGenerator.nextInt(5500))
                    )
                );
            }
        }
    }

    private void moveUpAndDown(ConditionProcess finishedAnimating) {
        float i = 1;

        for (Actor piece : gameWorld.getAllActors()) {
            if (piece.getComponent(BoardPieceComponent.class) != null) {
                ModelInstanceComponent modelInstanceComponent = piece.getComponent(ModelInstanceComponent.class);

                float originalX = modelInstanceComponent.getPosition().x,
                      originalY = modelInstanceComponent.getPosition().y;

                piece.getComponent(ProcessManagerComponent.class).addProcess(
                    new WaitForFinishProcess(finishedAnimating).then(
                        new LazyRunnableProcess( createRunnableProcess(finishedAnimating, i, piece, originalX, originalY) )
                    )
                );

                i = -1 * i;
            }
        }
    }

    private Runnable createRunnableProcess(final ConditionProcess finishedAnimating, final float i, final Actor piece, final float originalX, final float originalY) {
        final Runnable runnable[] = new Runnable[1];

        runnable[0] = new Runnable() {
            @Override
            public void run() {
                piece.getComponent(ProcessManagerComponent.class).addProcess(
                    new WaitForFinishProcess(finishedAnimating).then(
                        new LinearTranslateAnimationProcess(piece, 400).to(originalX, originalY, i * 0.4f).then(
                            new LinearTranslateAnimationProcess(piece, 400).to(originalX, originalY, i * -0.4f).then(
                                new LazyRunnableProcess(runnable[0])
                            )
                        )
                    )
                );
            }
        };

        return runnable[0];
    }

    private void createActors(final List<Actor> createdPieces) {
        int index = 0;

        float rows = createdPieces.size() / 4f;
        float delta = (rows - 1f) * (1f + PIECE_DISTANCE / 2f);

        GroupProcess createActorsProcess = new GroupProcess();

        for (Actor actor : createdPieces) {
            createActorsProcess.addProcess(
                    new LinearTranslateAnimationProcess(actor, 0).to(20, 0, 0).then(
                            new LinearTranslateAnimationProcess(actor, 300).to(
                                    -(3 + PIECE_DISTANCE + PIECE_DISTANCE / 2) + (2 + PIECE_DISTANCE) * (index % 4),
                                    -delta + (2 + PIECE_DISTANCE) * (index / 4), 0)
                    )
            );

            index++;
        }

        Actor newGameButton = gameWorld.get("newGameButton");
        newGameButton.getComponent(ProcessManagerComponent.class).addProcess(
            new Translate2DProcess( newGameButton.getComponent(Position2DComponent.class), 100, -45, 500 )
        );

        gameProcessManager.addProcess(
            new RunnableProcess(new Runnable() {
                @Override
                public void run() {
                    for (Actor actor : createdPieces) {
                        actor.getComponent(ModelInstanceComponent.class).setPosition(new Vector3(0, 20, 0));
                    }
                }
            }).then(
                new WaitForFinishProcess( removeProcesses ).then(
                    createActorsProcess
                )
            )
        );
    }

    private void removeActors(List<Actor> pieceActors) {
        int x = 0;

        assert removeProcesses == null;

        removeProcesses = new GroupProcess();

        for (Actor actor : pieceActors) {
            removeProcesses.addProcess(
                new DelayProcess(x++ * 10).then(
                    new LinearTranslateAnimationProcess(actor, 300).to(-10, -10, 0)
                        .then(new DestroyActorProcess(actor))
                )
            );
        }

        gameProcessManager.addProcess(removeProcesses.then(
            new RunnableProcess(new Runnable() {
                @Override
                public void run() {
                    removeProcesses = null;
                }
            })
        ));
    }

    private void showActor(Actor actor) {
        gameProcessManager.addProcess(
            new RotateModelInstanceProcess(actor.getComponent(ModelInstanceComponent.class), 180, 0, 0, 400)
        );
    }

    private void hideActors(Actor actor1, Actor actor2) {
        gameProcessManager.addProcess(
            new DelayProcess(1000).then(
                new RotateModelInstanceProcess(actor1.getComponent(ModelInstanceComponent.class), 180, 0, 0, 400),
                new RotateModelInstanceProcess(actor2.getComponent(ModelInstanceComponent.class), 180, 0, 0, 400)
            )
        );
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List result = new ArrayList();

        result.add( HidePiecesEvent.class );
        result.add( ShowPiecesEvent.class );
        result.add( PiecesPairedEvent.class );
        result.add( RemovePiecesEvent.class );
        result.add( CreatedPiecesEvent.class );
        result.add( GameWonEvent.class );

        return result;
    }
}
