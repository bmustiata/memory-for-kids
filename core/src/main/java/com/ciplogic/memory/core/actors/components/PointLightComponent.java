package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.util.XmlUtil;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class PointLightComponent extends ActorComponent implements Positionable {
    private final float intensity;

    public PointLight light = new PointLight();

    public PointLightComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        intensity = XmlUtil.floatAttribute(child, "intensity", "1");

        light.set(Color.WHITE, 1, 1, 1, intensity);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<pointLight " +
                        "intensity=\"" + intensity +"\"" +
                        "/>\n");
    }

    @Override
    public void setPosition(Vector3 position) {
        light.set(light.color, position, light.intensity);
    }

    @Override
    public Vector3 getPosition() {
        return light.position;
    }
}
