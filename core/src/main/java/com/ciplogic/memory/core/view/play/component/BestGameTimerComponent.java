package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.PreferencesComponent;
import com.ciplogic.memory.core.actors.components.TextComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.play.component.event.TimedGameWonEvent;

import java.util.Collections;
import java.util.List;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class BestGameTimerComponent extends ActorComponent implements EventListener {
    private final PreferencesComponent preferences;
    private long bestTime;

    private TextComponent bestTimeText;
    private final TextComponent bestTimeShadowText;

    public BestGameTimerComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        preferences = actor.gameWorld.get("preferences").getComponent(PreferencesComponent.class);
        bestTime = Long.parseLong( preferences.getString("best-time", "-1") );

        bestTimeText = actor.gameWorld.get("best-game-time").getComponent(TextComponent.class);
        bestTimeShadowText = actor.gameWorld.get("best-game-time-shadow").getComponent(TextComponent.class);

        bestTimeText.text = TimeFormatter.formatEllapsedTime( bestTime );
        bestTimeShadowText.text = bestTimeText.text;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<bestGameTimer/>\n");
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onEvent(Event event) {
        TimedGameWonEvent timedGameWonEvent = (TimedGameWonEvent) event;

        if (timedGameWonEvent.ellapsedTime < bestTime || bestTime < 0) {
            bestTime = timedGameWonEvent.ellapsedTime;
            preferences.putString("best-time", "" + bestTime);
            bestTimeText.text = TimeFormatter.formatEllapsedTime( bestTime );
            bestTimeShadowText.text = bestTimeText.text;
        }
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(TimedGameWonEvent.class);
    }
}
