package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.math.Vector3;
import com.ciplogic.memory.core.actors.components.Rotatable;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

public class RotateModelInstanceProcess extends GameProcess {
    private final Rotatable rotatable;
    private final float x;
    private final float y;
    private final float z;

    private final long millis;

    private long initialDelta;
    private Vector3 initialRotation;

    private Vector3 computationRotation = new Vector3();

    /**
     * Rotate a model instance
     * @param rotatable
     * @param x X axis angle (0-360)
     * @param y Y axis angle (0-360)
     * @param z Z axis angle (0-360)
     * @param millis
     */
    public RotateModelInstanceProcess(Rotatable rotatable, float x, float y, float z, long millis) {
        this.rotatable = rotatable;
        this.x = x;
        this.y = y;
        this.z = z;
        this.millis = millis;
    }

    @Override
    public void onInit(long delta) {
        super.onInit(delta);

        this.initialDelta = delta;
        initialRotation = this.rotatable.getRotation();
    }

    @Override
    public void onUpdate(long delta) {
        float passed = (float)(delta - initialDelta) / millis;

        if (passed >= 1.0f) {
            finish();

            return;
        }

        computationRotation.set(
                initialRotation.x + x * passed,
                initialRotation.y + y * passed,
                initialRotation.z + z * passed
        );

        rotatable.setRotation(computationRotation);
    }

    @Override
    public void onFinish() {
        computationRotation.set(
                initialRotation.x + x,
                initialRotation.y + y,
                initialRotation.z + z
        );
        rotatable.setRotation(computationRotation);
    }
}
