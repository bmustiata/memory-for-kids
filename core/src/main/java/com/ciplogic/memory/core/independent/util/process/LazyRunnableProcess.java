package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;

/**
 * <p>A lazy runnable process unlike a RunnableProcess doesn't guarantee that
 * its runnable will be run, since it will call it on the onUpdate lifecycle.</p>
 *
 * <p>This is particularily good when running processes that re-register themselves
 * in order to provide looping animations.</p>
 */
public class LazyRunnableProcess extends GameProcess {
    private final Runnable runnable;

    public LazyRunnableProcess(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    public void onUpdate(long delta) {
        try {
            this.runnable.run();
        } finally {
            finish();
        }
    }
}
