package com.ciplogic.memory.core.view.menu.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.view.menu.MenuType;

public class MenuOptionChangedEvent extends Event {
    public final MenuType menuType;
    public final String currentOption;

    public MenuOptionChangedEvent(MenuType menuType, String currentOption) {
        this.menuType = menuType;
        this.currentOption = currentOption;
    }
}
