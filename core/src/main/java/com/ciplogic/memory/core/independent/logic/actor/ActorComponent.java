package com.ciplogic.memory.core.independent.logic.actor;

import com.badlogic.gdx.utils.XmlReader;

public abstract class ActorComponent {
    public Actor actor;

    public boolean lifecycled; // if and only if the lifecycled attribute is setPosition, the onUpdate will be called.

    public ActorComponent(Actor actor, XmlReader.Element child) {
        this.actor = actor;
    }

    /**
     * Called immediately after the actor is attached to the world. The GameWorld is not yet fully
     * initialized at this point.
     */
    public void onInit() {
    }

    /**
     * Called only if the lifecycled attribute is set to true.
     * @param delta
     */
    public void onUpdate(long delta) {
    }

    /**
     * Called right before the actor is detached from the world.
     */
    public void onFinish() {
    }

    public void restart() {
    }

    /**
     * Write the current component as an XML node into the given stringBuilder.
     * @param stringBuilder
     */
    public abstract void serializeXml(int indentLevel, StringBuilder stringBuilder);
}
