package com.ciplogic.memory.core.independent.gdxinput.keyboard;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyDownEvent;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyTypedEvent;
import com.ciplogic.memory.core.independent.application.input.event.keyboard.KeyUpEvent;
import com.ciplogic.memory.core.independent.application.process.GameProcess;

/**
 * A keyboard input process is an application layer process that monitors the keyboard,
 * and generates event on the game bus regarding keypresses.
 */
public class GdxKeyboardInputProcess extends GameProcess implements InputProcessor {
    @Override
    public void onInit() {
        state = State.PAUSED; // make sure we're not calling onUpdate() for no reason

        InputMultiplexer inputMultiplexer = (InputMultiplexer) Gdx.input.getInputProcessor();
        inputMultiplexer.addProcessor(this);
    }

    @Override
    public void onFinish() {
        removeGdxInputProcessor();
    }

    @Override
    public void onFail() {
        removeGdxInputProcessor();
    }

    private void removeGdxInputProcessor() {
        InputMultiplexer inputMultiplexer = (InputMultiplexer) Gdx.input.getInputProcessor();
        // since we only add from processes, removal should be the last oen.
        inputMultiplexer.removeProcessor(inputMultiplexer.getProcessors().size - 1);
    }

    @Override
    public void onUpdate(long delta) {
        // if by accident this process is resumed, paused it again. this method should not be called.
        state = State.PAUSED;
    }

    @Override
    public boolean keyDown(int keycode) {
        EventBus.INSTANCE.fire( new KeyDownEvent(keycode) );
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        EventBus.INSTANCE.fire( new KeyUpEvent(keycode) );
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        EventBus.INSTANCE.fire( new KeyTypedEvent(character) );
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
