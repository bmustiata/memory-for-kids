package com.ciplogic.memory.core.logic;

import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.Actor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A class that allows reading actors from external files. It keeps a cache of the actors for performance
 * reasons.
 */
public class ExternActorParser {
    private static Map<String, ParsedXmlTree> xmlTreeCache = new HashMap<String, ParsedXmlTree>();

    private XmlActorBuilder xmlActorBuilder;

    public ExternActorParser(XmlActorBuilder xmlActorBuilder) {
        this.xmlActorBuilder = xmlActorBuilder;
    }

    /**
     * Create an actor, reading also its external resource file, and using the given
     * arguments for dynamic expressions.
     * @param fileName
     * @param actorArguments
     * @return
     */
    public Actor createActor(String fileName, Map<String, String> actorArguments) {
        ParsedXmlTree parsedXmlTree = xmlTreeCache.get(fileName);

        if (parsedXmlTree == null) {
            parsedXmlTree = loadXmlTree(fileName);
            xmlTreeCache.put(fileName, parsedXmlTree);
        }

        for (Map.Entry<String, String> entry : parsedXmlTree.defaultValues.entrySet()) {
            if (!actorArguments.containsKey(entry.getKey())) {
                actorArguments.put(entry.getKey(), entry.getValue());
            }
        }

        XmlReader.Element node = parsedXmlTree.transform(actorArguments).rootNode;

        String actorId = actorArguments.get("id");
        if (actorId != null) {
            return xmlActorBuilder.readActor(node, actorId);
        } else {
            return xmlActorBuilder.readActor(node);
        }
    }

    /**
     * Loads an XML file, and keep a cached version of all the expression nodes.
     * @param fileName
     * @return
     */
    private ParsedXmlTree loadXmlTree(String fileName) {
        ParsedXmlTree result;

        fileName = fileName + ".actor.xml";

        try {
            InternalFileHandleResolver internalFileResolver = new InternalFileHandleResolver();
            XmlReader.Element element = new XmlReader().parse(internalFileResolver.resolve(fileName));

            result = new ParsedXmlTree(element);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }

        return result;
    }
}
