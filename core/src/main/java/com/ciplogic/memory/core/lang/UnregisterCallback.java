package com.ciplogic.memory.core.lang;

public interface UnregisterCallback {
    void unregister();
}
