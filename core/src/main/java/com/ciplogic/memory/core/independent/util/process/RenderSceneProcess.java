package com.ciplogic.memory.core.independent.util.process;

import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.ciplogic.memory.core.actors.components.CameraComponent;
import com.ciplogic.memory.core.independent.application.graphics.CiplogicShaderProvider;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

import static com.ciplogic.memory.core.independent.application.graphics.ShaderManager.fragmentShaderCode;
import static com.ciplogic.memory.core.independent.application.graphics.ShaderManager.vertexShaderCode;

public class RenderSceneProcess extends GameProcess {
    private final GameWorld gameWorld;
    private final CameraComponent cameraComponent;

    private ModelBatch modelBatch;

    public RenderSceneProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;

        cameraComponent = gameWorld.get("camera").getComponent(CameraComponent.class);
    }

    @Override
    public void onInit() {
        modelBatch = new ModelBatch(null, new CiplogicShaderProvider(gameWorld, vertexShaderCode, fragmentShaderCode));
    }

    @Override
    public void onUpdate(long delta) {
        modelBatch.begin( cameraComponent.camera );
        modelBatch.render( gameWorld.getRenderables(), gameWorld.getEnvironment() );
        modelBatch.end();
    }

    @Override
    public void onFinish() {
        modelBatch.dispose();
        modelBatch = null;
    }
}
