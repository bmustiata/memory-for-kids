package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class SpriteComponent extends ActorComponent {
    public final String textureId;

    public final float x;
    public final float y;
    public final float width;
    public final float height;

    public Texture texture;

    public SpriteComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        this.textureId = child.getAttribute("textureId");
        this.x = Float.parseFloat(child.getAttribute("x"));
        this.y = Float.parseFloat(child.getAttribute("y"));
        this.width = Float.parseFloat(child.getAttribute("width"));
        this.height = Float.parseFloat(child.getAttribute("height"));
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                .append("<sprite textureId=\"")
                .append(textureId)
                .append("\" x=\"")
                .append(x)
                .append("\" y=\"")
                .append(y)
                .append("\" width=\"")
                .append(width)
                .append("\" height=\"")
                .append(height)
                .append("\"/>\n");
    }
}
