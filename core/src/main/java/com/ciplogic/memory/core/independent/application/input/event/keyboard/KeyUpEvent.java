package com.ciplogic.memory.core.independent.application.input.event.keyboard;

import com.ciplogic.memory.core.independent.application.input.event.InputEvent;

public class KeyUpEvent extends InputEvent {
    public int keyCode;

    public KeyUpEvent(int keyCode) {
        this.keyCode = keyCode;
    }
}
