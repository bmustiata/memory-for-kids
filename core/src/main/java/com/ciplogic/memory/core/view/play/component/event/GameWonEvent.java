package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;

public class GameWonEvent extends Event {
    public String deck;
    public int totalPairs;

    public GameWonEvent(String deck, int totalPairs) {
        this.deck = deck;
        this.totalPairs = totalPairs;
    }
}
