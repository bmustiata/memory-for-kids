package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.PreferencesComponent;
import com.ciplogic.memory.core.actors.components.PreloadSpriteComponent;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.menu.MenuType;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class LoadSelectedPackComponent extends ActorComponent {
    private PreferencesComponent preferencesComponent;

    public LoadSelectedPackComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

    }

    @Override
    public void onInit() {
        preferencesComponent = actor.gameWorld.get("preferences").getComponent(PreferencesComponent.class);

        XmlReader.Element element;
        String packName = MenuType.getOption(preferencesComponent, MenuType.DECK);
        String packPrefix = "play/pack/" + packName + "/";

        element = parsePackXml(packPrefix);

        PreloadSpriteComponent preloadSpriteComponent = actor.getComponent(PreloadSpriteComponent.class);
        PackDataComponent packDataComponent = actor.getComponent(PackDataComponent.class);

        packDataComponent.setPackPrefix(packPrefix);

        for (XmlReader.Element pieceElement : element.getChildrenByName("piece")) {
            preloadSpriteComponent.spriteList.add( packPrefix + pieceElement.getAttribute("image") );
            packDataComponent.addDataFromNode( packName, pieceElement );
        }
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                        .append("<loadSelectedPack/>\n");
    }

    private XmlReader.Element parsePackXml(String packPrefix) {
        XmlReader.Element element;
        try {
            InternalFileHandleResolver internalFileResolver = new InternalFileHandleResolver();
            element = new XmlReader().parse(internalFileResolver.resolve(packPrefix + MenuType.getOption(preferencesComponent, MenuType.DECK) + ".pack.xml"));
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to parse pack.", e);
        }
        return element;
    }
}
