package com.ciplogic.memory.core.view.menu.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.PreferencesComponent;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.menu.MenuType;
import com.ciplogic.memory.core.view.menu.component.event.MenuOptionChangedEvent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class MenuOptionComponent extends ActorComponent {
    MenuType menuType;
    String currentOption;
    int currentOptionIndex;

    PreferencesComponent preferencesComponent;

    public MenuOptionComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        menuType = MenuType.fromString(child.getAttribute("type"));
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                        .append("<menuOption type=\"")
                        .append(menuType)
                        .append("\"/>\n");
    }

    @Override
    public void onInit() {
        preferencesComponent = actor.gameWorld.get("preferences").getComponent(PreferencesComponent.class);
        currentOption = MenuType.getOption(preferencesComponent, menuType);
        currentOptionIndex = menuType.getIndex(currentOption);
    }

    public void setNextValue() {
        currentOptionIndex = (currentOptionIndex + 1) % menuType.possibleValues.length;
        currentOption = menuType.possibleValues[currentOptionIndex];

        MenuType.setOption(preferencesComponent, menuType, currentOption);

        EventBus.INSTANCE.fire(new MenuOptionChangedEvent(menuType, currentOption));
    }
}
