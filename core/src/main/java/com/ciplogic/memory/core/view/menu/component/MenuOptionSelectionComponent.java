package com.ciplogic.memory.core.view.menu.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.process.GameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.process.Translate2DProcess;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.time.TimeShift;
import com.ciplogic.memory.core.view.util.StringUtil;

/**
 * Animates the menu selection on the screen.
 */
public class MenuOptionSelectionComponent extends ActorComponent {
    public MenuOptionComponent selected;

    private Translate2DProcess currentTranslation;
    private GameProcessManager gameProcessManager = new GameProcessManager();

    public MenuOptionSelectionComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
                        .append("<menuOptionSelection/>\n");
    }

    public void selectOption(MenuOptionComponent menuOptionComponent) {
        selected = menuOptionComponent;

        if (currentTranslation != null) {
            currentTranslation.finish();
        }

        currentTranslation = new Translate2DProcess(actor.getComponent(Position2DComponent.class),
                0,
                getY(menuOptionComponent) + 2.2f,
                200,
                TimeShift.LINEAR,
                TimeShift.SQRT);

        gameProcessManager.addProcess(currentTranslation);
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    private float getY(MenuOptionComponent menuOptionComponent) {
        float screenY = menuOptionComponent.actor.getComponent(Position2DComponent.class).position2D.y.value;

        return screenY;
    }
}
