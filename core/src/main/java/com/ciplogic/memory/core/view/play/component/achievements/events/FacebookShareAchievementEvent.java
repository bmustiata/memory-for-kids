package com.ciplogic.memory.core.view.play.component.achievements.events;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.view.play.component.achievements.Achievement;

/**
 * Event that gets fired when the user clicks on the facebook
 * share link.
 */
public class FacebookShareAchievementEvent extends Event {
    public final Achievement achievement;

    public FacebookShareAchievementEvent(Achievement achievement) {
        this.achievement = achievement;
    }
}
