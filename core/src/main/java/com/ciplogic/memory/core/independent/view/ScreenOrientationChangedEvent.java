package com.ciplogic.memory.core.independent.view;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.logic.actor.ScreenOrientation;

public class ScreenOrientationChangedEvent extends Event {
    public final ScreenOrientation newScreenOrientation;
    public final ScreenOrientation oldScreenOrientation;

    public ScreenOrientationChangedEvent(ScreenOrientation newScreenOrientation, ScreenOrientation oldScreenOrientation) {
        this.newScreenOrientation = newScreenOrientation;
        this.oldScreenOrientation = oldScreenOrientation;
    }
}
