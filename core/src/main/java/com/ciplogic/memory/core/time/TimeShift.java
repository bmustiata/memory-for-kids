package com.ciplogic.memory.core.time;

/**
 * Time shift computes time.
 */
public abstract class TimeShift {
    public static final TimeShift LINEAR = new LinearTimeShift();

    public static final TimeShift SQR = new SquareTimeShift();
    public static final TimeShift CUBIC = new CubicTimeShift();
    public static final TimeShift QUAD = new QuadTimeShift();
    public static final TimeShift OCTO = new OctoTimeShift();

    public static final TimeShift SQRT = new SqrtTimeShift();
    public static final TimeShift QUADT = new QuadTTimeShift();
    public static final TimeShift OCTOT = new OctoTTimeShift();

    public static final TimeShift SIN = new SinTimeShift();
    public static final TimeShift COS = new CosTimeShift();

    public static TimeShift get(String name) {
        if ("SQR".equalsIgnoreCase(name)) {
            return SQR;
        } else if ("CUBIC".equalsIgnoreCase(name)) {
            return CUBIC;
        } else if ("QUAD".equalsIgnoreCase(name)) {
            return QUAD;
        } else if ("OCTO".equalsIgnoreCase(name)) {
            return OCTO;
        } else if ("SQRT".equalsIgnoreCase(name)) {
            return SQRT;
        } else if ("QUADT".equalsIgnoreCase(name)) {
            return QUADT;
        } else if ("OCTOT".equalsIgnoreCase(name)) {
            return OCTOT;
        } else if ("SIN".equalsIgnoreCase(name)) {
            return SIN;
        } else if ("COS".equalsIgnoreCase(name)) {
            return COS;
        } else {
            return LINEAR;
        }
    }

    abstract public float compute(long start, long duration, long current);
}
