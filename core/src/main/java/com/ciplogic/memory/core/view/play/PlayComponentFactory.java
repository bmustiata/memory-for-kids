package com.ciplogic.memory.core.view.play;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.ComponentFactory;
import com.ciplogic.memory.core.actors.components.DefaultComponentFactory;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.chest.component.AchievementVisualActorComponent;
import com.ciplogic.memory.core.view.play.component.*;
import com.ciplogic.memory.core.view.play.component.achievements.*;

public class PlayComponentFactory implements ComponentFactory {
    private final DefaultComponentFactory defaultFactory;

    public PlayComponentFactory() {
        this.defaultFactory = new DefaultComponentFactory();
    }

    @Override
    public ActorComponent fromXmlNode(Actor actor, XmlReader.Element child) {
        if ("loadSelectedPack".equals(child.getName())) {
            return new LoadSelectedPackComponent(actor, child);
        } else if ("generateInitialPieces".equals(child.getName())) {
            return new GenerateInitialPiecesComponent(actor, child);
        } else if ("packData".equals(child.getName())) {
            return new PackDataComponent(actor, child);
        } else if ("board".equals(child.getName())) {
            return new BoardComponent(actor, child);
        } else if ("boardUi".equals(child.getName())) {
            return new BoardUiComponent(actor, child);
        } else if ("starsGenerator".equals(child.getName())) {
            return new StarsGeneratorComponent(actor, child);
        } else if ("movingSprites".equals(child.getName())) {
            return new MovingSpritesComponent(actor, child);
        } else if ("boardTimer".equals(child.getName())) {
            return new BoardTimerComponent(actor, child);
        } else if ("bestGameTimer".equals(child.getName())) {
            return new BestGameTimerComponent(actor, child);
        } else if ("tileAnimatorProcess".equals(child.getName())) {
            return new TileAnimatorProcessComponent(actor, child);
        }

        // achievements
        if ("achievementStorage".equals(child.getName())) {
            return new AchievementStorageActorComponent(actor, child);
        } else if ("achievements".equals(child.getName())) {
            return new AchievementsActorComponent(actor, child);
        } else if ("achievementGameFinishedEventLauncher".equals(child.getName())) {
            return new AchievementsGameFinishedEventLauncherActorComponent(actor, child);
        } else if ("achievementsDialogsManager".equals(child.getName())) {
            return new AchievementsDialogsManagerActorComponent(actor, child);
        } else if ("achievementVisual".equals(child.getName())) {
            return new AchievementVisualActorComponent(actor, child);
        } else if ("achievementDefinitions".equals(child.getName())) {
            return new AchievementDefinitionsActorComponent(actor, child);
        }

        return defaultFactory.fromXmlNode(actor, child);
    }
}