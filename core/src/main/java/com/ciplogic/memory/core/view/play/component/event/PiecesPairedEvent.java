package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.view.play.component.BoardPieceComponent;

public class PiecesPairedEvent extends Event {
    public final BoardPieceComponent firstPiece;
    public final BoardPieceComponent secondPiece;

    public PiecesPairedEvent(BoardPieceComponent firstPiece, BoardPieceComponent secondPiece) {
        this.firstPiece = firstPiece;
        this.secondPiece = secondPiece;
    }
}
