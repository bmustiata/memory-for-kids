package com.ciplogic.memory.core.independent.logic.actor;

public enum ScreenOrientation {
    /**
     * Portrait.
     */
    PORTRAIT,
    /**
     * Landscape
     */
    LANDSCAPE,

    ANY;

    // the current orientation can never be ANY.
    public static ScreenOrientation CURRENT = PORTRAIT;

    public static final ScreenOrientation[] SCREEN_ORIENTATIONS = new ScreenOrientation[] { PORTRAIT, LANDSCAPE };
}
