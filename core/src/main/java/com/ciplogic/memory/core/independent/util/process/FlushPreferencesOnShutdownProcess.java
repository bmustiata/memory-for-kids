package com.ciplogic.memory.core.independent.util.process;

import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.actors.components.PreferencesComponent;

public class FlushPreferencesOnShutdownProcess extends GameProcess {
    private final GameWorld gameWorld;

    public FlushPreferencesOnShutdownProcess(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public void onUpdate(long delta) {
        // on purpose empty.
    }

    @Override
    public void onFinish() {
        gameWorld.get("preferences").getComponent(PreferencesComponent.class).flush();
    }
}
