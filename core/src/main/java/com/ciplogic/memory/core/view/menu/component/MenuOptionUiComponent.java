package com.ciplogic.memory.core.view.menu.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;
import com.ciplogic.memory.core.view.menu.MenuType;
import com.ciplogic.memory.core.view.menu.component.event.MenuOptionChangedEvent;
import com.ciplogic.memory.core.view.util.StringUtil;

import java.util.Collections;
import java.util.List;

public class MenuOptionUiComponent extends ActorComponent implements EventListener {
    public MenuOptionUiComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<menuOptionUi/>\n");
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void restart() {
        MenuOptionComponent menuOptionComponent = actor.getComponent(MenuOptionComponent.class);
        updateTexture(menuOptionComponent.menuType, menuOptionComponent.currentOption);
    }

    @Override
    public void onEvent(Event event) {
        MenuOptionChangedEvent menuOptionChangedEvent = (MenuOptionChangedEvent) event;
        MenuOptionComponent menuOptionComponent = actor.getComponent(MenuOptionComponent.class);

        if (menuOptionComponent.menuType == menuOptionChangedEvent.menuType) {
            updateTexture(menuOptionChangedEvent.menuType, menuOptionChangedEvent.currentOption);
        }
    }

    private String getTextureId(MenuType menuType, String currentOption) {
        if (currentOption == null) {
            currentOption = menuType.possibleValues[0];
        }

        return "menu/options/" + menuType.name().toLowerCase() + "/" + currentOption.toLowerCase() + ".png";
    }

    private void updateTexture(MenuType menuType, String currentOption) {
        Sprite2DComponent sprite2DComponent = actor.getComponent(Sprite2DComponent.class);
        sprite2DComponent.setTexture( getTextureId(menuType, currentOption) );
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList( MenuOptionChangedEvent.class );
    }
}
