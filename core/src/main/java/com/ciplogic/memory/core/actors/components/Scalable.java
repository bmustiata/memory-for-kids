package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.math.Vector3;

public interface Scalable {
    void setScaling(Vector3 position);
    Vector3 getScaling();
}
