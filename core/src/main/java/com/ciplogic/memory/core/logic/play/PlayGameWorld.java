package com.ciplogic.memory.core.logic.play;

import com.ciplogic.memory.core.actors.ActorFactory;
import com.ciplogic.memory.core.independent.application.cache.ResourceCache;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;
import com.ciplogic.memory.core.logic.GameWorldParser;
import com.ciplogic.memory.core.view.menu.MenuType;
import com.ciplogic.memory.core.view.menu.component.event.MenuOptionChangedEvent;
import com.ciplogic.memory.core.view.play.PlayComponentFactory;

import java.util.List;

/**
 * <p>The Play GameWorld represents the actual game world for the memory game.</p>
 */
public class PlayGameWorld extends GameWorld implements EventListener {
    public static PlayGameWorld INSTANCE = new PlayGameWorld();

    public PlayGameWorld() {
    }

    @Override
    public ActorFactory getActorFactory() {
        return new GameWorldParser(this, "play.world.xml", new PlayComponentFactory());
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List result = super.getListenedEvents();

        result.add( MenuOptionChangedEvent.class );

        return result;
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof MenuOptionChangedEvent) {
            MenuOptionChangedEvent menuOptionChangedEvent = (MenuOptionChangedEvent) event;

            // if the deck changed, nuke the resource cache as well, since we need to reload a bunch of stuff
            if (menuOptionChangedEvent.menuType == MenuType.DECK) {
                resourceCache.dispose();
                resourceCache = new ResourceCache();
            }

            this.nuke();
        } else {
            super.onEvent(event);
        }
    }
}
