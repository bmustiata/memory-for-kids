package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ColorComponent;
import com.ciplogic.memory.core.actors.components.TextComponent;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.play.component.event.CreatedPiecesEvent;
import com.ciplogic.memory.core.view.play.component.event.GameWonEvent;
import com.ciplogic.memory.core.view.play.component.event.ShowPiecesEvent;
import com.ciplogic.memory.core.view.play.component.event.TimedGameWonEvent;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.memory.core.view.play.component.TimeFormatter.formatEllapsedTime;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class BoardTimerComponent extends ActorComponent implements EventListener {
    private final TextComponent currentTimeText;
    private final TextComponent currentTimeTextShadow;
    private final ColorComponent currentTimeColor;

    enum GameState {
        PIECES_CREATED,
        TIMER_STARTING,
        TIMER_STARTED,
        GAME_WINNING,
        GAME_WON
    }

    private GameState gameState = GameState.PIECES_CREATED;

    private long startingTime = -1;
    private long ellapsedTime;

    public BoardTimerComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        this.lifecycled = true;
        currentTimeText = actor.gameWorld.get("current-game-time").getComponent(TextComponent.class);
        currentTimeTextShadow = actor.gameWorld.get("current-game-time-shadow").getComponent(TextComponent.class);
        currentTimeColor = actor.gameWorld.get("current-game-time").getComponent(ColorComponent.class);
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onUpdate(long delta) {
        if (gameState == GameState.TIMER_STARTING) {
            startingTime = delta;
            gameState = GameState.TIMER_STARTED;
        }

        switch (gameState) {
            case PIECES_CREATED:
                currentTimeColor.setRGBA(1,1,1,1); // FIXME: animation process?
                ellapsedTime = 0;
                currentTimeText.text = formatEllapsedTime(ellapsedTime);
                currentTimeTextShadow.text = currentTimeText.text;
                break;
            case TIMER_STARTING:
                startingTime = delta;
                gameState = GameState.TIMER_STARTED;
            case TIMER_STARTED:
                ellapsedTime = delta - startingTime;
                currentTimeText.text =  formatEllapsedTime(ellapsedTime);
                currentTimeTextShadow.text = currentTimeText.text;
                break;
            case GAME_WINNING:
                ellapsedTime = delta - startingTime;
                EventBus.INSTANCE.fire( new TimedGameWonEvent(ellapsedTime) );
                currentTimeText.text =  formatEllapsedTime(ellapsedTime);
                currentTimeTextShadow.text = currentTimeText.text;
                gameState = GameState.GAME_WON;
                currentTimeColor.setRGBA(1,0,1,1); // FIXME: animation process?
        }
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<boardTimer/>\n");
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof CreatedPiecesEvent) {
            gameState = GameState.PIECES_CREATED;
        } else if (event instanceof ShowPiecesEvent) {
            if (gameState == GameState.PIECES_CREATED ) {
                gameState = GameState.TIMER_STARTING;
            }
        } else if (event instanceof GameWonEvent) {
            gameState = GameState.GAME_WINNING;
        }
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List events = new ArrayList();

        events.add(CreatedPiecesEvent.class);
        events.add(ShowPiecesEvent.class);
        events.add(GameWonEvent.class);

        return events;
    }
}
