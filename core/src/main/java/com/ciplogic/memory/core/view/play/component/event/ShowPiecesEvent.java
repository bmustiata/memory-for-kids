package com.ciplogic.memory.core.view.play.component.event;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.view.play.component.BoardPieceComponent;

public class ShowPiecesEvent extends Event {
    public final BoardPieceComponent boardPieceComponent;

    public ShowPiecesEvent(BoardPieceComponent boardPieceComponent) {
        this.boardPieceComponent = boardPieceComponent;
    }
}
