package com.ciplogic.memory.core.view.menu.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.process.RotateModelInstanceProcess;
import com.ciplogic.memory.core.independent.util.process.RunnableTentativeProcess;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class SpinningSunComponent extends ActorComponent {
    private final Sprite2DComponent modelInstance;

    public SpinningSunComponent(Actor actor, XmlReader.Element child) {
        super(actor, child);

        modelInstance = actor.getComponent(Sprite2DComponent.class);
        actor.getComponent(ProcessManagerComponent.class).addProcess(createProcess());
    }

    private GameProcess createProcess() {
        return new RotateModelInstanceProcess(modelInstance, 0, 0, 360, 6000)
            .then(new RunnableTentativeProcess(new Runnable() {
                @Override
                public void run() {
                    actor.getComponent(ProcessManagerComponent.class).addProcess(createProcess());
                }
            }));
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<spinningSun/>\n");
    }
}
