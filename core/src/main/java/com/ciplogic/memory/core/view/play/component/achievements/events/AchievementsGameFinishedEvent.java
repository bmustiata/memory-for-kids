package com.ciplogic.memory.core.view.play.component.achievements.events;

import com.ciplogic.memory.core.independent.application.event.Event;

public class AchievementsGameFinishedEvent extends Event {
    public String deck;
    public int totalPairs;
    public long time;

    public AchievementsGameFinishedEvent(String deck, int totalPairs, long time) {
        this.deck = deck;
        this.totalPairs = totalPairs;
        this.time = time;
    }
}
