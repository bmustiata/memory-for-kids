package com.ciplogic.memory.core.independent.view.gameview;

/**
 * A game view represents a view on top of the model of the game, that can send commands
 * to the game logic. There could be multiple GameView classes, for example an AI, would
 * also be implementing a GameView.
 */
public abstract class GameView {
    public void onInit() {}

    public abstract void onUpdate(long delta);

    public void onFinish() {}
}
