package com.ciplogic.memory.core.view.util;

public class StringUtil {
    public static String indent(int index) {
        StringBuilder result = new StringBuilder("");

        for (int i = 0; i < index; i++) {
            result.append("    ");
        }

        return result.toString();
    }
}
