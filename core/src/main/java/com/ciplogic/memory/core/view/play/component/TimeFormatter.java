package com.ciplogic.memory.core.view.play.component;

public class TimeFormatter {
    public static String formatEllapsedTime(long ellapsedTime) {
        long hours, minutes, seconds, millis;

        if (ellapsedTime < 0) {
            return "--:--:--.---";
        }

        millis = ellapsedTime % 1000L;
        seconds =   ellapsedTime / 1000L % 60L;
        minutes = ellapsedTime / 600000L % 60L;
        hours = ellapsedTime / 36000000L;

        return (hours < 10? "0" + hours : "" + hours) + ":" +
                (minutes < 10? "0" + minutes : minutes) + ":" +
                (seconds < 10? "0" + seconds : seconds) + "." +
                (millis < 10? "00" + millis : (millis < 100 ? "0" + millis : millis));
    }

}
