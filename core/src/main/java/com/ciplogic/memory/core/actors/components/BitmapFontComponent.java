package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.logic.actor.OpenGlContextLostEvent;
import com.ciplogic.memory.core.independent.view.ScreenResizeEvent;

import java.util.ArrayList;
import java.util.List;

import static com.ciplogic.memory.core.actors.components.util.XmlUtil.floatAttribute;
import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public class BitmapFontComponent extends ActorComponent implements EventListener {
    private final String dataFile;
    private final String imageFile;

    public BitmapFont bitmapFont;
    public String font; // the actual font name to register.

    public float fontSize;
    public float initialScreenWidth, initialScreenHeight;
    public float xScaling = 1;
    public float yScaling = 1;

    public BitmapFontComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        dataFile = child.getAttribute("data");
        imageFile = child.getAttribute("image");
        fontSize = floatAttribute(child, "fontSize");

        font = child.getAttribute("name");
        lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<bitmapFont name=\"")
                .append(font)
                .append("\"\n")
                .append(indent(indentLevel + 1))
                .append("data=\"")
                .append(dataFile)
                .append("\"\n")
                .append(indent(indentLevel + 1))
                .append("image=\"")
                .append(imageFile)
                .append("\"\n")
                .append(indent(indentLevel + 1))
                .append("fontSize=\"")
                .append(fontSize)
                .append("\"/>\n");
    }

    @Override
    public void onInit() {
        EventBus.INSTANCE.registerListener(this);
    }

    @Override
    public void onUpdate(long delta) {
    }

    @Override
    public void restart() {
        if (bitmapFont == null) {
            bitmapFont = createBitmapFont();
        }

        initialScreenWidth = Gdx.graphics.getWidth();
        initialScreenHeight = Gdx.graphics.getHeight();
        xScaling = 1;
        yScaling = 1;
    }

    @Override
    public void onFinish() {
        EventBus.INSTANCE.unregisterListener(this);
        if (bitmapFont != null) {
            bitmapFont.dispose();
            bitmapFont = null;
        }
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof OpenGlContextLostEvent) {
            if (bitmapFont == null) { // in case the openGL gets destroyed, but the font was not yet created.
                return;
            }

            bitmapFont.dispose();
            bitmapFont = null;
        } else if (event instanceof ScreenResizeEvent) {
            ScreenResizeEvent screenResizeEvent = (ScreenResizeEvent) event;

            xScaling = initialScreenWidth / screenResizeEvent.width;
            yScaling = initialScreenHeight / screenResizeEvent.height;
        }
    }

    private BitmapFont createBitmapFont() {
        Texture texture = actor.gameWorld.resourceCache.getTexture(imageFile);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        BitmapFont result = new BitmapFont(
                Gdx.files.internal(dataFile),
                new TextureRegion(texture),
                false);
        result.setUseIntegerPositions(false);

        return result;
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        List events = new ArrayList();

        events.add( OpenGlContextLostEvent.class );
        events.add( ScreenResizeEvent.class );

        return events;
    }
}
