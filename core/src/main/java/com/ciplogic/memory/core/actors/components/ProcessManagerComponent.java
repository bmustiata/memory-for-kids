package com.ciplogic.memory.core.actors.components;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.independent.application.process.GameProcess;
import com.ciplogic.memory.core.independent.application.process.GameProcessManager;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

/**
 * A class that allows attaching processes to an actor.
 */
public class ProcessManagerComponent extends ActorComponent {
    private GameProcessManager gameProcessManager = new GameProcessManager();

    public ProcessManagerComponent(Actor actor, XmlReader.Element child) {
        super(actor, null);

        lifecycled = true;
    }

    public ProcessManagerComponent(Actor actor) {
        super(actor, null);

        lifecycled = true;
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<processManager/>\n");
    }

    @Override
    public void onUpdate(long delta) {
        gameProcessManager.updateProcesses(delta);
    }

    @Override
    public void onFinish() {
        gameProcessManager.shutdown();
    }

    public void addProcess(GameProcess gameProcess) {
        gameProcessManager.addProcess(gameProcess);
    }
}
