package com.ciplogic.memory.core.independent.application.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.BaseShaderProvider;
import com.ciplogic.memory.core.independent.logic.actor.GameWorld;

/**
 * The shader provider creates shaders for the things that need rendering.
 */
public class CiplogicShaderProvider extends BaseShaderProvider {
    private final DefaultShader.Config config;

    public CiplogicShaderProvider(GameWorld gameWorld, String vertexShaderCode, String fragmentShaderCode) {
        this.config = new DefaultShader.Config(vertexShaderCode, fragmentShaderCode);

        this.config.numPointLights = gameWorld.getEnvironment().pointLights.size;
        this.config.numDirectionalLights = gameWorld.getEnvironment().directionalLights.size;
    }

    @Override
    protected Shader createShader(Renderable renderable) {
        DefaultShader shader = new DefaultShader(renderable, config);

        if (!shader.program.isCompiled()) {
            Gdx.app.error("KidMem", "Failed compiling shader: \n" + shader.program.getLog() +
                      ", vertex shader code: \n" + config.vertexShader +
                      "\nfragment shader code: \n" + config.fragmentShader);
            return new DefaultShader(renderable);
        }

        return shader;
    }
}
