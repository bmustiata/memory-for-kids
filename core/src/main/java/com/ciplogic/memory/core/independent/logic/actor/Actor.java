package com.ciplogic.memory.core.independent.logic.actor;

import java.util.*;

import static com.ciplogic.memory.core.view.util.StringUtil.indent;

public final class Actor {
    /**
     * Unique ID of the actor inside a game world.
     */
    public String id;

    /**
     * Marks if the actor is alive. If set to false, it means its onFinish()
     * methods were called and it was removed from the game world.
     */
    public boolean alive = true;

    /**
     * Components are what make an actor actually do anything. Components are either regular components,
     * either screen aware components, that have different behaviors depending on the resolution of the
     * client.
     */
    public Map<Class, ActorComponent> components = new LinkedHashMap<Class, ActorComponent>();

    /**
     * Screen aware components are components that activate depending on the screen orientation.
     * They should not be cached, unless called from other screen actor components.
     */
    public Map<Class, Map<ScreenOrientation, ScreenActorComponent>> screenAwareComponents =
            new LinkedHashMap<Class, Map<ScreenOrientation, ScreenActorComponent>>();

    /**
     * Lifecycled components are components that have their onUpdate method called in the view.
     */
    public List<ActorComponent> lifecycledComponents = null;

    public GameWorld gameWorld;

    public Actor(GameWorld gameWorld, String id) {
        assert id != null;
        assert gameWorld != null;

        this.id = id;
        this.gameWorld = gameWorld;
    }

    public Actor withComponent(ActorComponent actorComponent) {
        if (actorComponent instanceof ScreenActorComponent) {
            addScreenActor((ScreenActorComponent) actorComponent);
        } else {
            addRegularActor(actorComponent);
        }

        // FIXME: if there are screen actor components, that are lifecycled, they will be added independent of the screen
        if (actorComponent.lifecycled) {
            if (lifecycledComponents == null) {
                lifecycledComponents = new LinkedList<ActorComponent>();
            }
            lifecycledComponents.add(actorComponent);
        }

        return this;
    }

    public <T extends ActorComponent> T getComponent(Class<T> clazz) {
        ActorComponent result = components.get(clazz);

        if (result != null) {
            return (T) result;
        }

        Map<ScreenOrientation, ScreenActorComponent> screenAwareComponent = screenAwareComponents.get(clazz);
        if (screenAwareComponent != null) {
            return (T) screenAwareComponent.get(ScreenOrientation.CURRENT);
        }

        return null;
    }

    private void addScreenActor(ScreenActorComponent actorComponent) {
        Map<ScreenOrientation, ScreenActorComponent> orientationActorComponentMap = screenAwareComponents.get(actorComponent.getClass());

        if (orientationActorComponentMap == null) {
            orientationActorComponentMap = new LinkedHashMap<ScreenOrientation, ScreenActorComponent>();
            screenAwareComponents.put(actorComponent.getClass(), orientationActorComponentMap);
        }

        if (actorComponent.screenOrientation == ScreenOrientation.ANY) {
            for (ScreenOrientation screenOrientation : ScreenOrientation.SCREEN_ORIENTATIONS) {
                orientationActorComponentMap.put(screenOrientation, actorComponent);
            }
        } else {
            orientationActorComponentMap.put(actorComponent.screenOrientation, actorComponent);
        }
    }

    private void addRegularActor(ActorComponent actorComponent) {
        components.put(actorComponent.getClass(), actorComponent);
    }

    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(indent(indentLevel))
                .append("<actor id=\"")
                .append(id)
                .append("\">\n");

        for (ActorComponent component : components.values()) {
            component.serializeXml(indentLevel + 1, stringBuilder);
        }

        for (Map<ScreenOrientation, ScreenActorComponent> screenActors : screenAwareComponents.values()) {
            for (ActorComponent component: new HashSet<ActorComponent>(screenActors.values())) {
                component.serializeXml(indentLevel + 1, stringBuilder);
            }
        }

        stringBuilder.append(indent(indentLevel))
                .append("</actor>\n");
    }

    public List<ScreenActorComponent> getScreenAwareComponents() {
        List<ScreenActorComponent> actorComponents = new ArrayList<ScreenActorComponent>();

        for (Map.Entry<Class, Map<ScreenOrientation, ScreenActorComponent>> screenOrientationComponent : screenAwareComponents.entrySet()) {
            actorComponents.add( screenOrientationComponent.getValue().get(ScreenOrientation.CURRENT) );
        }

        return actorComponents;
    }
}
