package com.ciplogic.memory.core.view.play.component;

import com.badlogic.gdx.utils.XmlReader;
import com.ciplogic.memory.core.actors.components.ProcessManagerComponent;
import com.ciplogic.memory.core.independent.application.process.BuilderProcess;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.util.process.DestroyActorProcess;
import com.ciplogic.memory.core.independent.util.process.RotateModelInstanceProcess;
import com.ciplogic.memory.core.independent.util.process.Translate2DProcess;
import com.ciplogic.memory.core.independent.view.sprite.Position2DComponent;
import com.ciplogic.memory.core.independent.view.sprite.Sprite2DComponent;
import com.ciplogic.memory.core.view.util.StringUtil;

public class MovingSpritesComponent extends ActorComponent {
    public static final int DURATION = 10000;

    private final float offset;

    public MovingSpritesComponent(Actor actor, XmlReader.Element child) {
        this(actor, Float.parseFloat( child.getAttribute("offset") ));
    }

    public MovingSpritesComponent(Actor actor, float offset) {
        super(actor, null);

        this.offset = offset;

        ProcessManagerComponent processManager = actor.getComponent(ProcessManagerComponent.class);
        Position2DComponent position2DComponent = actor.getComponent(Position2DComponent.class);
        Sprite2DComponent sprite2DComponent = actor.getComponent(Sprite2DComponent.class);

        processManager.addProcess(
            new BuilderProcess().then(
                new Translate2DProcess(position2DComponent,
                                       -60 + offset,
                                       position2DComponent.position2D.y.value - offset,
                                       DURATION),
                new RotateModelInstanceProcess(sprite2DComponent, 0, 0, 145, DURATION / 2)
                    .then(new RotateModelInstanceProcess(sprite2DComponent, 0, 0, -1200, DURATION / 2))
            )
        );
    }

    @Override
    public void serializeXml(int indentLevel, StringBuilder stringBuilder) {
        stringBuilder.append(StringUtil.indent(indentLevel))
            .append("<movingSprites offset=\"")
            .append(offset)
            .append("\"/>\n");
    }
}
