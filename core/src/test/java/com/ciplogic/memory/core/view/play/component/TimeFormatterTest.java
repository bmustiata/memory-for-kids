package com.ciplogic.memory.core.view.play.component;

import org.junit.Test;

import static org.junit.Assert.*;

public class TimeFormatterTest {
    @Test
    public void testTimeFormatter() {
        assertEquals("00:00:00.001", TimeFormatter.formatEllapsedTime(1L));
        assertEquals("00:00:01.001", TimeFormatter.formatEllapsedTime(1001L));
        assertEquals("00:01:01.001", TimeFormatter.formatEllapsedTime(601001L));
        assertEquals("01:01:01.001", TimeFormatter.formatEllapsedTime(36601001L));
    }
}