package com.ciplogic.memory.core.logic;

import org.junit.Assert;
import org.junit.Test;

import static com.ciplogic.memory.core.lang.MapMaker.entry;
import static com.ciplogic.memory.core.lang.MapMaker.map;
import static org.junit.Assert.assertEquals;

public class StringTemplateTest {
    @Test
    public void testTemplate() {
        String formatted = StringTemplate.format("1{FOO}={ASD}2", map(
            entry("FOO", "foo"),
            entry("ASD", "asd")
        ));

        assertEquals("1foo=asd2", formatted);
    }

    @Test
    public void testSingleVariable() {
        String formatted = StringTemplate.format("{starY}", map(
            entry("starY", "-0.3123123")
        ));

        assertEquals("-0.3123123", formatted);
    }
}
