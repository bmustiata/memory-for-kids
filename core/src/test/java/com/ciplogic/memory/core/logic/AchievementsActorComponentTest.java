package com.ciplogic.memory.core.logic;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.independent.logic.actor.Actor;
import com.ciplogic.memory.core.independent.logic.actor.ActorComponent;
import com.ciplogic.memory.core.independent.logic.actor.EmptyGameWorld;
import com.ciplogic.memory.core.view.menu.MenuType;
import com.ciplogic.memory.core.view.play.component.achievements.AchievementStorageActorComponent;
import com.ciplogic.memory.core.view.play.component.achievements.AchievementsActorComponent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementUnlockedEvent;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementsGameFinishedEvent;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertFalse;

public class AchievementsActorComponentTest {
    @Test
    public void testAchievements() {
        Actor actor = new Actor(new EmptyGameWorld(), "");

        actor.withComponent(new AchievementStorageActorComponent(actor, null))
                .withComponent(new AchievementsActorComponent(actor, null));

        for (ActorComponent component : actor.components.values()) {
            component.onInit();
        }

        String firstDeck = MenuType.DECK.possibleValues[0];

        Set achievements = new HashSet();
        EventBus.INSTANCE.registerListener(createAchievementUnlockedListener(achievements));

        EventBus.INSTANCE.fire(new AchievementsGameFinishedEvent(firstDeck, 12, 5));
        EventBus.INSTANCE.fire(new AchievementsGameFinishedEvent(firstDeck, 12, 1));

        for (ActorComponent component : actor.components.values()) {
            component.onFinish();
        }
    }

    private EventListener createAchievementUnlockedListener(final Set achievements) {
        return new EventListener() {
            @Override
            public void onEvent(Event event) {
                AchievementUnlockedEvent castEvent = (AchievementUnlockedEvent) event;

                System.out.println(castEvent.achievement);

                if (achievements.contains(castEvent.achievement)) {
                    assertFalse("Achievement " + castEvent.achievement + " was already unlocked.", false);
                }
            }

            @Override
            public <T extends Event> List<Class<T>> getListenedEvents() {
                return (List) Collections.singletonList(AchievementUnlockedEvent.class);
            }
        };
    }
}
