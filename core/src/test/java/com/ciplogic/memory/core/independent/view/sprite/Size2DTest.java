package com.ciplogic.memory.core.independent.view.sprite;

import com.badlogic.gdx.Gdx;
import com.ciplogic.memory.core.independent.NoopGraphics;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Size2DTest {
    static NoopGraphics graphics = new NoopGraphics();

    @BeforeClass
    public static void initializeTest() {
        Gdx.graphics = graphics;
    }

    @Test
    public void testSimpleScaling() {
        graphics.setSize(400, 300);

        Size2D size2D = Size2D.readFromValues("100%", "100%", Size2D.KeepAspect.NONE, 0f);

        assertEquals(400, size2D.getScreenWidth(), 0.001f);
        assertEquals(300, size2D.getScreenHeight(), 0.001f);
    }
}
