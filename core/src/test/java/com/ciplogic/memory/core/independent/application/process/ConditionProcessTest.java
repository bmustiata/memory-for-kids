package com.ciplogic.memory.core.independent.application.process;

import com.ciplogic.memory.core.independent.util.process.ConditionProcess;
import com.ciplogic.memory.core.independent.util.process.ValidateProcess;
import junit.framework.Assert;
import org.junit.Test;

public class ConditionProcessTest {
    @Test
    public void testConditionProcess() {
        GameProcessManager gameProcessManager = new GameProcessManager();

        final int i[] = { 0 };

        ValidateProcess validateProcess = new ValidateProcess();
        gameProcessManager.addProcess( new ConditionProcess(new ConditionProcess.FinishCondition() {
            @Override
            public boolean check() {
                return ++i[0] == 3;
            }
        }).then(validateProcess) );

        gameProcessManager.updateProcesses(0);
        gameProcessManager.updateProcesses(1);
        gameProcessManager.updateProcesses(2);

        Assert.assertEquals(1, validateProcess.initialized);
        Assert.assertEquals(1, validateProcess.updated);
        Assert.assertEquals(0, validateProcess.shutdown);
    }
}
