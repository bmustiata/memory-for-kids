package com.ciplogic.memory.core.independent.application.process;

import com.ciplogic.memory.core.independent.util.process.*;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GameProcessManagerTest {
    @Test
    public void testGameProcessManagerShutdown() {
        GameProcessManager gameProcessManager = new GameProcessManager();

        ValidateProcess validateProcess = new ValidateProcess();

        gameProcessManager.addProcess(new DelayProcess(100)
                    .then(new DelayProcess(100))
                        .then(validateProcess));

        gameProcessManager.updateProcesses(0);
        gameProcessManager.shutdown();

        assertEquals(1, validateProcess.initialized);
        assertEquals(0, validateProcess.updated);
        assertEquals(1, validateProcess.shutdown);
    }

    @Test
    public void testComplexBuilderShutdown() {
        GameProcessManager gameProcessManager = new GameProcessManager();

        ValidateProcess validateProcess1 = new ValidateProcess();
        ValidateProcess validateProcess2 = new ValidateProcess();
        ValidateProcess validateProcess3 = new ValidateProcess();

        gameProcessManager.addProcess(new DelayProcess(100)
                .then(new DelayProcess(100))
                .then(validateProcess1.then(
                     new DelayProcess(100),
                     validateProcess2,
                     validateProcess3
                )));

        gameProcessManager.updateProcesses(0);
        gameProcessManager.updateProcesses(150);
        gameProcessManager.shutdown();

        assertEquals(1, validateProcess1.initialized);
        assertEquals(1, validateProcess1.updated);
        assertEquals(1, validateProcess1.shutdown);

        assertEquals(1, validateProcess2.initialized);
        assertEquals(0, validateProcess2.updated);
        assertEquals(1, validateProcess2.shutdown);

        assertEquals(1, validateProcess3.initialized);
        assertEquals(0, validateProcess3.updated);
        assertEquals(1, validateProcess3.shutdown);
    }

    @Test
    public void testReallyComplexChain() throws InterruptedException {
        NoopProcess clearScreenProcess;
        final ValidateProcess loadingRenderProcess;
        ValidateProcess renderProcess;

        GameProcess process = new BuilderProcess().then(
                new NoopProcess(),
                new NoopProcess(),
                new NoopProcess(),
                clearScreenProcess = new NoopProcess("clearScreenProcess"),
                new DelayProcess(100).then(
                        new DelayProcess(100).then(
                                new DelayProcess(100),
                                new FinalizeProcess(clearScreenProcess),
                                loadingRenderProcess = renderProcess = new ValidateProcess("loadingHumanView"),
                                new DelayProcess(100).then(
                                        new DelayProcess(100).then(
                                                new DelayProcess(100).then(
                                                        new DelayProcess(100).then(
                                                                new FinalizeProcess(clearScreenProcess),
                                                                new FinalizeProcess(renderProcess).then(
                                                                    new RunnableProcess(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            assertEquals(1, loadingRenderProcess.initialized);
                                                                            assertEquals(1, loadingRenderProcess.shutdown);
                                                                            assertTrue(loadingRenderProcess.updated > 0);
                                                                        }
                                                                    }),
                                                                    renderProcess = new ValidateProcess("memoryGame"),
                                                                    new DelayProcess(100).then(
                                                                        new FinalizeProcess(renderProcess)
                                                                    )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                )
        );

        GameProcessManager gameProcessManager = new GameProcessManager();
        gameProcessManager.addProcess(process);

        while (! gameProcessManager.isEmpty()) {
            gameProcessManager.updateProcesses( new Date().getTime() );
            Thread.sleep(10);
        }

        System.out.println(renderProcess);
    }
}
