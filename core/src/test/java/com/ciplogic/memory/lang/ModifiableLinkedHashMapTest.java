package com.ciplogic.memory.lang;

import com.ciplogic.memory.core.lang.ModifiableLinkedHashMap;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ModifiableLinkedHashMapTest {
    @Test
    public void testAddWhileIterating() {
        ModifiableLinkedHashMap<String, String> map = new ModifiableLinkedHashMap<String, String>();

        map.put("a", "1");
        map.put("b", "2");
        map.put("c", "3");

        int index = 0;

        String[] newKeys = { "d", "e", "f" };
        String[] newValues = { "4", "5", "6" };

        String[] expectedValues = {"1", "2", "3", "4", "5", "6"};

        for (String value : map.values()) {
            if (index < 3) {
                map.put(newKeys[index], newValues[index]);
            }

            System.out.println("value: " + value);

            assertEquals(expectedValues[index], value);
            index++;
        }
    }

    @Test
    public void testRemoveCurrentValueWhileIterating() {
        ModifiableLinkedHashMap<String, String> map = new ModifiableLinkedHashMap<String, String>();

        map.put("a", "1");
        map.put("b", "2");
        map.put("c", "3");
        map.put("d", "4");
        map.put("e", "5");
        map.put("f", "6");

        int index = 0;
        String[] indexToRemove  = {"a", "b", "c", "d", "e", "f"};
        String[] expectedValues = {"1", "2", "3", "4", "5", "6"};

        for (String value : map.values()) {
            System.out.println("value: " + value);

            assertEquals(expectedValues[index], value);

            if (index < 3 || index == 4) {
                map.remove(indexToRemove[index]);
            }

            index++;
        }

        String[] expectedRemovedValues = {"4", "6"};

        index = 0;
        for (String value : map.values()) {
            assertEquals(expectedRemovedValues[index], value);
            index++;
        }
    }
}
