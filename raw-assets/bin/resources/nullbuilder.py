from .resourceprocessor import ResourceProcessor

class NullBuilder(ResourceProcessor):
    def refresh(self, path):
        print("Null: Refreshing %s" % path)

    def remove(self, path):
        print("Null: Removing %s" % path)
