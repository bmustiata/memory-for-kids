import os

from PIL import Image

from .resourceprocessor import ResourceProcessor

def find_power_of_two(n):
    r = 2
    while r < n:
        r *= 2

    if r > 256:
        r = 256

    return r

def compute_new_size(old_size):
    return (find_power_of_two(old_size[0]),
            find_power_of_two(old_size[1]))

class ImageBuilder(ResourceProcessor):
    """
    The ImageBuilder will rescale the images so they are multiple of power of two.
    """
    def refresh(self, path):
        target_path = self.target_path(path)
        if os.path.isfile(path) and self.is_newer(path, target_path):
            print("Process image asset from: %s to %s" % (path, target_path))

            image = Image.open(path)
            new_size = compute_new_size(image.size)

#            print("computed size as " + new_size + " from " + image.size);

            new_image = image.resize(new_size, Image.ANTIALIAS)\
                .transpose( Image.FLIP_TOP_BOTTOM )
            new_image.save(target_path, "PNG")

    def remove(self, path):
        print("Remove asset: %s .. %s" % (path, self.target_path(path)))
        try:
            os.remove( self.target_path(path) )
        except:
            pass # yes, on purpose ignore removal failures.
