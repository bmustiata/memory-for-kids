import subprocess
import os

from .resourceprocessor import ResourceProcessor

class FbxBuilder(ResourceProcessor):
    def refresh(self, path):
        target_path = self.target_path(path)
        target_path = target_path[:-4] + ".g3dj"

        if self.is_newer(path, target_path):
            #FIXME:
            subprocess.call(["C:\\projects\\memory\\raw-assets\\bin\\fbx-conv-win32.exe",
                             "-o", "G3DJ",
                             path,
                             target_path])

    def remove(self, path):
        target_path = self.target_path(path)
        target_path = target_path[:-4] + ".g3dj"

        try:
            os.remove( target_path )
        except:
            pass # yes, on purpose ignore removal failures.
