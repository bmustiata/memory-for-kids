from .resourceprocessor import ResourceProcessor
from time import sleep

class Lazy(ResourceProcessor):
    def __init__(self, delegate):
        self.delegate = delegate

    def refresh(self, path):
        sleep(0.5)
        return self.delegate.refresh(path)

    def remove(self, path):
        return self.delegate.remove(path)
