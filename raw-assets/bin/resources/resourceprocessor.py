
import os
import os.path

class ResourceProcessor(object):
    """
    A resource processor deals with a single file from the assets folder.
    It can either rebuild the asset, it can either remove the asset (in case it spawns
    in multiple resource files).
    """
    def __init__(self):
        self.target_folder = "assets/"
        self.source_folder = "raw-assets/assets"

    def refresh(self, path):
        pass

    def remove(self, path):
        pass

    def is_newer(self, newer_file, reference_file):
        try:
            if not os.path.exists(reference_file):
                return True

            return os.stat(newer_file).st_mtime > os.stat(reference_file).st_mtime
        except:
            return False


    def target_path(self, path):
        """
        Returns the target path which corresponds to this file path from the raw-assets
        folder.
        """
        result_target_path = "assets/" + self.local_path(path)

        print("Target path for %s is %s" % (path, result_target_path))

        return result_target_path

    def local_path(self, path):
        """
        Returns the path relative to the raw-assets root folder.
        """

        path = os.path.abspath(path)

        # FIXME: pretty hardcoded
        prefix = "C:\\projects\\memory\\raw-assets\\raw\\"
        if path.startswith(prefix):
            return path[len(prefix):]

        prefix = "raw-assets/raw"
        if path.startswith(prefix):
            return path[len(prefix) + 1:]

        raise Exception("Unable to find local path for path '%s'" % path)
