import os

from watchdog.events import FileSystemEventHandler

from .directorybuilder import DirectoryBuilder

class FileMonitorHandler(FileSystemEventHandler):
    def __init__(self):
        self._known_processors = {}
        self._directory_processor = DirectoryBuilder()

    def on_modified(self, event):
        processor = self._find_processor( event.src_path )
        if processor:
            processor.refresh( event.src_path )
        else:
            print("Monitor: Unable to process file %s" % event.src_path)

    def on_created(self, event):
        self.on_modified(event)

    def on_deleted(self, event):
        processor = self._find_processor( event.src_path )
        if processor:
            processor.remove( event.src_path )
        else:
            print("Monitor: Removed file has no watcher: %s" % event.src_path)

    def on_moved(self, event):
        processor = self._find_processor( event.dest_path )
        if processor:
            processor.remove( event.src_path )
            processor.refresh( event.dest_path )
        else:
            print("Monitor: Moved file has no watcher: %s" % event.src_path)

    def register(self, extension, resource_processor):
        self._known_processors[extension] = resource_processor

    def _find_processor(self, file_name):
        if file_name.endswith("___jb_bak___"): #hack
            return None

        if os.path.isdir(file_name):
            return self._directory_processor

        for item in self._known_processors.items():
            if file_name.endswith(item[0]):
                print("File `%s` ends with `%s`." % (file_name, item[0]))
                return item[1]

        return None

