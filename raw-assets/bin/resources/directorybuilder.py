from .resourceprocessor import ResourceProcessor

import shutil
import os.path
import os

class DirectoryBuilder(ResourceProcessor):
    def refresh(self, path):
        if os.path.isdir(path):
            print("Copy folder: %s .. %s" % (path, self.target_path(path)))
            os.makedirs(self.target_path(path), exist_ok=True)

    def remove(self, path):
        print("Remove folder: %s .. %s" % (path, self.target_path(path)))
        shutil.rmtree(self.target_path(path), ignore_errors=True)
