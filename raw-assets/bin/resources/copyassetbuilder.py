import shutil
import os.path
import os

from .resourceprocessor import ResourceProcessor

class CopyAssetBuilder(ResourceProcessor):
    def refresh(self, path):
        target_path = self.target_path(path)
        if os.path.isfile(path) and self.is_newer(path, target_path):
            print("Copy asset from: %s to %s" % (path, target_path))
            shutil.copyfile(path, target_path)

    def remove(self, path):
        print("Remove asset: %s .. %s" % (path, self.target_path(path)))
        try:
            os.remove( self.target_path(path) )
        except:
            pass # yes, on purpose ignore removal failures.
