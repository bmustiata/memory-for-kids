# This application is a daemon that monitors the resources from the raw-assets and ista transforms them for the assets
# note that no file should exist initially into the assets themselves.

import time

from watchdog.observers import Observer

from resources import FbxBuilder
from resources import CopyAssetBuilder
from resources import ImageBuilder
from resources import Lazy

from resources.file_monitor_handler import FileMonitorHandler

if __name__ == "__main__":
    event_handler = FileMonitorHandler()

    event_handler.register(".png", Lazy(ImageBuilder()))
    # event_handler.register(".png", CopyAssetBuilder())
    event_handler.register(".xml", CopyAssetBuilder())
    event_handler.register(".g3dj", CopyAssetBuilder())
    event_handler.register(".fnt", CopyAssetBuilder())
    event_handler.register(".glsl", CopyAssetBuilder())
    event_handler.register(".fbx", FbxBuilder())

    observer = Observer()
    observer.schedule(event_handler, path='raw-assets/raw', recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    finally:
        observer.stop()
        observer.join()
