from resources import *
from resources.file_monitor_handler import FileMonitorHandler

import os

class Event(object):
    def __init__(self, path):
        self.src_path = path

import shutil

if __name__ == "__main__":
    event_handler = FileMonitorHandler()

    event_handler.register(".png", ImageBuilder())
    #event_handler.register(".png", CopyAssetBuilder())
    event_handler.register(".xml", CopyAssetBuilder())
    event_handler.register(".glsl", CopyAssetBuilder())
    event_handler.register(".g3dj", CopyAssetBuilder())
    event_handler.register(".fnt", CopyAssetBuilder())
    event_handler.register(".fbx", FbxBuilder())

    shutil.rmtree("assets", ignore_errors=True)

    os.mkdir("assets")

    for root, dirs, files in os.walk("raw-assets/raw"):
        for d in dirs: # create first the folders
            event_handler.on_modified(Event(root + "/" + d))

        for f in files: # then process the files
            event_handler.on_modified(Event(root + "/" + f))

