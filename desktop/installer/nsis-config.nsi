; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------

; The name of the installer
Name "KidMem"
Icon ..\..\raw-assets\raw\memory\icon\icon-128.ico

; The file to write
OutFile "kidmem-installer.exe"

; The default installation directory
InstallDir "$LOCALAPPDATA\KidMem"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Ciplogic\KidMem" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "KidMem (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "kidmem.exe"
  File /r jre7
  
  ; Write the installation path into the registry
  WriteRegStr HKLM Software\Ciplogic\KidMem "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\KidMem" "DisplayName" "KidMem"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\KidMem" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\KidMem" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\KidMem" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\KidMem"
  CreateShortCut "$SMPROGRAMS\KidMem\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\KidMem\KidMem.lnk" "$INSTDIR\KidMem.exe" "" "$INSTDIR\KidMem.exe" 0
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Desktop Shortcut"

  CreateShortCut "$DESKTOP\KidMem.lnk" "$INSTDIR\KidMem.exe" "" "$INSTDIR\KidMem.exe" 0
  
SectionEnd


;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\KidMem"
  DeleteRegKey HKLM Software\Ciplogic\KidMem

  ; Remove files and uninstaller
  Delete $INSTDIR\kidmem.exe
  Delete $INSTDIR\jre7
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\KidMem\*.*"
  Delete "$DESKTOP\KidMem.lnk"

  ; Remove directories used
  RMDir "$SMPROGRAMS\KidMem"
  RMDir "$INSTDIR"

SectionEnd
