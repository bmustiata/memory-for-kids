package com.ciplogic.memory.java;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ciplogic.memory.core.MemoryGame;
import com.ciplogic.memory.core.independent.application.event.EventBus;

public class MemoryGameDesktop {
	public static void main (String[] args) {
        LwjglApplicationConfiguration configuration = configureApplication();

        EventBus.INSTANCE.registerListener(new DesktopFacebookShareListener());

		new LwjglApplication(new MemoryGame(), configuration);
	}

    private static LwjglApplicationConfiguration configureApplication() {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.fullscreen = false;
        config.width = 600;
        config.height = 400;
        config.vSyncEnabled = false;

        config.addIcon("play/icon/icon-128.png", Files.FileType.Internal);
        config.addIcon("play/icon/icon-64.png", Files.FileType.Internal);
        config.addIcon("play/icon/icon-32.png", Files.FileType.Internal);
        config.addIcon("play/icon/icon-16.png", Files.FileType.Internal);

        config.title = "KidMem";
        return config;
    }
}
