package com.ciplogic.memory.java;

import com.ciplogic.memory.core.independent.application.event.Event;
import com.ciplogic.memory.core.independent.application.event.EventBus;
import com.ciplogic.memory.core.independent.application.event.EventListener;
import com.ciplogic.memory.core.view.play.component.achievements.events.AchievementProcessedEvent;
import com.ciplogic.memory.core.view.play.component.achievements.events.FacebookShareAchievementEvent;

import java.awt.*;
import java.net.URI;
import java.util.Collections;
import java.util.List;

/**
 * Implements the Desktop Facebook Sharing.
 */
public class DesktopFacebookShareListener implements EventListener {
    public DesktopFacebookShareListener() {
    }

    @Override
    public void onEvent(Event event) {
        openWebpage("http://facebook.com/yay");
        EventBus.INSTANCE.fire(new AchievementProcessedEvent());
    }

    @Override
    public <T extends Event> List<Class<T>> getListenedEvents() {
        return (List) Collections.singletonList(FacebookShareAchievementEvent.class);
    }

    public static void openWebpage(String url) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                URI uri = URI.create(url);
                desktop.browse(uri);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}
