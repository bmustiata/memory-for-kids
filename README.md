h1. Memory for Kids

Memory for Kids, is a Memory Game, that has for memory tiles, letters, numbers, and objects, and whenever matches are made by the child, the actual object is being narrated.

The game is available as a Windows, Android or HTML5 application.

h2. Rules of the game.

When the game starts, a set of 24 tiles are presented (configurable) with 12 different pairs of images, from a set of 40+. The child can pick then two cards that will be presented to it. If the selected cards match one of the pairs, the cards are removed from the desk, and the name of the visible image, narrated (e.g. the letter, the object, etc.)

